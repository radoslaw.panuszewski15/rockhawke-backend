package pl.semidude.rockhawk.presentation.main.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.PopupMenu
import androidx.fragment.app.Fragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.sellmair.disposer.disposeBy
import io.sellmair.disposer.onDestroy
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.top_bar.*
import org.koin.android.viewmodel.ext.android.sharedViewModel
import pl.semidude.rockhawk.R
import pl.semidude.rockhawk.data.dto.Playback
import pl.semidude.rockhawk.domain.tracks.LibraryManager.LibraryItemsState
import pl.semidude.rockhawk.helpers.createVerticalLayoutManager
import pl.semidude.rockhawk.helpers.invisible
import pl.semidude.rockhawk.helpers.startActivity
import pl.semidude.rockhawk.helpers.visible
import pl.semidude.rockhawk.presentation.login.LoginActivity
import pl.semidude.rockhawk.presentation.main.MainViewModel
import pl.semidude.rockhawk.presentation.main.search.TracksAdapter


class HomeFragment : Fragment() {

    private val viewModel: MainViewModel by sharedViewModel()
    private lateinit var tracksAdapter: TracksAdapter

    companion object {
        fun newInstance() = HomeFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupTopBar()
        initRecyclerView()
        initOptionsButton()
        observeLibraryItems()
        viewModel.fetchLibraryItems()
    }

    private fun setupTopBar() {
        subtitleTopBar.text = "Library"
        subtitleTopBar.visible()
        circleDivider.visible()
    }

    private fun initRecyclerView() {
        tracksAdapter = TracksAdapter(viewModel, context!!)

        tracksRecycler.apply {
            adapter = tracksAdapter
            layoutManager = createVerticalLayoutManager(context)
        }

        tracksAdapter.onItemClickListener = { track, viewHolder ->
            when (track.type) {
                "spotify" -> {
                    viewModel.requestPlaySpotify(track)
                }
                "youtube" -> {
                    viewModel.requestPlayYoutube(track)
                }
            }
        }
    }

    private fun initOptionsButton() {
        userOptionsButton.setOnClickListener {
            PopupMenu(activity!!, userOptionsButton).apply {
                setOnMenuItemClickListener { item ->
                    when (item?.itemId) {
                        R.id.logout -> {
                            viewModel.logout()
                            startActivity<LoginActivity>()
                            true
                        }
                        else -> false
                    }
                }
                inflate(R.menu.menu_user)
                show()
            }
        }
    }

    private fun observeLibraryItems() {
        viewModel.observeLibraryItems()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { libraryItemsState ->
                when (libraryItemsState) {
                    is LibraryItemsState.Loading -> {
                        if (tracksAdapter.itemCount == 0) {
                            fetchProgressBar.visible()
                        }
                    }
                    is LibraryItemsState.Result -> {
                        tracksAdapter.loadItems(libraryItemsState.libraryItems)
                        fetchProgressBar.invisible()
                    }
                    is LibraryItemsState.Error -> {

                    }
                }
            }
            .disposeBy(onDestroy)
    }
}
