package pl.semidude.rockhawk.data.dto

import com.squareup.moshi.Json

data class TokenResponse(
    @Json(name = "user_id") val userId: String,
    @Json(name = "access_token") val accessToken: String,
    @Json(name = "refresh_token") val refreshToken: String
)
