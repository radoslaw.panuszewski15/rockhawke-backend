package pl.semidude.rockhawk.presentation.main.playlist

import android.annotation.SuppressLint
import android.content.Context
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_playlist_to_import.view.*
import pl.semidude.rockhawk.R
import pl.semidude.rockhawk.data.dto.Playlist
import pl.semidude.rockhawk.helpers.BaseAdapter
import pl.semidude.rockhawk.helpers.inflateView
import pl.semidude.rockhawk.helpers.invisible
import pl.semidude.rockhawk.presentation.main.MainViewModel
import pl.semidude.rockhawk.presentation.main.playlist.PlaylistsFromSpotifyAdapter.PlaylistViewHolder

class PlaylistsFromSpotifyAdapter(
    private val viewModel: MainViewModel,
    context: Context
) : BaseAdapter<PlaylistViewHolder, Playlist>(context) {

    override fun doCreateViewHolder(parent: ViewGroup, viewType: Int): PlaylistViewHolder {
        val itemView = inflateView(context, R.layout.item_playlist_to_import, parent, false)
        return PlaylistViewHolder(itemView)
    }

    override fun doBindViewHolder(viewHolder: PlaylistViewHolder, position: Int) {
        viewHolder.bindViewHolder(items[position])
    }

    inner class PlaylistViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val playlistName: TextView = itemView.playlistName
        private val firstInfoField: TextView = itemView.firstInfoField
        private val secondInfoField: TextView = itemView.secondInfoField
        private val thirdInfoField: TextView = itemView.thirdInfoField
        private val firstDivider: ImageView = itemView.circleDivider
        private val secondDivider: ImageView = itemView.circleDivider2
        private val importPlaylistButton: FrameLayout = itemView.importPlaylistButton

        @SuppressLint("SetTextI18n")
        fun bindViewHolder(playlist: Playlist) {
            playlistName.text = playlist.playlistName
            firstInfoField.text = playlist.tracksCount.toString() + " songs"
            firstDivider.invisible()
            secondInfoField.invisible()
            secondDivider.invisible()
            thirdInfoField.invisible()

            importPlaylistButton.setOnClickListener {
                viewModel.importSpotifyPlaylist(playlist.playlistId!!)
            }
        }
    }
}