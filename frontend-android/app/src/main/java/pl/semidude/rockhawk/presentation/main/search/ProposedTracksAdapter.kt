package pl.semidude.rockhawk.presentation.main.search

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_proposed_track.view.*
import kotlinx.android.synthetic.main.item_track.view.circleDivider
import kotlinx.android.synthetic.main.item_track.view.circleDivider2
import kotlinx.android.synthetic.main.item_track.view.firstInfoField
import kotlinx.android.synthetic.main.item_track.view.secondInfoField
import kotlinx.android.synthetic.main.item_track.view.spotifyIcon
import kotlinx.android.synthetic.main.item_track.view.thirdInfoField
import kotlinx.android.synthetic.main.item_track.view.titleView
import kotlinx.android.synthetic.main.item_track.view.youtubeIcon
import kotlinx.android.synthetic.main.item_track.view.youtubeIconBackground
import pl.semidude.rockhawk.R
import pl.semidude.rockhawk.data.dto.Playlist
import pl.semidude.rockhawk.data.dto.Track
import pl.semidude.rockhawk.helpers.*
import pl.semidude.rockhawk.presentation.main.MainViewModel
import pl.semidude.rockhawk.presentation.main.search.ProposedTracksAdapter.ProposedTrackViewHolder

class ProposedTracksAdapter(
    private val viewModel: MainViewModel,
    private val playlist: Playlist,
    context: Context
) : BaseAdapter<ProposedTrackViewHolder, Track>(context) {

    override fun doCreateViewHolder(parent: ViewGroup, viewType: Int): ProposedTrackViewHolder {
        val itemView = inflateView(context, R.layout.item_proposed_track, parent, false)
        return ProposedTrackViewHolder(itemView)
    }

    override fun doBindViewHolder(viewHolder: ProposedTrackViewHolder, position: Int) {
        viewHolder.bindProposedTrack(items[position])
    }

    inner class ProposedTrackViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val titleView: TextView = itemView.titleView
        private val youtubeIcon: ImageView = itemView.youtubeIcon
        private val youtubeIconBackground: ImageView = itemView.youtubeIconBackground
        private val spotifyIcon: ImageView = itemView.spotifyIcon
        private val firstInfoField: TextView = itemView.firstInfoField
        private val secondInfoField: TextView = itemView.secondInfoField
        private val thirdInfoField: TextView = itemView.thirdInfoField
        private val firstDivider: ImageView = itemView.circleDivider
        private val secondDivider: ImageView = itemView.circleDivider2
        private val addTrackButton: FrameLayout = itemView.addTrackButton

        fun bindProposedTrack(track: Track) {
            titleView.text = track.title
            firstInfoField.text = track.durationFormatted

            when (track.type) {
                "youtube" -> bindYoutubeTrack(track)
                "spotify" -> bindSpotifyTrack(track)
            }
            addTrackButton.setOnClickListener {
                viewModel.addTrackToPlaylist(playlist, track)
            }
        }

        private fun bindYoutubeTrack(track: Track) {
            secondInfoField.text = track.viewCount!!
            thirdInfoField.text = track.owner

            thirdInfoField.visible()
            secondDivider.visible()

            youtubeIcon.visible()
            youtubeIconBackground.visible()
            spotifyIcon.invisible()
        }

        private fun bindSpotifyTrack(track: Track) {
            secondInfoField.text = track.artists?.joinToString(", ") { it.name }

            thirdInfoField.gone()
            secondDivider.gone()

            spotifyIcon.visible()
            youtubeIcon.invisible()
            youtubeIconBackground.invisible()
        }
    }
}