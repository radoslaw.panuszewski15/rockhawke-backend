package pl.semidude.rockhawk.presentation.register

import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.sellmair.disposer.disposeBy
import pl.semidude.rockhawk.data.api.RockhawkApi
import pl.semidude.rockhawk.domain.account.AccountManager
import pl.semidude.rockhawk.helpers.ViewModelWithDisposers

class RegisterViewModel(
    private val rockhawkApi: RockhawkApi,
    private val accountManager: AccountManager
) : ViewModelWithDisposers() {

    private val registerStateSubject = BehaviorSubject.create<RegisterState>()

    fun observeRegisterState() = registerStateSubject

    fun registerUser(email: String, password: String) {
        rockhawkApi.registerUser(email, password)
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { registerStateSubject.onNext(RegisterState.Loading) }
            .andThen(rockhawkApi.loginWithCredentials(email, password))
            .subscribeBy(
                onSuccess = { tokenResponse ->

                    with(tokenResponse) {

                        accountManager.loginUser(userId, accessToken, refreshToken)

                        registerStateSubject.onNext(RegisterState.Registered)
                    }
                },
                onError = { throwable -> registerStateSubject.onNext(RegisterState.Error(throwable)) }
            )
            .disposeBy(onCleared)
    }

    sealed class RegisterState {
        object Loading : RegisterState()
        object Registered : RegisterState()
        data class Error(val throwable: Throwable) : RegisterState()
    }
}