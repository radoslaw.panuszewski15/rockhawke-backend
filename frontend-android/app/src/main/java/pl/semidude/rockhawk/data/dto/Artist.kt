package pl.semidude.rockhawk.data.dto

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Artist(
    var id: String,
    var name: String
) : Parcelable