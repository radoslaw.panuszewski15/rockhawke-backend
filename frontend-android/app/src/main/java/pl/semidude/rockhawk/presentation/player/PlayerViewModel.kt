package pl.semidude.rockhawk.presentation.player

import io.reactivex.schedulers.Schedulers
import pl.semidude.rockhawk.data.api.RockhawkApi
import pl.semidude.rockhawk.data.dto.Track
import pl.semidude.rockhawk.domain.tracks.LibraryManager
import pl.semidude.rockhawk.domain.tracks.SearchManager
import pl.semidude.rockhawk.helpers.ViewModelWithDisposers

class PlayerViewModel(
    private val rockhawkApi: RockhawkApi,
    private val libraryManager: LibraryManager,
    private val searchManager: SearchManager
) : ViewModelWithDisposers() {

    fun addTrackToLibrary(track: Track) {
        libraryManager.addTrackToLibrary(track)
            .subscribe()

        track.inLibrary = true
    }

    fun removeTrackFromLibrary(track: Track) {
        libraryManager.removeTrackFromLibrary(track)
            .subscribe()

        track.inLibrary = false
    }

    fun prepareYoutubePlayback(youtubeTrack: Track) =
        rockhawkApi.prepareYoutubePlayback(youtubeTrack)
            .subscribeOn(Schedulers.io())

    fun obtainRecommendedNextTrack(track: Track) =
        rockhawkApi.getRecommendedNextTrack(track.id)
            .subscribeOn(Schedulers.io())
}