package pl.semidude.rockhawk.domain.playback

import android.app.*
import android.app.Notification.GROUP_ALERT_SUMMARY
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP
import android.content.Intent.FLAG_ACTIVITY_SINGLE_TOP
import android.net.Uri
import android.os.Binder
import android.os.IBinder
import android.view.View
import android.widget.RemoteViews
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.upstream.*
import com.google.android.exoplayer2.util.Util.getUserAgent
import com.spotify.android.appremote.api.ConnectionParams
import com.spotify.android.appremote.api.Connector
import com.spotify.android.appremote.api.SpotifyAppRemote
import com.spotify.protocol.types.Repeat
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import pl.semidude.rockhawk.R
import pl.semidude.rockhawk.data.dto.Album
import pl.semidude.rockhawk.data.dto.Artist
import pl.semidude.rockhawk.data.dto.Playback
import pl.semidude.rockhawk.data.dto.Track
import pl.semidude.rockhawk.di.YOUTUBE_PLAYBACKS_ENDPOINT
import pl.semidude.rockhawk.domain.tracks.TracksMemorizer
import pl.semidude.rockhawk.presentation.player.PlayerActivity
import java.util.*
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.thread
import kotlin.concurrent.withLock


class PlaybackService : Service() {

    var currentPlayingTrack: Track? = null
        private set

    private lateinit var exoPlayer: ExoPlayer
    private var spotifyAppRemote: SpotifyAppRemote? = null
    private var isStartedForeground = false
    private lateinit var playbackQueue: PlaybackQueue
    private lateinit var tracksMemorizer: TracksMemorizer
    private lateinit var memorizerDisposable: Disposable

    private val playbackStatusSubject = BehaviorSubject.create<PlaybackStatus>()
    private val playbackProgressSubject = PublishSubject.create<Int>()
    private val currentPlayingTrackSubject = BehaviorSubject.create<Track>()
    private val stoppedPlaybacksSubject = BehaviorSubject.create<Long>()

    private var currentPlaybackProvider: PlaybackProvider? = null
    private var isSeekingInProgress = false
    private val seekingLock = ReentrantLock()

    private var lastYoutubePlaybacks: Deque<Playback> = LinkedList()
    private var lastYoutubeTracks: Deque<Track> = LinkedList()
    private var recommendedNextYoutubeTrack: Track? = null
    private var recommendedNextYoutubePlayback: Playback? = null

    private lateinit var dataSourceFactory: DataSource.Factory
    private lateinit var accessToken: String

    private lateinit var remoteViews: RemoteViews
    private lateinit var notification: Notification
    private lateinit var notificationManager: NotificationManager

    enum class PlaybackProvider { EXO_PLAYER, SPOTIFY_REMOTE }
    enum class PlaybackStatus { PLAYING, PAUSED }

    private val binder = LocalBinder()

    inner class LocalBinder : Binder() {
        fun getService() = this@PlaybackService
    }

    override fun onBind(intent: Intent?): IBinder? {

        val channel = NotificationChannel("rockhawk-player", "Rockhawk Player", NotificationManager.IMPORTANCE_HIGH)
        channel.description = "The music player notification for Rockhawk"

        notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.createNotificationChannel(channel)

        val resumeIntent = createPlaybackPendingRequest(REQUEST_RESUME)
        val pauseIntent = createPlaybackPendingRequest(REQUEST_PAUSE)
        val previousIntent = createPlaybackPendingRequest(REQUEST_PREVIOUS)
        val nextIntent = createPlaybackPendingRequest(REQUEST_NEXT)

        val activityIntent = Intent(this, PlayerActivity::class.java).apply {
            flags = FLAG_ACTIVITY_CLEAR_TOP or FLAG_ACTIVITY_SINGLE_TOP
        }
        val activityPendingIntent = PendingIntent.getActivity(this, 0, activityIntent, 0)

        remoteViews = RemoteViews(packageName, R.layout.notification_player).apply {
            setOnClickPendingIntent(R.id.playButton, resumeIntent)
            setOnClickPendingIntent(R.id.pauseButton, pauseIntent)
            setOnClickPendingIntent(R.id.previousButton, previousIntent)
            setOnClickPendingIntent(R.id.nextButton, nextIntent)
        }
        notification = Notification.Builder(this, "rockhawk-player")
            .setGroupAlertBehavior(GROUP_ALERT_SUMMARY)
            .setGroup("Rockhawk")
            .setCustomContentView(remoteViews)
            .setSmallIcon(R.drawable.ic_hawk)
            .setContentIntent(activityPendingIntent)
            .build()

        return binder
    }

    private fun createPlaybackPendingRequest(requestCode: Int): PendingIntent {
        val intent = Intent(applicationContext, PlaybackBroadcastReceiver::class.java)
        intent.action = requestCode.toString()
        return PendingIntent.getBroadcast(this, 0, intent, 0)
    }

    override fun onCreate() {
        exoPlayer = ExoPlayerFactory.newSimpleInstance(this)
        dataSourceFactory = DefaultHttpDataSourceFactory(getUserAgent(this, "Rockhawke"))
        connectToSpotify()
        notifyWhenYoutubePlaybackEnded()
        thread { keepOnPlaybackPosition() }
    }

    private fun notifyWhenYoutubePlaybackEnded() {

        exoPlayer.addListener(object : Player.EventListener {

            override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {

                if (playbackState == Player.STATE_ENDED) {
                    lastYoutubeTracks.peek()?.let { track ->
                        notifyPlaybackProgressChanged(track.durationMillis)

                        playbackQueue.popPlayback()?.let { nextPlayback ->
                            Thread.sleep(100)
                            playYoutube(nextPlayback)
                        }
                    }
                }
            }
        })
    }

    private fun keepOnPlaybackPosition() {
        while (true) {
            when (currentPlaybackProvider) {
                PlaybackProvider.EXO_PLAYER -> {
                    if (exoPlayer.playbackState != Player.STATE_ENDED) {
                        notifyPlaybackProgressChanged(exoPlayer.currentPosition)
                    }
                }
                PlaybackProvider.SPOTIFY_REMOTE -> {
                    spotifyAppRemote?.playerApi?.playerState?.setResultCallback { playerState ->
                        seekingLock.withLock {
                            if (!isSeekingInProgress) {
                                notifyPlaybackProgressChanged(playerState.playbackPosition)
                            } else {
                                isSeekingInProgress = false
                            }
                        }
                    }
                }
            }
            Thread.sleep(100)
        }
    }

    fun setTracksMemorizer(tracksMemorizer: TracksMemorizer) {
        this.tracksMemorizer = tracksMemorizer

        memorizerDisposable = tracksMemorizer.observeMemorizedTracks()
            .subscribe { memorizedTracks ->
                val track = memorizedTracks.find { it.id == currentPlayingTrack?.id }
                if (track != null) {
                    currentPlayingTrackSubject.onNext(track)
                }
            }
    }

    fun setPlaybackQueue(playbackQueue: PlaybackQueue) {
        this.playbackQueue = playbackQueue
    }

    override fun onDestroy() {
        super.onDestroy()
        memorizerDisposable.dispose()
    }

    fun resumePlayback() {
        when (currentPlaybackProvider) {
            PlaybackProvider.EXO_PLAYER -> exoPlayer.playWhenReady = true
            PlaybackProvider.SPOTIFY_REMOTE -> spotifyAppRemote?.playerApi?.resume()
        }
        notifyPlaybackStatusChanged(PlaybackStatus.PLAYING)
    }

    fun pausePlayback() {
        when (currentPlaybackProvider) {
            PlaybackProvider.EXO_PLAYER -> exoPlayer.playWhenReady = false
            PlaybackProvider.SPOTIFY_REMOTE -> spotifyAppRemote?.playerApi?.pause()
        }
        notifyPlaybackStatusChanged(PlaybackStatus.PAUSED)
    }

    fun skipNext() {
        playbackQueue.popPlayback()?.let { playback ->
            when (playback.track.type) {
                "spotify" -> playSpotify(playback.track)
                "youtube" -> playYoutube(playback)
            }
            return
        }
        if (currentPlaybackProvider == PlaybackProvider.SPOTIFY_REMOTE) {
            spotifyAppRemote?.playerApi?.skipNext()
        }
    }

    fun skipPrevious() {
        when (currentPlaybackProvider) {
            PlaybackProvider.EXO_PLAYER -> {
                if (lastYoutubeTracks.isNotEmpty()) {

                    if (exoPlayer.currentPosition < 1000 && lastYoutubeTracks.size >= 2) {
                        lastYoutubeTracks.pop()
                        lastYoutubePlaybacks.pop()
                    }
                    val playback = lastYoutubePlaybacks.pop()

                    if (playback != null) {
                        playYoutube(playback)
                    }
                }
            }
            PlaybackProvider.SPOTIFY_REMOTE -> spotifyAppRemote?.playerApi?.skipPrevious()
        }
    }

    fun playSpotify(track: Track) {
        currentPlaybackProvider = PlaybackProvider.SPOTIFY_REMOTE
        exoPlayer.stop()
        spotifyAppRemote?.playerApi?.setRepeat(Repeat.OFF)
        spotifyAppRemote?.playerApi?.play("spotify:track:${track.id}")
        notifyCurrentPlayingTrackChanged(track)
        notifyPlaybackStatusChanged(PlaybackStatus.PLAYING)
    }

    fun playYoutube(playback: Playback) {
        currentPlaybackProvider = PlaybackProvider.EXO_PLAYER
        spotifyAppRemote?.playerApi?.pause()
        lastYoutubePlaybacks.peek()?.let { notifyYoutubePlaybackStopped(it.playbackId) }

        val mediaSource =
            if(playback.track.s3Url != null) createMediaSourceForS3(playback)
            else createMediaSourceForStream(playback)

        exoPlayer.prepare(mediaSource)
        exoPlayer.playWhenReady = true
        lastYoutubePlaybacks.push(playback)
        lastYoutubeTracks.push(playback.track)
        notifyCurrentPlayingTrackChanged(playback.track)
        notifyPlaybackStatusChanged(PlaybackStatus.PLAYING)
    }

    private fun createMediaSourceForStream(playback: Playback): ProgressiveMediaSource? {
        val url = "$YOUTUBE_PLAYBACKS_ENDPOINT/${playback.playbackId}/0"
        val mediaSourceFactory = ProgressiveMediaSource.Factory {
            val dataSource = DefaultHttpDataSource(getUserAgent(this, "Rockhawke"))
            dataSource.setRequestProperty("Authorization", "Bearer $accessToken")
            dataSource
        }
        return mediaSourceFactory.createMediaSource(Uri.parse(url))
    }

    private fun createMediaSourceForS3(playback: Playback)
        = ProgressiveMediaSource.Factory(dataSourceFactory)
            .createMediaSource(Uri.parse(playback.track.s3Url))

    fun stopSpotify() {
        spotifyAppRemote?.playerApi?.pause()
    }

    fun seekTo(position: Int) {
        System.err.println("SEEK TO $position")
        when (currentPlaybackProvider) {
            PlaybackProvider.EXO_PLAYER -> {
                exoPlayer.seekTo(position.toLong())
            }
            PlaybackProvider.SPOTIFY_REMOTE -> {
                seekingLock.withLock {
                    isSeekingInProgress = true
                    spotifyAppRemote?.playerApi?.seekTo(position.toLong())
                    notifyPlaybackProgressChanged(position.toLong())
                }
            }
        }
    }

    fun setNextPlayback(track: Track, playback: Playback) {
        recommendedNextYoutubeTrack = track
        recommendedNextYoutubePlayback = playback
    }

    fun setAccessToken(accessToken: String) {
        this.accessToken = accessToken
    }

    fun connectToSpotify() {
        val connectionParams = ConnectionParams.Builder(CLIENT_ID)
            .setRedirectUri("https://rockhawke.herokuapp.com/callback/spotify") //it must be present
            .showAuthView(true)
            .build()

        SpotifyAppRemote.connect(this, connectionParams,
            object : Connector.ConnectionListener {
                override fun onConnected(spotifyAppRemote: SpotifyAppRemote) {
                    this@PlaybackService.spotifyAppRemote = spotifyAppRemote
                    subscribeToPlayerState()
                }
                override fun onFailure(throwable: Throwable) {
                    throwable.printStackTrace()
                }
            })
    }

    fun disconnectFromSpotify() {
        SpotifyAppRemote.disconnect(spotifyAppRemote)
    }

    private fun subscribeToPlayerState() {
        spotifyAppRemote?.playerApi
            ?.subscribeToPlayerState()
            ?.setEventCallback { playerState ->

                playerState.track?.let { track ->

                    if (currentPlaybackProvider == PlaybackProvider.EXO_PLAYER)
                        return@let

                    val rawTrackId = track.uri.replace("spotify:track:", "")
                    val thumbnailUrl = track.imageUri.raw.replace("spotify:image:", "https://i.scdn.co/image/")

                    val artists = track.artists.map { Artist(it.uri.replace("spotify:artist:", ""), it.name) }
                    val album = Album(
                            track.album.uri.replace("spotify:album:", ""),
                            track.album.name,
                            artists)

                    currentPlaybackProvider = PlaybackProvider.SPOTIFY_REMOTE

                    val trackDto = Track(
                            id = rawTrackId,
                            title = track.name,
                            type = "spotify",
                            durationMillis = track.duration,
                            durationFormatted = formatTimeMillis(track.duration.toInt()),
                            thumbnailUrl = thumbnailUrl,
                            imageUrl = thumbnailUrl,
                            artists = artists,
                            album = album
                    )

                    val memorizedTrack = tracksMemorizer.getMemorizedTrackInstance(trackDto)

                    notifyCurrentPlayingTrackChanged(memorizedTrack)

                    if (!playerState.isPaused) {
                        notifyPlaybackStatusChanged(PlaybackStatus.PLAYING)
                    }
                }
            }
    }

    fun observePlaybackStatus() = playbackStatusSubject

    fun observePlaybackProgress() = playbackProgressSubject

    fun observeCurrentPlayingTrack() = currentPlayingTrackSubject

    fun observeStoppedPlaybacks() = stoppedPlaybacksSubject

    private fun notifyPlaybackStatusChanged(status: PlaybackStatus) {
        if (status == PlaybackStatus.PLAYING && !isStartedForeground) {
            startForeground(NOTIFICATION_ID, notification)
            isStartedForeground = true
        }

        when (status) {
            PlaybackStatus.PLAYING -> {
                remoteViews.setViewVisibility(R.id.playButton, View.INVISIBLE)
                remoteViews.setViewVisibility(R.id.pauseButton, View.VISIBLE)
            }
            PlaybackStatus.PAUSED -> {
                remoteViews.setViewVisibility(R.id.pauseButton, View.INVISIBLE)
                remoteViews.setViewVisibility(R.id.playButton, View.VISIBLE)
            }
        }
        notificationManager.notify(NOTIFICATION_ID, notification)
        playbackStatusSubject.onNext(status)
    }

    private fun notifyPlaybackProgressChanged(progress: Long) {
        playbackProgressSubject.onNext(progress.toInt())
    }

    private fun notifyCurrentPlayingTrackChanged(track: Track) {
        when (track.type) {
            "youtube" -> {
                remoteViews.setTextViewText(R.id.currentPlayingTrack, track.title)
                remoteViews.setTextViewText(R.id.currentOwner, track.owner)

                remoteViews.setViewVisibility(R.id.youtubeIcon, View.VISIBLE)
                remoteViews.setViewVisibility(R.id.spotifyIcon, View.INVISIBLE)
            }
            "spotify" -> {
                remoteViews.setTextViewText(R.id.currentPlayingTrack, track.title + "  |  " + track.artists?.joinToString(", ") { it.name })
                remoteViews.setTextViewText(R.id.currentOwner, track.album?.name )

                remoteViews.setViewVisibility(R.id.spotifyIcon, View.VISIBLE)
                remoteViews.setViewVisibility(R.id.youtubeIcon, View.INVISIBLE)

            }
        }
        notificationManager.notify(NOTIFICATION_ID, notification)
        currentPlayingTrack = track
        currentPlayingTrackSubject.onNext(track)
    }

    private fun notifyYoutubePlaybackStopped(playbackId: Long) {
        stoppedPlaybacksSubject.onNext(playbackId)
    }

    companion object {
        private const val REQUEST_CODE = 1337
        private const val CLIENT_ID = "b5ced1bf973b44e5952919dbbb4c700b"
//        private const val REDIRECT_URI = "https://rockhawke.herokuapp.com/callback/spotify"
//        private const val REDIRECT_URI = "http://10.0.2.2:8080/callback/spotify"
        const val NOTIFICATION_ID = 123
        const val REQUEST_RESUME = 0
        const val REQUEST_PAUSE = 1
        const val REQUEST_PREVIOUS = 2
        const val REQUEST_NEXT = 3

        fun formatTimeMillis(progressMillis: Int): String {
            val seconds = progressMillis / 1000
            val minutes = seconds / 60
            val secondsLeft = seconds % 60
            val secondsLeftFormatted = if (secondsLeft < 10) "0$secondsLeft" else "$secondsLeft"
            return "$minutes:$secondsLeftFormatted"
        }
    }
}