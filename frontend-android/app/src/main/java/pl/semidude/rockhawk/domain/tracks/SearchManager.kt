package pl.semidude.rockhawk.domain.tracks

import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import pl.semidude.rockhawk.data.api.RockhawkApi
import pl.semidude.rockhawk.data.dto.Track
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock

class SearchManager(
    private val rockhawkApi: RockhawkApi,
    private val tracksMemorizer: TracksMemorizer
) {
    private val searchResultsSubject = BehaviorSubject.create<SearchResultsState>()
    private val searchResults = mutableListOf<Track>()
    private val searchLock = ReentrantLock()

    private val disposable: Disposable

    init {
        disposable = tracksMemorizer.observeMemorizedTracks()
            .subscribe {
                searchResultsSubject.onNext(SearchResultsState.Result(searchResults))
            }
    }

    protected fun finalize() {
        disposable.dispose()
    }

    fun observeSearchResults(): Observable<SearchResultsState> = searchResultsSubject

    fun searchForTracks(query: String): Disposable =
        rockhawkApi.searchTracks(query, youtube = true, spotify = true)
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { searchResultsSubject.onNext(SearchResultsState.Loading) }
            .subscribeBy(
                onSuccess = { results ->
                    val memorizedResults = tracksMemorizer.getMemorizedTrackInstances(results)

                    searchLock.withLock {
                        searchResults.clear()
                        searchResults.addAll(memorizedResults)
                    }
                    searchResultsSubject.onNext(SearchResultsState.Result(memorizedResults))
                },
                onError = { throwable ->
                    searchResultsSubject.onNext(SearchResultsState.Error(throwable))
                }
            )

    sealed class SearchResultsState {
        object Loading : SearchResultsState()
        data class Result(val searchResults: List<Track>) : SearchResultsState()
        data class Error(var throwable: Throwable) : SearchResultsState()
    }
}