package pl.semidude.rockhawk.data.api

import okhttp3.Interceptor
import okhttp3.Interceptor.Chain
import okhttp3.Response
import pl.semidude.rockhawk.domain.account.AccountManager

class BearerTokenInterceptor(
    private val accountManager: AccountManager
) : Interceptor {

    override fun intercept(chain: Chain): Response {

        val request = chain.request()

        val encodedPath = request.url().encodedPath()

        if (encodedPath.contains("oauth/token") || encodedPath.contains("api/users"))
            //just proceed chain, since endpoint /oauth/token should be authorized with basic auth, not a bearer token
            return chain.proceed(request)

        val authenticatedRequest =
            if (accountManager.accessToken != null)
                chain.request()
                    .newBuilder()
                    .header("Authorization", "Bearer " + accountManager.accessToken!!)
                    .build()
            else return chain.proceed(request)
//                throw IllegalStateException("no user logged in, but tries to call $encodedPath")

        return chain.proceed(authenticatedRequest)
    }
}