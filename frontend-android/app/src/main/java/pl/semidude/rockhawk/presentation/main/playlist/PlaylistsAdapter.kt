package pl.semidude.rockhawk.presentation.main.playlist

import android.annotation.SuppressLint
import android.content.Context
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.PopupMenu
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_playlist.view.*
import pl.semidude.rockhawk.R
import pl.semidude.rockhawk.data.dto.Playlist
import pl.semidude.rockhawk.helpers.BaseAdapter
import pl.semidude.rockhawk.helpers.inflateView
import pl.semidude.rockhawk.helpers.invisible
import pl.semidude.rockhawk.presentation.main.MainViewModel
import pl.semidude.rockhawk.presentation.main.playlist.PlaylistsAdapter.PlaylistViewHolder

class PlaylistsAdapter(
    private val viewModel: MainViewModel,
    context: Context
) : BaseAdapter<PlaylistViewHolder, Playlist>(context) {

    override fun doCreateViewHolder(parent: ViewGroup, viewType: Int): PlaylistViewHolder {
        val itemView = inflateView(context, R.layout.item_playlist, parent, false)
        return PlaylistViewHolder(itemView)
    }

    override fun doBindViewHolder(viewHolder: PlaylistViewHolder, position: Int) {
        viewHolder.bindViewHolder(items[position])
    }

    inner class PlaylistViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val playlistName: TextView = itemView.playlistName
        private val playlistThumbnail: ImageView = itemView.playlistThumbnail
        private val thumbnailContainer: FrameLayout = itemView.thumbnailContainer
        private val firstInfoField: TextView = itemView.firstInfoField
        private val secondInfoField: TextView = itemView.secondInfoField
        private val thirdInfoField: TextView = itemView.thirdInfoField
        private val firstDivider: ImageView = itemView.circleDivider
        private val secondDivider: ImageView = itemView.circleDivider2
        private val playlistOptionsButton: FrameLayout = itemView.playlistOptionsButton

        @SuppressLint("SetTextI18n")
        fun bindViewHolder(playlist: Playlist) {
            if (playlist.getTracks().isNotEmpty()) {
                thumbnailContainer.setBackgroundResource(R.color.primaryLightColor)
                playlistThumbnail.setPadding(0, 0, 0, 0)
                Glide.with(context).load(playlist.getTracks().first().thumbnailUrl).centerCrop().into(playlistThumbnail)
            }
            else {
                thumbnailContainer.setBackgroundResource(android.R.color.transparent)
                playlistThumbnail.setImageResource(R.drawable.ic_playlist)
                playlistThumbnail.setPadding(16, 16, 16, 16)
            }

            playlistName.text = playlist.playlistName
            firstInfoField.text = playlist.tracksCount.toString() + " songs"
            firstDivider.invisible()

            val popupMenu = PopupMenu(context, playlistOptionsButton).apply {
                setOnMenuItemClickListener { item ->
                    when (item?.itemId) {
                        R.id.deletePlaylist -> {
                            viewModel.deletePlaylist(playlist)
                            true
                        }
                        else -> false
                    }
                }
                menu.add(Menu.NONE, R.id.deletePlaylist, Menu.NONE, "Delete playlist")
            }

            playlistOptionsButton.setOnClickListener { popupMenu.show() }
        }
    }
}