package pl.semidude.rockhawk.data.api

import android.util.Base64
import okhttp3.Interceptor
import okhttp3.Interceptor.Chain
import okhttp3.Response
import pl.semidude.rockhawk.domain.account.AccountManager

class BasicAuthInterceptor : Interceptor

{
    override fun intercept(chain: Chain): Response {

        val request = chain.request()

        if (!request.url().encodedPath().contains("oauth/token"))
            //just proceed chain, since only the /oauth/token should be authorized with basic auth
            return chain.proceed(request)

        val clientCredentials = AccountManager.CLIENT_ID + ":" + AccountManager.CLIENT_SECRET
        val encodedCredentials = Base64.encodeToString(clientCredentials.toByteArray(), Base64.NO_WRAP)

        val authenticatedRequest = request.newBuilder()
            .header("Authorization", "Basic $encodedCredentials")
            .build()

        return chain.proceed(authenticatedRequest)
    }
}