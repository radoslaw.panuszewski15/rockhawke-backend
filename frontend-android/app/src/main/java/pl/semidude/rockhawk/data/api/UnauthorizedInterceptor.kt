package pl.semidude.rockhawk.data.api

import android.content.Context
import okhttp3.Interceptor
import okhttp3.Interceptor.Chain
import okhttp3.Response
import pl.semidude.rockhawk.domain.account.AccountManager
import pl.semidude.rockhawk.helpers.startActivity
import pl.semidude.rockhawk.presentation.login.LoginActivity

class UnauthorizedInterceptor(
    private val context: Context,
    private val accountManager: AccountManager
) : Interceptor {

    companion object {
        private const val CODE_UNAUTHORIZED = 401
    }

    override fun intercept(chain: Chain): Response {

        return chain.proceed(chain.request()).also { response ->
            if (response.code() == CODE_UNAUTHORIZED) {
                context.startActivity<LoginActivity>()
                accountManager.clear()
            }
        }
    }
}
