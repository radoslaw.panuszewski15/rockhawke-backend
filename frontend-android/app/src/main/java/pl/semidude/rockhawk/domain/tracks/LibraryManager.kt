package pl.semidude.rockhawk.domain.tracks

import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import pl.semidude.rockhawk.data.api.RockhawkApi
import pl.semidude.rockhawk.data.dto.Track
import pl.semidude.rockhawk.domain.tracks.LibraryManager.LibraryItemsState.*
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock

class LibraryManager(
    private val rockhawkApi: RockhawkApi,
    private val tracksMemorizer: TracksMemorizer
) {
    private val libraryItemsSubject = BehaviorSubject.create<LibraryItemsState>()
    private val libraryItems: MutableList<Track> = mutableListOf()
    private val libraryLock = ReentrantLock()

    fun observeLibraryItems(): Observable<LibraryItemsState> = libraryItemsSubject

    fun addTrackToLibrary(track: Track): Completable {
        val memorizedTrack = tracksMemorizer.getMemorizedTrackInstance(track)

        memorizedTrack.inLibrary = true

        tracksMemorizer.updateMemorizedTrack(memorizedTrack)

        return rockhawkApi.addTrackToLibrary(memorizedTrack.id, memorizedTrack)
            .subscribeOn(Schedulers.io())
            .doOnSubscribe {
                libraryLock.withLock {
                    libraryItems.add(memorizedTrack)
                }
                libraryItemsSubject.onNext(Result(libraryItems))
            }
    }

    fun removeTrackFromLibrary(track: Track): Completable {
        val memorizedTrack = tracksMemorizer.getMemorizedTrackInstance(track)

        memorizedTrack.inLibrary = false

        tracksMemorizer.updateMemorizedTrack(memorizedTrack)

        return rockhawkApi.removeTrackFromLibrary(memorizedTrack.id)
            .subscribeOn(Schedulers.io())
            .doOnSubscribe {
                libraryLock.withLock {
                    libraryItems.removeIf { it.id == memorizedTrack.id }
                }
                libraryItemsSubject.onNext(Result(libraryItems))
            }
    }

    fun fetchLibraryItems() =
        rockhawkApi.getLibrary()
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { libraryItemsSubject.onNext(Loading) }
            .subscribeBy(
                onSuccess = { items ->
                    items.forEach { tracksMemorizer.updateMemorizedTrack(it) }

                    val memorizedItems = tracksMemorizer.getMemorizedTrackInstances(items)

                    libraryLock.withLock {
                        libraryItems.clear()
                        libraryItems.addAll(memorizedItems)
                    }
                    libraryItemsSubject.onNext(Result(memorizedItems))
                },
                onError = { throwable -> libraryItemsSubject.onNext(Error(throwable)) }
            )

    sealed class LibraryItemsState {
        object Loading : LibraryItemsState()
        data class Result(val libraryItems: List<Track>) : LibraryItemsState()
        data class Error(var throwable: Throwable) : LibraryItemsState()
    }
}