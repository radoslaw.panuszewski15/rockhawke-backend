//package pl.semidude.rockhawke.data.api
//
//import io.reactivex.Completable
//import io.reactivex.Single
//import pl.semidude.rockhawke.data.dto.SpotifyLoginRequest
//import pl.semidude.rockhawke.data.dto.Track
//import java.util.concurrent.TimeUnit
//
//class MockRockhawkeApi : RockhawkApi {
//
//    override fun searchTracks(query: String, userId: String, youtube: Boolean, spotify: Boolean): Single<List<Track>> =
//        Single.fromCallable {
//            listOf(
//                Track("Super muzyczka", "12345", "Opis tej muzyczki, oto ona, oto jej przekaz"),
//                Track("Jakas fajna muzyczka", "54321", "Opis tej fajnej muzyczki")
//            )
//        }
//            .delay(2, TimeUnit.SECONDS)
//
//    override fun loginSpotify(spotifyLoginRequest: SpotifyLoginRequest): Single<String> =
//        throw NotImplementedError("mock api")
//}