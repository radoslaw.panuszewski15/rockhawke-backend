package pl.semidude.rockhawk.domain.playback

import pl.semidude.rockhawk.data.dto.Playback
import java.util.*

class PlaybackQueue {

    private val playbacks: Queue<Playback> = LinkedList()

    fun pushPlayback(playback: Playback) {
        playbacks.add(playback)
    }

    fun popPlayback(): Playback? {
        return playbacks.poll()
    }

    fun clear() {
        playbacks.clear()
    }
}
