package pl.semidude.rockhawk.domain.account

import android.content.SharedPreferences
import pl.semidude.rockhawk.helpers.string
import java.lang.IllegalStateException

class AccountManager(
    private val prefs: SharedPreferences
) {
    companion object {
        const val CLIENT_ID = "android-client"
        const val CLIENT_SECRET = "superpassword"
    }

    var userId: String? by prefs.string("user-id")
    var accessToken: String? by prefs.string("access-token")
    var refreshToken: String? by prefs.string("refresh-token")

    fun isUserLoggedIn() = userId != null

    fun loginUser(userId: String, accessToken: String?, refreshToken: String?) {
        this.userId = userId
        this.accessToken = accessToken
        this.refreshToken = refreshToken
    }

    fun clear() {
        prefs.edit().clear().apply()
    }
}

class UserNotLoggedInException: IllegalStateException("User is not logged in")