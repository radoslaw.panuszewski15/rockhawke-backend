package pl.semidude.rockhawk.data.dto

data class Playback(
    var playbackId: Long,
    var track: Track,
    var s3Url: String?
) {
    companion object {
        fun of(track: Track)
            = Playback(
                playbackId = -1,
                track = track,
                s3Url = track.s3Url
            )
    }
}