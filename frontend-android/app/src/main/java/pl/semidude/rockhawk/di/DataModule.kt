package pl.semidude.rockhawk.di

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module.module
//import pl.semidude.rockhawk.BuildConfig
import pl.semidude.rockhawk.data.api.BasicAuthInterceptor
import pl.semidude.rockhawk.data.api.BearerTokenInterceptor
import pl.semidude.rockhawk.data.api.RockhawkApi
import pl.semidude.rockhawk.data.api.UnauthorizedInterceptor
import pl.semidude.rockhawk.data.time.MoshiLocalDateAdapter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit

//const val BASE_URL = "http://10.0.2.2:8080"
//const val BASE_URL = "https://rockhawke.herokuapp.com"
const val BASE_URL = "http://192.168.1.33:8080"

const val YOUTUBE_PLAYBACKS_ENDPOINT = "$BASE_URL/youtubePlaybacks"

val dataModule = module {

    single<RockhawkApi> {

        val loggingInterceptor = HttpLoggingInterceptor().apply {
            level = if (true) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
        }

        val moshi = Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .add(MoshiLocalDateAdapter())
            .build()

        val httpClient = OkHttpClient.Builder()
            .connectTimeout(2, TimeUnit.MINUTES)
            .readTimeout(2, TimeUnit.MINUTES)
            .writeTimeout(2, TimeUnit.MINUTES)
            .addInterceptor(loggingInterceptor)
            .addInterceptor(BasicAuthInterceptor())
            .addInterceptor(BearerTokenInterceptor(get()))
            .addInterceptor(UnauthorizedInterceptor(androidContext(), get()))
            .build()

        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(httpClient)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .build()
            .create(RockhawkApi::class.java)

//        MockRockhawkeApi()
    }
}