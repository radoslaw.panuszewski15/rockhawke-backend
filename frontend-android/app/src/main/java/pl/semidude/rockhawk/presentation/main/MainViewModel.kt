package pl.semidude.rockhawk.presentation.main

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import io.sellmair.disposer.disposeBy
import io.sellmair.disposer.onDestroy
import pl.semidude.rockhawk.data.api.RockhawkApi
import pl.semidude.rockhawk.data.dto.Playback
import pl.semidude.rockhawk.data.dto.Playlist
import pl.semidude.rockhawk.data.dto.Track
import pl.semidude.rockhawk.domain.account.AccountManager
import pl.semidude.rockhawk.domain.tracks.LibraryManager
import pl.semidude.rockhawk.domain.tracks.LibraryManager.*
import pl.semidude.rockhawk.domain.tracks.PlaylistManager
import pl.semidude.rockhawk.domain.tracks.SearchManager
import pl.semidude.rockhawk.helpers.ViewModelWithDisposers

class MainViewModel(
        private val rockhawkApi: RockhawkApi,
        private val accountManager: AccountManager,
        private val searchManager: SearchManager,
        private val libraryManager: LibraryManager,
        private val playlistManager: PlaylistManager
) : ViewModelWithDisposers() {

    private val playRequestsSubject = PublishSubject.create<PlayRequest>()
    private val youtubeAudioSubject = BehaviorSubject.create<YoutubeAudioState>()

    fun observePlayRequests() = playRequestsSubject
    fun observeYoutubeAudioAvailability() = youtubeAudioSubject
    fun observeSearchResults() = searchManager.observeSearchResults()
    fun observeLibraryItems() = libraryManager.observeLibraryItems()
    fun observePlaylists() = playlistManager.observePlaylists()
    fun observeSpotifyPlaylists() = playlistManager.observeSpotifyPlaylists()

    fun isUserLoggedIn() = accountManager.isUserLoggedIn()

    fun getLibraryItems(): List<Track>
        = (observeLibraryItems().first(LibraryItemsState.Loading)?.blockingGet() as? LibraryItemsState.Result)?.libraryItems
        ?: emptyList()

    fun isInLibrary(track: Track) = getLibraryItems().contains(track)

    fun requestPlaySpotify(track: Track) {
        playRequestsSubject.onNext(PlayRequest.Spotify(track))
    }

    fun requestPlayYoutube(track: Track) {
        if (track.s3Url == null) {
            prepareAndPlayYoutubePlayback(track)
        }
        else {
            playRequestsSubject.onNext(PlayRequest.Youtube(Playback.of(track)))
        }
    }

    private fun prepareAndPlayYoutubePlayback(youtubeTrack: Track) {
        rockhawkApi.prepareYoutubePlayback(youtubeTrack)
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { youtubeAudioSubject.onNext(YoutubeAudioState.Loading(youtubeTrack)) }
            .subscribeBy(
                onSuccess = { playback -> youtubeAudioSubject.onNext(YoutubeAudioState.Result(youtubeTrack, playback)) },
                onError = { throwable -> youtubeAudioSubject.onNext(YoutubeAudioState.Error(throwable)) }
            )
            .disposeBy(onCleared)
    }

    fun prepareYoutubePlayback(youtubeTrack: Track) =
        rockhawkApi.prepareYoutubePlayback(youtubeTrack)
            .subscribeOn(Schedulers.io())

    fun stopYoutubePlayback(playbackId: Long) {
        rockhawkApi.stopYoutubePlayback(playbackId)
            .subscribeOn(Schedulers.io())
            .subscribe()
    }

    fun searchForTracks(query: String) {
        searchManager.searchForTracks(query)
            .disposeBy(onCleared)
    }

    fun fetchLibraryItems() {
        libraryManager.fetchLibraryItems()
            .disposeBy(onCleared)
    }

    fun addTrackToLibrary(track: Track) {
        libraryManager.addTrackToLibrary(track)
            .subscribe()
    }

    fun removeTrackFromLibrary(track: Track) {
        libraryManager.removeTrackFromLibrary(track)
            .subscribe()
    }

    fun fetchPlaylists() {
        playlistManager.fetchPlaylists()
            .disposeBy(onCleared)
    }

    fun fetchSpotifyPlaylists() {
        playlistManager.fetchSpotifyPlaylists()
            .disposeBy(onCleared)
    }

    fun importSpotifyPlaylist(playlistId: String) {
        playlistManager.importSpotifyPlaylist(playlistId)
            .disposeBy(onCleared)
    }

    fun createPlaylist(playlistName: String) {
        playlistManager.createPlaylist(playlistName)
            .disposeBy(onCleared)
    }

    fun addTrackToPlaylist(playlist: Playlist, track: Track) {
        playlistManager.addTrackToPlaylist(playlist, track)
            .disposeBy(onCleared)
    }

    fun removeTrackFromPlaylist(playlist: Playlist, track: Track) {
        playlistManager.removeTrackFromPlaylist(playlist, track)
    }

    fun deletePlaylist(playlist: Playlist) {
        playlistManager.deletePlaylist(playlist)
    }

    fun obtainRecommendedNextTrack(track: Track) =
        rockhawkApi.getRecommendedNextTrack(track.id)
            .subscribeOn(Schedulers.io())

    fun getAccessToken() = accountManager.accessToken

    fun logout() {
        accountManager.clear()
    }

    sealed class PlayRequest {
        data class Spotify(val track: Track) : PlayRequest()
        data class Youtube(val playback: Playback) : PlayRequest()
    }

    sealed class YoutubeAudioState {
        data class Loading(val trackBeingLoaded: Track) : YoutubeAudioState()
        data class Result(val trackBeingLoaded: Track, val playback: Playback) : YoutubeAudioState()
        data class Error(val throwable: Throwable) : YoutubeAudioState()
    }
}