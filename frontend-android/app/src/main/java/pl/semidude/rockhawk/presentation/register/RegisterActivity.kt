package pl.semidude.rockhawk.presentation.register

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.sellmair.disposer.disposeBy
import io.sellmair.disposer.onDestroy
import kotlinx.android.synthetic.main.activity_register.*
import org.koin.android.viewmodel.ext.android.viewModel
import pl.semidude.rockhawk.R
import pl.semidude.rockhawk.helpers.invisible
import pl.semidude.rockhawk.helpers.showToast
import pl.semidude.rockhawk.helpers.startActivity
import pl.semidude.rockhawk.helpers.visible
import pl.semidude.rockhawk.presentation.main.MainActivity
import pl.semidude.rockhawk.presentation.register.RegisterViewModel.RegisterState

class RegisterActivity : AppCompatActivity() {

    private val viewModel by viewModel<RegisterViewModel>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        observeRegisterState()
        listenToUiEvents()
    }

    private fun observeRegisterState() {
        viewModel.observeRegisterState()
            .subscribeOn(AndroidSchedulers.mainThread())
            .subscribe { state ->
                when (state) {
                    is RegisterState.Loading -> {
                        registerProgressBar.visible()
                    }
                    is RegisterState.Registered -> {
                        registerProgressBar.invisible()
                        startActivity<MainActivity>()
                        overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out)
                    }
                    is RegisterState.Error -> {
                        registerProgressBar.invisible()
                        state.throwable.printStackTrace()
//                        showToast("Cannot register user, try later")
                    }
                }
            }
            .disposeBy(onDestroy)
    }

    private fun listenToUiEvents() {
        registerButton.setOnClickListener {
            if (passwordEditText.text.toString() == confirmPasswordEditText.text.toString()) {
                viewModel.registerUser(emailEditText.text.toString(), passwordEditText.text.toString())
            }
            else {
                showToast("Passwords don't match!")
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out)
    }
}
