package pl.semidude.rockhawk.data.dto

data class SpotifyLoginRequest(
    val code: String
)