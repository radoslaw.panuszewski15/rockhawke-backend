package pl.semidude.rockhawk.data.dto

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import pl.semidude.rockhawk.domain.tracks.TracksMemorizer

@Parcelize
data class Playlist(
    var playlistId: String? = null,
    var playlistName: String,
    private var tracks: MutableList<Track>? = mutableListOf(),
    var tracksCount: Int = 0,
    var coverImage: String? = null
) : Parcelable {

    fun getTracks(): List<Track> = tracks ?: emptyList()

    fun containsTrack(track: Track) = tracks?.contains(track) == true

    fun addTrack(track: Track) {
        if (tracks == null) {
            tracks = mutableListOf()
        }
        tracks!!.add(track)
    }

    fun removeTrack(track: Track) {
        tracks?.remove(track)
    }

    fun withMemorizedTrackInstances(tracksMemorizer: TracksMemorizer): Playlist {
        return Playlist(
            playlistId = playlistId,
            playlistName = playlistName,
            tracks = tracksMemorizer.getMemorizedTrackInstances(tracks ?: emptyList()).toMutableList(),
            tracksCount = tracksCount,
            coverImage = coverImage
        )
    }
}