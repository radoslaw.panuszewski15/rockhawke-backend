package pl.semidude.rockhawk.presentation.widget

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.PorterDuff
import android.util.AttributeSet
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.annotation.ColorInt
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.widget.ImageViewCompat
import kotlinx.android.synthetic.main.widget_icon_button.view.*
import pl.semidude.rockhawk.R
import pl.semidude.rockhawk.helpers.gone

import pl.semidude.rockhawk.helpers.inflateView
import pl.semidude.rockhawk.helpers.visible


class IconButton @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    private var hasLeftImage = false
    private var hasRightImage = false

    init {
        inflateView(context, R.layout.widget_icon_button, this, true)
        val attributes = context.obtainStyledAttributes(attrs, R.styleable.IconButton)

        textView.text = attributes.getText(R.styleable.IconButton_text)
        textView.textSize = attributes.getFloat(R.styleable.IconButton_textSize, 14f)
        val textColor = attributes.getColorStateList(R.styleable.IconButton_textColor)
        textView.setTextColor(textColor)
        backgroundLayout.background = attributes.getDrawable(R.styleable.IconButton_background)

        attributes.getResourceId(R.styleable.IconButton_imageLeft, -1).let { resourceId ->
            if (resourceId != -1) {
                val attrImageLeft = AppCompatResources.getDrawable(context, resourceId)
                imageLeft.setImageDrawable(attrImageLeft)
                imageLeft.visible()
                val imageTint = attributes.getColor(R.styleable.IconButton_imageTint, -1)
                if (imageTint != -1) {
                    imageLeft.setTint(imageTint)
                }
                hasLeftImage = true
            }
            else {
                imageLeft.gone()
            }
        }

        attributes.getResourceId(R.styleable.IconButton_imageRight, -1).let { resourceId ->
            if (resourceId != -1) {
                val attrImageRight = AppCompatResources.getDrawable(context, resourceId)
                imageRight.setImageDrawable(attrImageRight)
                imageRight.visible()
                hasRightImage = true
            }
            else {
                imageRight.gone()
            }
        }

        attributes.recycle()
    }

    fun disable() {
        isEnabled = false
        backgroundLayout.isEnabled = false
        imageLeft.gone()
        imageRight.gone()
    }

    fun enable() {
        isEnabled = true
        backgroundLayout.isEnabled = true
        if (hasLeftImage) imageLeft.visible()
        if (hasRightImage) imageRight.visible()
    }
}

fun ImageView.setTint(@ColorInt color: Int?) {
    if (color == null) {
        ImageViewCompat.setImageTintList(this, null)
        return
    }
    ImageViewCompat.setImageTintMode(this, PorterDuff.Mode.SRC_ATOP)
    ImageViewCompat.setImageTintList(this, ColorStateList.valueOf(color))
}