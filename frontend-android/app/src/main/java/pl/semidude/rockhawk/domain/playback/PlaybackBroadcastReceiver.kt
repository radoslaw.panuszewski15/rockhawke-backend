package pl.semidude.rockhawk.domain.playback

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class PlaybackBroadcastReceiver : BroadcastReceiver() {

    private var playbackService: PlaybackService? = null

    override fun onReceive(context: Context, intent: Intent) {
        if (playbackService == null) {
            playbackService = (peekService(
                context,
                Intent(context, PlaybackService::class.java)
            ) as PlaybackService.LocalBinder).getService()
        }

        when (intent.action?.toInt()) {
            PlaybackService.REQUEST_RESUME -> playbackService?.resumePlayback()
            PlaybackService.REQUEST_PAUSE -> playbackService?.pausePlayback()
            PlaybackService.REQUEST_PREVIOUS -> playbackService?.skipPrevious()
            PlaybackService.REQUEST_NEXT -> playbackService?.skipNext()
        }
    }
}