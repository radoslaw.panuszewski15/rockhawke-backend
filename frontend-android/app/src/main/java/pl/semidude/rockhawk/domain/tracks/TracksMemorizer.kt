package pl.semidude.rockhawk.domain.tracks

import io.reactivex.subjects.PublishSubject
import pl.semidude.rockhawk.data.dto.Track

class TracksMemorizer {

    private val memorizedTracksProcessor = PublishSubject.create<List<Track>>()
    private val memorizedTracks = mutableListOf<Track>()

    fun observeMemorizedTracks() = memorizedTracksProcessor

    fun memorizeTrack(track: Track) {
        //TODO add lock because java.util.ConcurrentModificationException...
        if (track.id !in memorizedTracks.map { it.id }) {
            memorizedTracks.add(track)
        }
    }

    fun updateMemorizedTrack(track: Track) {
        val memorized = memorizedTracks.find { it.id == track.id }

        if (memorized != null) {
            memorized.updateFrom(track)
        }
        else {
            memorizeTrack(track)
        }
        memorizedTracksProcessor.onNext(memorizedTracks)
    }

    fun getMemorizedTrackInstance(track: Track): Track {
        val memorized = memorizedTracks.find { it.id == track.id }

        if (memorized != null) {
            return memorized
        }
        else {
            memorizeTrack(track)
            return track
        }
    }

    fun getMemorizedTrackInstances(tracks: List<Track>): List<Track> =
        tracks.map { getMemorizedTrackInstance(it) }
}