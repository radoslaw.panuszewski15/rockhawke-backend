package pl.semidude.rockhawk.presentation.login

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.FacebookSdk
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.spotify.sdk.android.authentication.AuthenticationClient
import com.spotify.sdk.android.authentication.AuthenticationRequest
import com.spotify.sdk.android.authentication.AuthenticationResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.sellmair.disposer.disposeBy
import io.sellmair.disposer.onDestroy
import kotlinx.android.synthetic.main.activity_login.*
import org.koin.android.viewmodel.ext.android.viewModel
import pl.semidude.rockhawk.R
import pl.semidude.rockhawk.helpers.invisible
import pl.semidude.rockhawk.helpers.showToast
import pl.semidude.rockhawk.helpers.startActivity
import pl.semidude.rockhawk.helpers.visible
import pl.semidude.rockhawk.presentation.login.LoginViewModel.LoginState
import pl.semidude.rockhawk.presentation.main.MainActivity
import pl.semidude.rockhawk.presentation.register.RegisterActivity


class LoginActivity : AppCompatActivity() {

    companion object {
        private const val SPOTIFY_CLIENT_ID = "b5ced1bf973b44e5952919dbbb4c700b"
        private const val SPOTIFY_REDIRECT_URI = "https://rockhawke.herokuapp.com/callback/spotify"
        private const val SPOTIFY_AUTH_CODE_REQUEST_CODE = 0x11

//        private const val GOOGLE_CLIENT_ID = "966058153256-a30gq04jt2m3vqtmacglincir47l2cnb.apps.googleusercontent.com"
        private const val GOOGLE_CLIENT_ID = "1087689718655-d0i3mk46iiij4t3jbmcvf7hrn6cfalvu.apps.googleusercontent.com"
        private const val GOOGLE_AUTH_CODE_REQUEST = 0x12
    }

    private val viewModel by viewModel<LoginViewModel>()

    private lateinit var googleSignInClient: GoogleSignInClient
    private lateinit var facebookCallbackManager: CallbackManager


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        initGoogleSignIn()
        initFacebookSignIn()
        observeLoginState()
        listenToUiEvents()
    }

    @SuppressLint("SetTextI18n")
    private fun initGoogleSignIn() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestServerAuthCode(GOOGLE_CLIENT_ID)
            .requestEmail()
            .build()

        googleSignInClient = GoogleSignIn.getClient(this, gso)

        for (i in 0 until googleLoginButton.childCount) {
            val v = googleLoginButton.getChildAt(i)

            if (v is TextView) {
                v.text = "LOGIN WITH GOOGLE"
                return
            }
        }
    }


    private fun initFacebookSignIn() {
        FacebookSdk.setApplicationId("2783455151703098")
        FacebookSdk.setAutoLogAppEventsEnabled(false)
        FacebookSdk.sdkInitialize(this)
        facebookCallbackManager = CallbackManager.Factory.create()

        LoginManager.getInstance().registerCallback(facebookCallbackManager,
            object : FacebookCallback<LoginResult?> {
                override fun onSuccess(loginResult: LoginResult?) {
                    loginResult?.accessToken?.let { facebookToken ->
                        println("Facebook login success, token = ${facebookToken.token}")
                        viewModel.loginFacebook(facebookToken = facebookToken.token)
                    }
                }

                override fun onCancel() {
                    Toast.makeText(this@LoginActivity, "Login Cancel", Toast.LENGTH_LONG).show()
                }

                override fun onError(exception: FacebookException) {
                    Toast.makeText(this@LoginActivity, exception.message, Toast.LENGTH_LONG).show()
                }
            })

        facebookLoginButton.setOnClickListener {
            LoginManager.getInstance().logInWithReadPermissions(
                this@LoginActivity,
                listOf("public_profile", "user_friends", "email")
            )
        }
    }

    private fun observeLoginState() {
        viewModel.observeLoginState()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { state ->
                when (state) {
                    is LoginState.Loading -> {
                        loginProgressBar.visible()
                    }
                    is LoginState.LoggedIn -> {
                        loginProgressBar.invisible()
                        startActivity<MainActivity>()
                        overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out)
                    }
                    is LoginState.Error -> {
                        loginProgressBar.invisible()
                        state.throwable.printStackTrace()
                        showToast("Login failed")
                    }
                }
            }
            .disposeBy(onDestroy)
    }

    private fun listenToUiEvents() {

        loginButton.setOnClickListener {
            viewModel.loginWithCredentials(emailEditText.text.toString(), passwordEditText.text.toString())
        }

        spotifyLoginButton.setOnClickListener {
            val request = getAuthenticationRequest(AuthenticationResponse.Type.CODE)
            AuthenticationClient.openLoginActivity(this, SPOTIFY_AUTH_CODE_REQUEST_CODE, request)
        }

        googleLoginButton.setOnClickListener {
            startActivityForResult(googleSignInClient.signInIntent, GOOGLE_AUTH_CODE_REQUEST)
        }

        registerButton.setOnClickListener {
            startActivity<RegisterActivity>()
            overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out)
        }
    }

    private fun getAuthenticationRequest(type: AuthenticationResponse.Type): AuthenticationRequest {
        return AuthenticationRequest.Builder(SPOTIFY_CLIENT_ID, type, SPOTIFY_REDIRECT_URI)
            .setShowDialog(false)
            .setScopes(arrayOf("user-modify-playback-state", "user-read-playback-state", "user-read-email", "user-library-read"))
            .build()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        facebookCallbackManager.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            SPOTIFY_AUTH_CODE_REQUEST_CODE -> {
                val response = AuthenticationClient.getResponse(resultCode, data)

                if (resultCode == Activity.RESULT_OK) {
                    println("code = ${response.code}")
                    viewModel.loginSpotify(response.code)
                }
                else {
                    showToast("Can't authenticate with Spotify")
                }
            }
            GOOGLE_AUTH_CODE_REQUEST -> {
                try {
                    val account: GoogleSignInAccount? =
                        GoogleSignIn.getSignedInAccountFromIntent(data).result

                    account?.serverAuthCode?.let { code -> viewModel.loginGoogle(code) }
                }
                catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }
}
