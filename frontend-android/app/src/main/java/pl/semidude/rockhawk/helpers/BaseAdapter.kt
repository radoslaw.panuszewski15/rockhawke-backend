package pl.semidude.rockhawk.helpers

import android.content.Context
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView

/**
 * LineItemDetails class for RecyclerView's adapters, just for convenience
 */
abstract class BaseAdapter<TViewHolder: RecyclerView.ViewHolder, TItem> (
    val context: Context
): RecyclerView.Adapter<TViewHolder>() {

    /**
     * Collection of adapted items
     */
    var items = listOf<TItem>()

    /**
     * Callback for item click
     */
    var onItemClickListener: (TItem, TViewHolder) -> Unit = { _, _ -> }

    /**
     * Callback for item long click
     */
    var onItemLongClickListener: (TItem, TViewHolder) -> Boolean = { _, _ -> false }

    /**
     * Operator []
     */
    operator fun get(i: Int) = items[i]

    /**
     * Refresh data set, not suitable for large data sets
     */
    fun loadItems(newItems: List<TItem>, compare: (TItem, TItem) -> Boolean) {
        val oldItems = items
        items = newItems
        notifyChanges(oldItems, newItems, compare)
    }

    /**
     * Simpler overload, no additional list analysis is done. Potentially faster than [loadItems]
     */
    fun loadItems(newItems: List<TItem>) {
        items = newItems
        notifyDataSetChanged()
    }

    /**
     * Remove item from adapter, use it instead of [loadItems] if you care about performance
     */
    fun removeItem(item: TItem) {
        val position = items.indexOf(item)
        items = items.filterNot { it == item }
        notifyItemRemoved(position)
    }

    /**
     * * Add item to adapter, use it instead of [loadItems] if you care about performance
     */
    fun addItem(item: TItem) {
        items += item
        notifyItemInserted(items.indexOf(item))
    }

    private fun notifyChanges(oldList: List<TItem>, newList: List<TItem>, compare: (TItem, TItem) -> Boolean) {
        val diff = DiffUtil.calculateDiff(object : DiffUtil.Callback() {

            override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                return compare(oldList[oldItemPosition], newList[newItemPosition])
            }

            override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                return oldList[oldItemPosition] == newList[newItemPosition]
            }

            override fun getOldListSize() = oldList.size

            override fun getNewListSize() = newList.size
        })

        diff.dispatchUpdatesTo(this)
    }

    /**
     * Called when new MemberViewHolder instance needs to be created
     * Override it instead of [onCreateViewHolder]
     * @param parent The ViewGroup into which the new View will be added after it is bound to
     *               an adapter position.
     * @param viewType The view type of the new View.
     * @return A new MemberViewHolder that holds a View of the given view type
     */
    abstract fun doCreateViewHolder(parent: ViewGroup, viewType: Int): TViewHolder


    /**
     * Called when item needs to be bound to MemberViewHolder
     * Override it instead of [onBindViewHolder]
     * @param viewHolder The MemberViewHolder which should be updated to represent the contents of the
     *                   item at the given position in the data set.
     * @param position The position of the item within the adapter's data set.
     */
    abstract fun doBindViewHolder(viewHolder: TViewHolder, position: Int)


    //==================================================================================================================


    override fun getItemCount() = items.size

    final override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TViewHolder {
        return doCreateViewHolder(parent, viewType)
    }

    final override fun onBindViewHolder(viewHolder: TViewHolder, position: Int) {
        doBindViewHolder(viewHolder, position)

        viewHolder.itemView.setOnClickListener {
            onItemClickListener(items[viewHolder.adapterPosition], viewHolder)
        }

        viewHolder.itemView.setOnLongClickListener {
            onItemLongClickListener(items[viewHolder.adapterPosition], viewHolder)
        }
    }
}