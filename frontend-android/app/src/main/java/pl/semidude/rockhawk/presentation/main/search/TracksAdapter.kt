package pl.semidude.rockhawk.presentation.main.search

import android.content.Context
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.PopupMenu
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_track.view.*
import pl.semidude.rockhawk.R
import pl.semidude.rockhawk.data.dto.Playlist
import pl.semidude.rockhawk.data.dto.Track
import pl.semidude.rockhawk.helpers.*
import pl.semidude.rockhawk.presentation.main.MainViewModel
import pl.semidude.rockhawk.presentation.main.search.TracksAdapter.TrackViewHolder

class TracksAdapter(
    private val viewModel: MainViewModel,
    context: Context,
    private val containingPlaylist: Playlist? = null
) : BaseAdapter<TrackViewHolder, Track>(context) {

    override fun doCreateViewHolder(parent: ViewGroup, viewType: Int): TrackViewHolder {
        val itemView = inflateView(context, R.layout.item_track, parent, false)
        return TrackViewHolder(itemView)
    }

    override fun doBindViewHolder(viewHolder: TrackViewHolder, position: Int) {
        viewHolder.bindTrack(items[position])
    }

    inner class TrackViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val titleView: TextView = itemView.titleView
        private val youtubeThumbnail: ImageView = itemView.youtubeThumbnail
        private val spotifyThumbnail: ImageView = itemView.spotifyThumbnail
        private val youtubeIcon: ImageView = itemView.youtubeIcon
        private val youtubeIconBackground: ImageView = itemView.youtubeIconBackground
        private val spotifyIcon: ImageView = itemView.spotifyIcon
        private val firstInfoField: TextView = itemView.firstInfoField
        private val secondInfoField: TextView = itemView.secondInfoField
        private val thirdInfoField: TextView = itemView.thirdInfoField
        private val firstDivider: ImageView = itemView.circleDivider
        private val secondDivider: ImageView = itemView.circleDivider2
        private val trackOptionsButton: FrameLayout = itemView.trackOptionsButton

        fun bindTrack(track: Track) {
            titleView.text = track.title
            firstInfoField.text = track.durationFormatted

            when (track.type) {
                "youtube" -> bindYoutubeTrack(track)
                "spotify" -> bindSpotifyTrack(track)
            }
            setupLibraryButtons(track)
        }

        private fun bindYoutubeTrack(track: Track) {
            Glide.with(context).load(track.thumbnailUrl).centerCrop().into(youtubeThumbnail)

            secondInfoField.text = track.viewCount!!
            thirdInfoField.text = track.owner

            thirdInfoField.visible()
            secondDivider.visible()

            youtubeIcon.visible()
            youtubeIconBackground.visible()
            youtubeThumbnail.visible()

            spotifyIcon.invisible()
            spotifyThumbnail.gone()
        }

        private fun bindSpotifyTrack(track: Track) {
            Glide.with(context).load(track.thumbnailUrl).centerCrop().into(spotifyThumbnail)

            secondInfoField.text = track.artists?.joinToString(", ") { it.name }

            thirdInfoField.gone()
            secondDivider.gone()

            spotifyIcon.visible()
            spotifyThumbnail.visible()

            youtubeIcon.invisible()
            youtubeIconBackground.invisible()
            youtubeThumbnail.gone()
        }

        private fun setupLibraryButtons(track: Track) {

            val popupMenu = PopupMenu(context, trackOptionsButton).apply {
                setOnMenuItemClickListener { item ->
                    when (item?.itemId) {
                        R.id.addToLibrary -> {
                            viewModel.addTrackToLibrary(track)
                            true
                        }
                        R.id.addToPlaylist -> {
                            true
                        }
                        R.id.removeFromLibrary -> {
                            viewModel.removeTrackFromLibrary(track)
                            true
                        }
                        R.id.removeFromPlaylist -> {
                            viewModel.removeTrackFromPlaylist(containingPlaylist!!, track)
                            true
                        }
                        else -> false
                    }
                }
                if (containingPlaylist == null) {
                    if (!viewModel.isInLibrary(track)) {
                        menu.add(Menu.NONE, R.id.addToLibrary, Menu.NONE, "Add to library")
                    }
                    else {
                        menu.add(Menu.NONE, R.id.removeFromLibrary, Menu.NONE, "Remove from library")
                    }
//                    menu.add(Menu.NONE, R.id.addToPlaylist, Menu.NONE, "Add to playlist...")
                }
                else {
                    menu.add(Menu.NONE, R.id.removeFromPlaylist, Menu.NONE, "Remove from ${containingPlaylist.playlistName}")
                }
            }
            trackOptionsButton.setOnClickListener { popupMenu.show() }
        }
    }
}