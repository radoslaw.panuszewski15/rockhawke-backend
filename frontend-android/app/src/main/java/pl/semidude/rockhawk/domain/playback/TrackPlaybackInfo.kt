package pl.semidude.rockhawk.domain.playback

data class TrackPlaybackInfo(
    var title: String,
    var type: TrackType,
    var durationMillis: Long,
    var durationFormatted: String,
    var imageUrl: String
)

enum class TrackType { YOUTUBE, SPOTIFY }