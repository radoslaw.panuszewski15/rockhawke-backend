package pl.semidude.rockhawk.presentation.main.playlist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.PopupMenu
import androidx.fragment.app.Fragment
import com.google.android.material.bottomsheet.BottomSheetBehavior
import io.reactivex.android.schedulers.AndroidSchedulers
import io.sellmair.disposer.disposeBy
import io.sellmair.disposer.onDestroy
import kotlinx.android.synthetic.main.fragment_playlists.*
import kotlinx.android.synthetic.main.top_bar.*
import org.koin.android.viewmodel.ext.android.viewModel
import pl.semidude.rockhawk.R
import pl.semidude.rockhawk.domain.tracks.PlaylistManager
import pl.semidude.rockhawk.helpers.*
import pl.semidude.rockhawk.presentation.login.LoginActivity
import pl.semidude.rockhawk.presentation.main.MainViewModel
import kotlin.concurrent.thread

class PlaylistsFragment : Fragment() {

    private val viewModel: MainViewModel by viewModel()
    private lateinit var playlistsAdapter: PlaylistsAdapter
    private lateinit var playlistsFromSpotifyAdapter: PlaylistsFromSpotifyAdapter
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<View>

    companion object {
        fun newInstance() = PlaylistsFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_playlists, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupTopBar()
        initRecyclers()
        initOptionsButton()
        initFloatingButton()
        observePlaylists()
        observeSpotifyPlaylists()
        viewModel.fetchPlaylists()
    }

    private fun setupTopBar() {
        subtitleTopBar.text = "Playlists"
        circleDivider.visible()
        subtitleTopBar.visible()
    }

    private fun initRecyclers() {
        playlistsAdapter = PlaylistsAdapter(viewModel, activity!!)
        playlistsAdapter.onItemClickListener = { playlist, _ ->
            activity!!.supportFragmentManager.beginTransaction()
                .addToBackStack("ENTER_PLAYLIST")
                .replace(R.id.playlistDetailsContainer, PlaylistDetailsFragment.newInstance(playlist))
                .commit()
        }
        playlistsRecycler.apply {
            adapter = playlistsAdapter
            layoutManager = createVerticalLayoutManager(context)
        }

        playlistsFromSpotifyAdapter = PlaylistsFromSpotifyAdapter(viewModel, activity!!)
        playlistsFromSpotifyRecycler.apply {
            adapter = playlistsFromSpotifyAdapter
            layoutManager = createVerticalLayoutManager(context)
        }
    }

    private fun initOptionsButton() {
        userOptionsButton.setOnClickListener {
            PopupMenu(activity!!, userOptionsButton).apply {
                setOnMenuItemClickListener { item ->
                    when (item?.itemId) {
                        R.id.logout -> {
                            viewModel.logout()
                            startActivity<LoginActivity>()
                            true
                        }
                        else -> false
                    }
                }
                inflate(R.menu.menu_user)
                show()
            }
        }
    }

    private fun initFloatingButton() {
        openPlaylistCreatorButton.setOnClickListener {
            createPlaylistContainer.visible()
            importPlaylistFromSpotifyContainer.invisible()
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
            fabMenu.close(true)
        }

        openImportingFromSpotifyButton.setOnClickListener {
            importPlaylistFromSpotifyContainer.visible()
            createPlaylistContainer.invisible()
            viewModel.fetchSpotifyPlaylists()
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
            fabMenu.close(true)
        }

        createPlaylistButton.setOnClickListener {
            viewModel.createPlaylist(playlistNameEditText.text.toString())
            playlistNameEditText.text.clear()
            activity?.hideKeyboard()
            thread {
                Thread.sleep(200)
                activity?.runOnUiThread { bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN }
            }
        }

        closeButton1.setOnClickListener { bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN }
        closeButton2.setOnClickListener { bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN }

        bottomSheetBehavior = BottomSheetBehavior.from(newPlaylistContainer)
        bottomSheetBehavior.isHideable = true
        bottomSheetBehavior.peekHeight = 415
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
    }

    private fun observePlaylists() {
        viewModel.observePlaylists()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { playlistsState ->
                when (playlistsState) {
                    is PlaylistManager.PlaylistsState.Loading -> {
                        fetchProgressBar.visible()
                        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                    }
                    is PlaylistManager.PlaylistsState.Result -> {
                        fetchProgressBar.invisible()
                        playlistsAdapter.loadItems(playlistsState.playlists.values.toList())
                    }
                    is PlaylistManager.PlaylistsState.Error -> {
                        showToast(playlistsState.throwable.message ?: "Unknown error occurred while fetching playlists")
                    }
                }
            }
            .disposeBy(onDestroy)
    }

    private fun observeSpotifyPlaylists() {
        viewModel.observeSpotifyPlaylists()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { spotifyPlaylistsState ->
                when (spotifyPlaylistsState) {
                    is PlaylistManager.PlaylistsState.Loading -> {
                        if (playlistsFromSpotifyAdapter.items.isEmpty()) {
                            spotifyPlaylistsProgressBar.visible()
                        }
                    }
                    is PlaylistManager.PlaylistsState.Result -> {
                        spotifyPlaylistsProgressBar.invisible()
                        playlistsFromSpotifyAdapter.loadItems(spotifyPlaylistsState.playlists.values.toList())
                    }
                    is PlaylistManager.PlaylistsState.Error -> {
                        showToast(spotifyPlaylistsState.throwable.message ?: "Unknown error occurred while fetching Spotify playlists")
                    }
                }
            }
            .disposeBy(onDestroy)
    }
}