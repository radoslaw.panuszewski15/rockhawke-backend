package pl.semidude.rockhawk.presentation.main.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.fragment.app.Fragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.sellmair.disposer.disposeBy
import io.sellmair.disposer.onDestroy
import kotlinx.android.synthetic.main.fragment_search.*
import org.koin.android.viewmodel.ext.android.sharedViewModel
import pl.semidude.rockhawk.R
import pl.semidude.rockhawk.data.dto.Playback
import pl.semidude.rockhawk.domain.tracks.SearchManager.SearchResultsState
import pl.semidude.rockhawk.helpers.*
import pl.semidude.rockhawk.presentation.main.MainViewModel


class SearchFragment : Fragment() {

    private val viewModel: MainViewModel by sharedViewModel()
    private lateinit var tracksAdapter: TracksAdapter

    companion object {
        fun newInstance() = SearchFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUi()
        observeSearchResults()
        listenToUiEvents()
    }

    private fun initUi() {
        tracksAdapter = TracksAdapter(viewModel, context!!)

        tracksRecycler.apply {
            adapter = tracksAdapter
            layoutManager = createVerticalLayoutManager(context)
        }
    }

    private fun observeSearchResults() {
        viewModel.observeSearchResults()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { state ->
                when (state) {
                    is SearchResultsState.Loading -> {
                        searchProgressBar.visible()
                        tracksAdapter.loadItems(emptyList())
                    }

                    is SearchResultsState.Result -> {
                        searchProgressBar.invisible()
                        tracksAdapter.loadItems(state.searchResults)

                    }
                    is SearchResultsState.Error ->
                        showToast("${state.throwable}")
                }
            }
            .disposeBy(onDestroy)
    }

    private fun listenToUiEvents() {
        searchEditText.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                viewModel.searchForTracks(searchEditText.text.toString())
            }
            false
        }

        searchButton.setOnClickListener {
            if (searchEditText.text.toString().isNotEmpty()) {
                viewModel.searchForTracks(searchEditText.text.toString())
                activity?.hideKeyboard()
            }
        }

        tracksAdapter.onItemClickListener = { track, viewHolder ->
            when (track.type) {
                "spotify" -> {
                    viewModel.requestPlaySpotify(track)
                }
                "youtube" -> {
                    viewModel.requestPlayYoutube(track)
                }
            }
        }
    }
}