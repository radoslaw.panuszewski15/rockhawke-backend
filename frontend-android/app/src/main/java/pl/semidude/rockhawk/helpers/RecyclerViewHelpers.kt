package pl.semidude.rockhawk.helpers

import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

fun createVerticalLayoutManager(context: Context)
    = LinearLayoutManager(context, RecyclerView.VERTICAL, false)

fun createHorizontalLayoutManager(context: Context)
    = LinearLayoutManager(context, RecyclerView.VERTICAL, false)