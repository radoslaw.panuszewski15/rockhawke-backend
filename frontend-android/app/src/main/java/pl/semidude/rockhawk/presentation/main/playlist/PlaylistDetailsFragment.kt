package pl.semidude.rockhawk.presentation.main.playlist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.material.bottomsheet.BottomSheetBehavior
import io.reactivex.android.schedulers.AndroidSchedulers
import io.sellmair.disposer.disposeBy
import io.sellmair.disposer.onDestroy
import kotlinx.android.synthetic.main.fragment_playlist_details.*
import kotlinx.android.synthetic.main.top_bar.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.sharedViewModel
import pl.semidude.rockhawk.R
import pl.semidude.rockhawk.data.dto.Playback
import pl.semidude.rockhawk.data.dto.Playlist
import pl.semidude.rockhawk.data.dto.Track
import pl.semidude.rockhawk.domain.playback.PlaybackQueue
import pl.semidude.rockhawk.domain.tracks.LibraryManager
import pl.semidude.rockhawk.domain.tracks.PlaylistManager
import pl.semidude.rockhawk.helpers.createVerticalLayoutManager
import pl.semidude.rockhawk.helpers.invisible
import pl.semidude.rockhawk.helpers.showToast
import pl.semidude.rockhawk.helpers.visible
import pl.semidude.rockhawk.presentation.main.MainViewModel
import pl.semidude.rockhawk.presentation.main.search.ProposedTracksAdapter
import pl.semidude.rockhawk.presentation.main.search.TracksAdapter

class PlaylistDetailsFragment : Fragment() {

    private val viewModel: MainViewModel by sharedViewModel()
    private val playbackQueue: PlaybackQueue by inject()
    private lateinit var tracksAdapter: TracksAdapter
    private lateinit var proposedTracksAdapter: ProposedTracksAdapter
    private val playlist: Playlist get() = arguments?.getParcelable(ARG_PLAYLIST)!!
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<View>

    companion object {
        const val ARG_PLAYLIST = "playlist"

        fun newInstance(playlist: Playlist): PlaylistDetailsFragment {
            val fragment = PlaylistDetailsFragment()
            val bundle = Bundle().apply {
                putParcelable(ARG_PLAYLIST, playlist)
            }
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_playlist_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupTopBar()
        initRecyclerView()
        initBottomSheet()
        observePlaylistItems()
    }

    private fun setupTopBar() {
        subtitleTopBar.text = playlist.playlistName
        circleDivider.visible()
        subtitleTopBar.visible()
    }

    private fun initRecyclerView() {
        tracksAdapter = TracksAdapter(viewModel, activity!!, playlist).apply {
            onItemClickListener = {track, _ -> requestPlay(track) }
        }
        tracksRecycler.apply {
            adapter = tracksAdapter
            layoutManager = createVerticalLayoutManager(context)
        }

        proposedTracksAdapter = ProposedTracksAdapter(viewModel, playlist, activity!!).apply {
            onItemClickListener = {track, _ -> requestPlay(track) }
        }
        proposedTracksRecycler.apply {
            adapter = proposedTracksAdapter
            layoutManager = createVerticalLayoutManager(context)
        }

        proposedTracksAdapter.loadItems(viewModel.getLibraryItems())
    }

    private fun requestPlay(track: Track) {
        val startIndex = playlist.getTracks().indexOf(track)
        playbackQueue.clear()
        playlist.getTracks()
            .filterIndexed { index, _ -> index > startIndex }
            .map { Playback.of(it) }
            .forEach(playbackQueue::pushPlayback)

        when (track.type) {
            "spotify" -> viewModel.requestPlaySpotify(track)
            "youtube" -> viewModel.requestPlayYoutube(track)
        }
    }

    private fun initBottomSheet() {
        bottomSheetBehavior = BottomSheetBehavior.from(proposedTracksContainer)
        bottomSheetBehavior.peekHeight = 140
        bottomSheetBehavior.halfExpandedRatio = 0.5f
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
    }

    private fun observePlaylistItems() {
        viewModel.observePlaylists()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { playlistsState ->
                when (playlistsState) {
                    is PlaylistManager.PlaylistsState.Loading -> { }
                    is PlaylistManager.PlaylistsState.Result -> {
                        playlistsState.playlists[playlist.playlistName]?.getTracks()?.let { playlistTracks ->
                            if (playlistTracks.isNotEmpty()) {
                                tracksRecycler.visible()
                                noTracksInfo.invisible()
                                iconMusicNote.invisible()
                            } else {
                                tracksRecycler.invisible()
                                noTracksInfo.visible()
                                iconMusicNote.visible()
                            }
                            bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
                            tracksAdapter.loadItems(playlistTracks)
                        }
                    }
                    is PlaylistManager.PlaylistsState.Error -> {
                        showToast(playlistsState.throwable.message ?: "Unknown error occurred while obtaining playlist items")
                    }
                }
            }
            .disposeBy(onDestroy)
    }
}