package pl.semidude.rockhawk.data.dto

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.lang.IllegalArgumentException

@Parcelize
data class Track(
    var title: String,
    var id: String,
    var durationMillis: Long,
    var durationFormatted: String,
    var type: String,
    var thumbnailUrl: String,
    var imageUrl: String,
    var initialized: Boolean = true,
    var inLibrary: Boolean = false,
    var description: String? = null,
    var owner: String? = null,
    var viewCount: String? = null,
    var isLoading: Boolean = false,
    var artists: List<Artist>? = null,
    var album: Album? = null,
    var s3Url: String? = null
) : Parcelable {
    fun updateFrom(track: Track) {
        if (id != track.id)
            throw IllegalArgumentException("cannot update track of id $id from track of id ${track.id}")

        title = track.title
        durationMillis = track.durationMillis
        durationFormatted = track.durationFormatted
        type = track.type
        thumbnailUrl = track.thumbnailUrl
        imageUrl = track.imageUrl
        initialized = track.initialized
        inLibrary = track.inLibrary
        description = track.description
        owner = track.owner
        viewCount = track.viewCount
        isLoading = track.isLoading
        artists = track.artists
        album = track.album
        s3Url = track.s3Url
    }
}