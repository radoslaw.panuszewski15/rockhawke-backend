package pl.semidude.rockhawk.data.api

import io.reactivex.Completable
import io.reactivex.Single
import pl.semidude.rockhawk.data.dto.TokenResponse
import pl.semidude.rockhawk.data.dto.Playback
import pl.semidude.rockhawk.data.dto.Playlist
import pl.semidude.rockhawk.data.dto.Track
import retrofit2.http.*

interface RockhawkApi {

    @GET("/search")
    fun searchTracks(
        @Query("query") query: String,
        @Query("youtube") youtube: Boolean = false,
        @Query("spotify") spotify: Boolean = false
    ): Single<List<Track>>

    @POST("/oauth/token")
    @FormUrlEncoded
    fun loginWithCredentials(
        @Field("username") email: String,
        @Field("password") password: String,
        @Field("grant_type") grantType: String = "password",
        @Field("scope") scope: String = "read write"
    ): Single<TokenResponse>

    @POST("/oauth/token")
    @FormUrlEncoded
    fun loginSpotify(
        @Field("spotify_code") spotifyCode: String,
        @Field("grant_type") grantType: String = "password",
        @Field("scope") scope: String = "read write"
    ): Single<TokenResponse>

    @POST("/oauth/token")
    @FormUrlEncoded
    fun loginGoogle(
        @Field("google_code") googleCode: String,
        @Field("grant_type") grantType: String = "password",
        @Field("scope") scope: String = "read write"
    ): Single<TokenResponse>

    @POST("/oauth/token")
    @FormUrlEncoded
    fun loginFacebook(
        @Field("facebook_token") facebookToken: String,
        @Field("grant_type") grantType: String = "password",
        @Field("scope") scope: String = "read write"
    ): Single<TokenResponse>

    @POST("/users")
    @FormUrlEncoded
    fun registerUser(
        @Field("email") user: String,
        @Field("password") password: String,
        @Field("nickname") nickname: String? = null
    ): Completable

    @POST("/youtubePlaybacks")
    fun prepareYoutubePlayback(
        @Body youtubeTrack: Track
    ): Single<Playback>

    @DELETE("/youtubePlaybacks/{playbackId}")
    fun stopYoutubePlayback(
        @Path("playbackId") playbackId: Long
    ): Completable

    @GET("/me/library")
    fun getLibrary(): Single<List<Track>>

    @PUT("/me/library/{trackId}")
    fun addTrackToLibrary(
        @Path("trackId") trackId: String,
        @Body track: Track
    ): Completable

    @DELETE("/me/library/{trackId}")
    fun removeTrackFromLibrary(
        @Path("trackId") trackId: String
    ): Completable

    @GET("/tracks/{trackId}/recommendedNext")
    fun getRecommendedNextTrack(
        @Path("trackId") trackId: String
    ): Single<Track>

    @GET("/me/playlists")
    fun getPlaylists(): Single<List<Playlist>>

    @GET("/me/playlists/spotify")
    fun getSpotifyPlaylists(): Single<List<Playlist>>

    @POST("/me/playlists/spotify/{playlistId}")
    fun importSpotifyPlaylist(
        @Path("playlistId") playlistId: String
    ): Single<Playlist>

    @POST("/me/playlists")
    fun createPlaylist(
        @Body playlist: Playlist
    ): Single<Playlist>

    @DELETE("/playlists/{playlistId}")
    fun deletePlaylist(
        @Path("playlistId") playlistId: String
    ): Completable

    @PATCH("/playlists/{playlistId}")
    fun addTrackToPlaylist(
        @Path("playlistId") playlistId: String,
        @Body track: Track
    ): Single<Playlist>

    @DELETE("/playlists/{playlistId}/{trackId}")
    fun removeTrackFromPlaylist(
        @Path("playlistId") playlistId: String,
        @Path("trackId") trackId: String
    ): Completable
}