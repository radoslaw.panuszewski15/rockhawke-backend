package pl.semidude.rockhawk.presentation.login

import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.sellmair.disposer.disposeBy
import pl.semidude.rockhawk.data.api.RockhawkApi
import pl.semidude.rockhawk.domain.account.AccountManager
import pl.semidude.rockhawk.helpers.ViewModelWithDisposers

class LoginViewModel(
    private val rockhawkApi: RockhawkApi,
    private val accountManager: AccountManager
) : ViewModelWithDisposers() {

    private val loginStateSubject = BehaviorSubject.create<LoginState>()

    fun observeLoginState() = loginStateSubject

    fun loginWithCredentials(email: String, password: String) {
        rockhawkApi.loginWithCredentials(email, password)
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { loginStateSubject.onNext(LoginState.Loading) }
            .subscribeBy(
                onSuccess = { tokenResponse ->

                    with(tokenResponse) {

                        accountManager.loginUser(userId, accessToken, refreshToken)

                        loginStateSubject.onNext(LoginState.LoggedIn)
                    }
                },
                onError = { throwable -> loginStateSubject.onNext(LoginState.Error(throwable)) }
            )
            .disposeBy(onCleared)
    }

    fun loginSpotify(code: String) {
        rockhawkApi.loginSpotify(code)
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { loginStateSubject.onNext(LoginState.Loading) }
            .subscribeBy(
                onSuccess = { tokenResponse ->
                    with(tokenResponse) {
                        accountManager.loginUser(userId, accessToken, refreshToken)
                        loginStateSubject.onNext(LoginState.LoggedIn)
                    }
                },
                onError = { throwable -> loginStateSubject.onNext(LoginState.Error(throwable)) }
            )
            .disposeBy(onCleared)
    }

    fun loginGoogle(code: String) {
        rockhawkApi.loginGoogle(code)
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { loginStateSubject.onNext(LoginState.Loading) }
            .subscribeBy(
                onSuccess = { tokenResponse ->
                    with(tokenResponse) {
                        accountManager.loginUser(userId, accessToken, refreshToken)
                        loginStateSubject.onNext(LoginState.LoggedIn)
                    }
                },
                onError = { throwable -> loginStateSubject.onNext(LoginState.Error(throwable)) }
            )
            .disposeBy(onCleared)
    }

    fun loginFacebook(facebookToken: String) {
        rockhawkApi.loginFacebook(facebookToken)
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { loginStateSubject.onNext(LoginState.Loading) }
            .subscribeBy(
                onSuccess = { tokenResponse ->
                    with(tokenResponse) {
                        accountManager.loginUser(userId, accessToken, refreshToken)
                        loginStateSubject.onNext(LoginState.LoggedIn)
                    }
                },
                onError = { throwable -> loginStateSubject.onNext(LoginState.Error(throwable)) }
            )
            .disposeBy(onCleared)
    }

    sealed class LoginState {
        object Loading : LoginState()
        object LoggedIn : LoginState()
        data class Error(val throwable: Throwable) : LoginState()
    }
}