package pl.semidude.rockhawk

import android.app.Application
import org.koin.android.ext.android.startKoin
import pl.semidude.rockhawk.di.appModule
import pl.semidude.rockhawk.di.dataModule

class App: Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin(this, listOf(appModule, dataModule))
    }
}