package pl.semidude.rockhawk.presentation.main

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import android.view.Window.ID_ANDROID_CONTENT
import android.widget.SeekBar
import androidx.appcompat.app.AppCompatActivity
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import io.reactivex.android.schedulers.AndroidSchedulers
import io.sellmair.disposer.disposeBy
import io.sellmair.disposer.onDestroy
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import pl.semidude.rockhawk.R
import pl.semidude.rockhawk.data.dto.Track
import pl.semidude.rockhawk.domain.playback.PlaybackQueue
import pl.semidude.rockhawk.domain.playback.PlaybackService
import pl.semidude.rockhawk.domain.tracks.TracksMemorizer
import pl.semidude.rockhawk.helpers.*
import pl.semidude.rockhawk.presentation.login.LoginActivity
import pl.semidude.rockhawk.presentation.main.MainViewModel.PlayRequest
import pl.semidude.rockhawk.presentation.main.MainViewModel.YoutubeAudioState
import pl.semidude.rockhawk.presentation.player.PlayerActivity


class MainActivity : AppCompatActivity() {

    private val viewModel: MainViewModel by viewModel()
    private val tracksMemorizer: TracksMemorizer by inject()
    private val playbackQueue: PlaybackQueue by inject()
    private var playerBlocked = true
    private lateinit var playbackService: PlaybackService
    private var keyboardListenersAttached = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (!viewModel.isUserLoggedIn()) {
            startActivity<LoginActivity>()
            return
        }

        setContentView(R.layout.activity_main)
        supportFragmentManager
            .beginTransaction()
            .show(homeFragment)
            .hide(searchFragment)
            .hide(playlistFragment)
            .commit()
        observeYoutubeAudioAvailability()
        listenToUiEvents()
        bindPlaybackService()
        attachKeyboardListeners()
    }

    private fun observeYoutubeAudioAvailability() {
        viewModel.observeYoutubeAudioAvailability()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { state ->
                when (state) {
                    is YoutubeAudioState.Loading -> {
                        val track = state.trackBeingLoaded

                        if (!track.initialized) {
                            track.isLoading = true
                            currentPlayingTextView.invisible()
                            audioAvailabilityProgressBar.visible()
                            playerBlocked = true
                        }
                    }
                    is YoutubeAudioState.Result -> {
                        val track = state.trackBeingLoaded
                        track.isLoading = false
                        track.initialized = true
                        audioAvailabilityProgressBar.invisible()
                        currentPlayingTextView.visible()
                        playerBlocked = false
                        playbackService.playYoutube(state.playback)
                    }
                    is YoutubeAudioState.Error -> {

                    }
                }
            }
            .disposeBy(onDestroy)
    }

    private fun listenToUiEvents() {
        listenToClicks()
        listenToSeekBarEvents()
    }

    private fun listenToClicks() {
        playButton.setOnClickListener {
            playbackService.resumePlayback()
        }

        pauseButton.setOnClickListener {
            playbackService.pausePlayback()
        }

        addToLibraryButton.setOnClickListener {
            playbackService.currentPlayingTrack?.let { track ->
                viewModel.addTrackToLibrary(track)
                addToLibraryButton.invisible()
                removeFromLibraryButton.visible()
            }
        }

        removeFromLibraryButton.setOnClickListener {
            playbackService.currentPlayingTrack?.let { track ->
                viewModel.removeTrackFromLibrary(track)
                removeFromLibraryButton.invisible()
                addToLibraryButton.visible()
            }
        }

        currentPlayingContainer.setOnClickListener {
            if (!playerBlocked) {
                startActivity<PlayerActivity>()
            }
        }

        checkBackgroundHome.setOnClickListener {
            playlistDetailsContainer.invisible()
            supportFragmentManager
                .beginTransaction()
                .show(homeFragment)
                .hide(searchFragment)
                .hide(playlistFragment)
                .commit()
            checkBackgroundHome.setBackgroundResource(R.color.primaryColor)
            checkBackgroundSearch.setBackgroundResource(R.color.primaryDarkColor)
            checkBackgroundPlaylists.setBackgroundResource(R.color.primaryDarkColor)
        }

        checkBackgroundSearch.setOnClickListener {
            playlistDetailsContainer.invisible()
            supportFragmentManager
                .beginTransaction()
                .show(searchFragment)
                .hide(homeFragment)
                .hide(playlistFragment)
                .commit()
            checkBackgroundSearch.setBackgroundResource(R.color.primaryColor)
            checkBackgroundHome.setBackgroundResource(R.color.primaryDarkColor)
            checkBackgroundPlaylists.setBackgroundResource(R.color.primaryDarkColor)
        }

        checkBackgroundPlaylists.setOnClickListener {
            playlistDetailsContainer.visible()
            supportFragmentManager
                .beginTransaction()
                .show(playlistFragment)
                .hide(homeFragment)
                .hide(searchFragment)
                .commit()
            checkBackgroundPlaylists.setBackgroundResource(R.color.primaryColor)
            checkBackgroundHome.setBackgroundResource(R.color.primaryDarkColor)
            checkBackgroundSearch.setBackgroundResource(R.color.primaryDarkColor)
        }
    }

    private fun listenToSeekBarEvents() {
        musicSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onStopTrackingTouch(seekBar: SeekBar) {
                playbackService.seekTo(seekBar.progress)
            }
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {}
            override fun onStartTrackingTouch(seekBar: SeekBar?) {}
        })
    }

    private fun bindPlaybackService() {
        val connection = object : ServiceConnection {
            override fun onServiceConnected(className: ComponentName, service: IBinder) {
                playbackService = (service as PlaybackService.LocalBinder).getService()
                playbackService.setAccessToken(viewModel.getAccessToken()!!)
                playbackService.setPlaybackQueue(playbackQueue)
                playbackService.setTracksMemorizer(tracksMemorizer)

                observePlayRequests()
                observePlaybackService()
            }
            override fun onServiceDisconnected(arg0: ComponentName) {}
        }
        Intent(this, PlaybackService::class.java).also { intent ->
            bindService(intent, connection, Context.BIND_AUTO_CREATE)
        }
    }

    private fun observePlaybackService() {
        observePlaybackStatus()

        observePlaybackProgress()

        observeCurrentPlayingTrack()

        observeStoppedPlaybacks()
    }

    private fun observePlaybackStatus() {
        playbackService.observePlaybackStatus()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { status ->
                when (status!!) {
                    PlaybackService.PlaybackStatus.PLAYING -> {
                        playButton.invisible()
                        pauseButton.visible()
                    }
                    PlaybackService.PlaybackStatus.PAUSED -> {
                        pauseButton.invisible()
                        playButton.visible()
                    }
                }
            }
            .disposeBy(onDestroy)
    }

    private fun observePlaybackProgress() {
        playbackService.observePlaybackProgress()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { progress ->
                musicSeekBar.progress = progress
//                currentProgressTextView.text = PlaybackService.formatTimeMillis(progress)
            }
            .disposeBy(onDestroy)
    }

    private fun observeCurrentPlayingTrack() {
        playbackService.observeCurrentPlayingTrack()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { track ->
                when (track.type) {
                    "youtube" -> {
                        if (currentPlayingTextView.text != track.title) {
                            musicSeekBar.progress = 0
                            scheduleNextTrack(track)
                        }
                        currentPlayingTextView.text = track.title
                    }
                    "spotify" -> {
                        currentPlayingTextView.text = track.title + "  |  " + track?.artists?.joinToString(", ") { it.name }

                        if (currentPlayingTextView.text != track.title + "  |  " + track?.artists?.joinToString(", ") { it.name }) {
                            musicSeekBar.progress = 0
                        }
                    }
                }

                musicSeekBar.max = track.durationMillis.toInt()

                if (track.inLibrary) {
                    addToLibraryButton.invisible()
                    removeFromLibraryButton.visible()
                } else {
                    removeFromLibraryButton.invisible()
                    addToLibraryButton.visible()
                }


                playerBlocked = false
            }
            .disposeBy(onDestroy)
    }

    private fun observeStoppedPlaybacks() {
        playbackService.observeStoppedPlaybacks()
            .subscribe { playbackId ->
                viewModel.stopYoutubePlayback(playbackId)
            }
            .disposeBy(onDestroy)
    }

    private fun observePlayRequests() {
        viewModel.observePlayRequests()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { playRequest ->
                when (playRequest) {
                    is PlayRequest.Spotify -> playbackService.playSpotify(playRequest.track)
                    is PlayRequest.Youtube -> playbackService.playYoutube(playRequest.playback)
                }
            }
            .disposeBy(onDestroy)
    }

    private fun scheduleNextTrack(previousTrack: Track) {
        viewModel.obtainRecommendedNextTrack(previousTrack)
            .flatMap { nextTrack ->
                viewModel.prepareYoutubePlayback(nextTrack)
            }
            .subscribe { nextPlayback ->
                playbackQueue.pushPlayback(nextPlayback)
            }
            .disposeBy(onDestroy)
    }

    override fun onStart() {
        super.onStart()
        if (::playbackService.isInitialized) {
            playbackService.connectToSpotify()
        }
        hideKeyboard()
    }

    override fun onDestroy() {
        super.onDestroy()
        playbackService.stopSpotify()
        playbackService.disconnectFromSpotify()
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            super.onBackPressed()
        }
    }

    private fun attachKeyboardListeners() {
        if (keyboardListenersAttached) {
            return
        }
        rootLayout.viewTreeObserver.addOnGlobalLayoutListener(OnGlobalLayoutListener {
            val heightDiff: Int = rootLayout.rootView.height - rootLayout.height
            val contentViewTop = window.findViewById<View>(ID_ANDROID_CONTENT).top
            val broadcastManager = LocalBroadcastManager.getInstance(this)
            if (heightDiff <= contentViewTop) {
                onHideKeyboard()
                val intent = Intent("KeyboardWillHide")
                broadcastManager.sendBroadcast(intent)
            } else {
                val keyboardHeight = heightDiff - contentViewTop
                onShowKeyboard(keyboardHeight)
                val intent = Intent("KeyboardWillShow")
                intent.putExtra("KeyboardHeight", keyboardHeight)
                broadcastManager.sendBroadcast(intent)
            }
        })
        keyboardListenersAttached = true
    }

    private fun onHideKeyboard() {
        constraintLayout.visible()
    }

    private fun onShowKeyboard(keyboardHeight: Int) {
        if (keyboardHeight > 800) {
            constraintLayout.gone()
        }
        else {
            constraintLayout.visible()
        }
    }
}
