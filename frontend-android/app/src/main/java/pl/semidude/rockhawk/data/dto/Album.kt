package pl.semidude.rockhawk.data.dto

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Album(
    var id: String,
    var name: String,
    var artists: List<Artist>
) : Parcelable