package pl.semidude.rockhawk.presentation.player

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.view.Menu
import android.view.MenuItem
import android.widget.SeekBar
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.PopupMenu
import com.bumptech.glide.Glide
import io.reactivex.android.schedulers.AndroidSchedulers
import io.sellmair.disposer.disposeBy
import io.sellmair.disposer.onDestroy
import kotlinx.android.synthetic.main.activity_player.*
import org.koin.android.viewmodel.ext.android.viewModel
import pl.semidude.rockhawk.R
import pl.semidude.rockhawk.data.dto.Track
import pl.semidude.rockhawk.domain.playback.PlaybackService
import pl.semidude.rockhawk.helpers.gone
import pl.semidude.rockhawk.helpers.invisible
import pl.semidude.rockhawk.helpers.visible


class PlayerActivity : AppCompatActivity() {

    private val viewModel: PlayerViewModel by viewModel()
    private lateinit var playbackService: PlaybackService

    private var currentPlayingTrack: Track? = null
    private lateinit var connection: ServiceConnection

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_player)
        bindPlaybackService()
        listenToClicks()
        listenToMusicSeekBarEvents()
        invalidateOptionsMenu()
    }

    private fun listenToClicks() {
        backButton.setOnClickListener {
            onBackPressed()
        }

        playButton.setOnClickListener {
            playbackService.resumePlayback()
            playButton.invisible()
            pauseButton.visible()
        }

        pauseButton.setOnClickListener {
            playbackService.pausePlayback()
            pauseButton.invisible()
            playButton.visible()
        }

        nextButton.setOnClickListener {
            playbackService.skipNext()
        }

        previousButton.setOnClickListener {
            playbackService.skipPrevious()
        }

        addToLibraryButton.setOnClickListener {
            currentPlayingTrack?.let { track ->

                viewModel.addTrackToLibrary(track)

                addToLibraryButton.invisible()
                removeFromLibraryButton.visible()
                invalidateOptionsMenu()
            }
        }

        removeFromLibraryButton.setOnClickListener {
            currentPlayingTrack?.let { track ->

                viewModel.removeTrackFromLibrary(track)

                removeFromLibraryButton.invisible()
                addToLibraryButton.visible()
                invalidateOptionsMenu()
            }
        }

        moreOptions.setOnClickListener {

            PopupMenu(this, moreOptions).apply {

                setOnMenuItemClickListener { item ->
                    when (item?.itemId) {

                        R.id.addToLibrary -> {

                            currentPlayingTrack?.let { track ->

                                if (track.inLibrary) {
                                    viewModel.removeTrackFromLibrary(track)
                                    removeFromLibraryButton.invisible()
                                    addToLibraryButton.visible()
                                }
                                else {
                                    viewModel.addTrackToLibrary(track)
                                    addToLibraryButton.invisible()
                                    removeFromLibraryButton.visible()
                                }

                                invalidateOptionsMenu()
                            }

                            true
                        }
                        else -> false
                    }
                }
                inflate(R.menu.menu_track)

                this.menu.findItem(R.id.addToLibrary).title =
                    if (currentPlayingTrack?.inLibrary == true) "Remove from library"
                    else "Add to library"

                show()
            }
        }
    }

    private fun listenToMusicSeekBarEvents() {
        musicSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {

            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {

            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {
                playbackService.seekTo(seekBar.progress)
            }
        })
    }

    private fun bindPlaybackService() {
        connection = object : ServiceConnection {

            override fun onServiceConnected(className: ComponentName, service: IBinder) {
                playbackService = (service as PlaybackService.LocalBinder).getService()
                observePlayback()
            }

            override fun onServiceDisconnected(arg0: ComponentName) {

            }
        }

        Intent(this, PlaybackService::class.java).also { intent ->
            bindService(intent, connection, Context.BIND_AUTO_CREATE)
        }
    }

    private fun observePlayback() {
        playbackService.observePlaybackStatus()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { status ->
                when (status!!) {
                    PlaybackService.PlaybackStatus.PLAYING -> {
                        playButton.invisible()
                        pauseButton.visible()
                    }
                    PlaybackService.PlaybackStatus.PAUSED -> {
                        pauseButton.invisible()
                        playButton.visible()
                    }
                }
            }
            .disposeBy(onDestroy)

        playbackService.observePlaybackProgress()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { progress ->
                musicSeekBar.progress = progress
                currentProgressTextView.text = PlaybackService.formatTimeMillis(progress)

                //omg XD
                if (currentProgressTextView.text == maxProgressTextView.text) {
                    pauseButton.invisible()
                    playButton.visible()
                }
            }
            .disposeBy(onDestroy)


        playbackService.observeCurrentPlayingTrack()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { track ->
                currentPlayingTrack = track

                if (currentPlayingTrack!!.inLibrary) {
                    removeFromLibraryButton.visible()
                    addToLibraryButton.invisible()
                } else {
                    addToLibraryButton.visible()
                    removeFromLibraryButton.invisible()
                }

                if (currentPlayingTextView.text != track.title) {
                    musicSeekBar.apply {
                        max = track.durationMillis.toInt()
                        progress = 0
                    }
                }

                currentPlayingTextView.text = track.title

                maxProgressTextView.text = track.durationFormatted

                when (track.type) {
                    "youtube" -> {
                        youtubeIcon.visible()
                        spotifyIcon.invisible()
                        youtubeImageView.visible()
                        spotifyImageView.gone()

                        ownerTextView.text = track.owner
                        viewCountTextView.text = track.viewCount

                        viewCountTextView.visible()
                        circleDivider.visible()

                        Glide.with(this).load(track.imageUrl).centerCrop().into(youtubeImageView)
                    }
                    "spotify" -> {
                        spotifyIcon.visible()
                        youtubeIcon.invisible()
                        spotifyImageView.visible()
                        youtubeImageView.gone()

                        ownerTextView.text = track.artists?.joinToString(", ") { it.name }

                        viewCountTextView.gone()
                        circleDivider.gone()

                        Glide.with(this).load(track.imageUrl).centerCrop().into(spotifyImageView)
                    }
                }

            }
            .disposeBy(onDestroy)
    }

    override fun onStart() {
        super.onStart()
        if (::playbackService.isInitialized) {
            playbackService.connectToSpotify()
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        unbindService(connection)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_track, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        currentPlayingTrack?.let { track ->

            if (track.inLibrary) {
                item.title = "Remove from library"
            }
            else {
                item.title = "Add to library"
            }
        }
        return true
    }
}