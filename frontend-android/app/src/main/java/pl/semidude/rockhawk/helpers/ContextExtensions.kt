package pl.semidude.rockhawk.helpers

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment

fun inflateView(context: Context, resource: Int, parent: ViewGroup, attachToRoot: Boolean = false) =
    LayoutInflater.from(context)
        .inflate(resource, parent, attachToRoot)

val Context.preferences: SharedPreferences
    get() = PreferenceManager.getDefaultSharedPreferences(this)

fun Context.showToast(message: String, length: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, message, length).show()
}

fun Fragment.showToast(message: String, length: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(context, message, length).show()
}

inline fun <reified T> Context.startActivity() {
    startActivity(Intent(this, T::class.java))
}

inline fun <reified T> Context.startActivity(intentCustomizer: Intent.() -> Unit) {
    startActivity(Intent(this, T::class.java).apply(intentCustomizer))
}

inline fun <reified T> Fragment.startActivity() {
    startActivity(Intent(context, T::class.java))
}

inline fun <reified T> Fragment.startActivity(intentCustomizer: Intent.() -> Unit) {
    startActivity(Intent(context, T::class.java).apply(intentCustomizer))
}