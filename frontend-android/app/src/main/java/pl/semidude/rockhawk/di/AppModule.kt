@file:Suppress("RemoveExplicitTypeArguments")

package pl.semidude.rockhawk.di

import android.preference.PreferenceManager
import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import pl.semidude.rockhawk.domain.account.AccountManager
import pl.semidude.rockhawk.domain.playback.PlaybackQueue
import pl.semidude.rockhawk.domain.tracks.LibraryManager
import pl.semidude.rockhawk.domain.tracks.PlaylistManager
import pl.semidude.rockhawk.domain.tracks.SearchManager
import pl.semidude.rockhawk.domain.tracks.TracksMemorizer
import pl.semidude.rockhawk.presentation.login.LoginViewModel
import pl.semidude.rockhawk.presentation.main.MainViewModel
import pl.semidude.rockhawk.presentation.player.PlayerViewModel
import pl.semidude.rockhawk.presentation.register.RegisterViewModel

val appModule = module {

    single { PreferenceManager.getDefaultSharedPreferences(androidContext()) }

    single { AccountManager(get()) }

    single { PlaybackQueue() }

    single { TracksMemorizer() }

    single { SearchManager(get(), get()) }

    single { LibraryManager(get(), get()) }

    single { PlaylistManager(get(), get()) }

    viewModel { LoginViewModel(get(), get()) }

    viewModel { RegisterViewModel(get(), get()) }

    viewModel { MainViewModel(get(), get(), get(), get(), get()) }

    viewModel { PlayerViewModel(get(), get(), get()) }
}