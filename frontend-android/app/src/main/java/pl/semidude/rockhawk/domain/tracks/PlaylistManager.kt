package pl.semidude.rockhawk.domain.tracks

import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import pl.semidude.rockhawk.data.api.RockhawkApi
import pl.semidude.rockhawk.data.dto.Playlist
import pl.semidude.rockhawk.data.dto.Track

class PlaylistManager(
    private val rockhawkApi: RockhawkApi,
    private val tracksMemorizer: TracksMemorizer
) {
    private val playlistsSubject = BehaviorSubject.create<PlaylistsState>()
    private val spotifyPlaylistsSubject = BehaviorSubject.create<PlaylistsState>()
    private val playlists: MutableMap<String, Playlist> = mutableMapOf()

    fun observePlaylists(): Observable<PlaylistsState> = playlistsSubject
    fun observeSpotifyPlaylists(): Observable<PlaylistsState> = spotifyPlaylistsSubject

    fun fetchPlaylists(): Disposable {
        return rockhawkApi.getPlaylists()
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { playlistsSubject.onNext(PlaylistsState.Loading) }
            .subscribeBy(
                onSuccess = { fetchedPlaylists ->
                    if (fetchedPlaylists.any { it.playlistId == null }) {
                        throw IllegalStateException("Fetched playlists should not have null playlistIds")
                    }
                    playlists.putAll(fetchedPlaylists
                        .map { it.withMemorizedTrackInstances(tracksMemorizer) }
                        .associateBy { it.playlistName })

                    playlistsSubject.onNext(PlaylistsState.Result(playlists))
                },
                onError = { throwable -> playlistsSubject.onNext(PlaylistsState.Error(throwable)) }
            )
    }

    fun createPlaylist(playlistName: String): Disposable {
        val playlistToCreate = Playlist(playlistName = playlistName)

        return rockhawkApi.createPlaylist(playlistToCreate)
            .subscribeOn(Schedulers.io())
            .doOnSubscribe {
                playlists[playlistName] = playlistToCreate
                playlistsSubject.onNext(PlaylistsState.Result(playlists))
            }
            .subscribeBy(
                onSuccess = { createdPlaylist ->
                    if (createdPlaylist.playlistId == null) {
                        throw IllegalStateException("Created playlist $createdPlaylist returned with null playlistId")
                    }
                    playlists[createdPlaylist.playlistName] = createdPlaylist
                    println("Playlist ${createdPlaylist.playlistName} created")
                    playlistsSubject.onNext(PlaylistsState.Result(playlists))
                },
                onError = { throwable ->  playlistsSubject.onNext(PlaylistsState.Error(throwable)) }
            )
    }

    fun addTrackToPlaylist(playlist: Playlist, track: Track): Disposable {
        val playlistToUpdate = playlists[playlist.playlistName]
            ?: throw IllegalArgumentException("Playlist $playlist not found in PlaylistManager!")

        if (playlistToUpdate.containsTrack(track)) {
            return dummyDisposable()
        }
        playlistToUpdate.addTrack(tracksMemorizer.getMemorizedTrackInstance(track))
        playlistToUpdate.tracksCount += 1

        if (playlistToUpdate.playlistId == null) {
            throw IllegalStateException("Playlist ${playlist.playlistName} has not been created on backend!")
        }

        return rockhawkApi.addTrackToPlaylist(playlistToUpdate.playlistId!!, track)
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { playlistsSubject.onNext(PlaylistsState.Result(playlists)) }
            .subscribeBy(
                onSuccess = { /* do nothing, playlist was already updated */},
                onError = { throwable -> playlistsSubject.onNext(PlaylistsState.Error(throwable)) }
            )
    }

    fun removeTrackFromPlaylist(playlist: Playlist, track: Track): Disposable {
        val playlistToUpdate = playlists[playlist.playlistName]
            ?: throw IllegalArgumentException("Playlist $playlist does not exist!")

        playlistToUpdate.removeTrack(tracksMemorizer.getMemorizedTrackInstance(track))
        playlistToUpdate.tracksCount -= 1

        if (playlistToUpdate.playlistId == null) {
            throw IllegalStateException("Playlist ${playlist.playlistName} has not been created on backend!")
        }

        return rockhawkApi.removeTrackFromPlaylist(playlistToUpdate.playlistId!!, track.id)
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { playlistsSubject.onNext(PlaylistsState.Result(playlists)) }
            .subscribeBy(
                onComplete = { /* do nothing, playlist was already updated */},
                onError = { throwable -> playlistsSubject.onNext(PlaylistsState.Error(throwable)) }
            )
    }

    fun deletePlaylist(playlist: Playlist): Disposable {
        val playlistToDelete = playlists[playlist.playlistName]
            ?: throw IllegalArgumentException("Playlist $playlist does not exist!")

        playlists.remove(playlistToDelete.playlistName)

        if (playlistToDelete.playlistId == null) {
            throw IllegalStateException("Playlist ${playlist.playlistName} has not been created on backend!")
        }

        return rockhawkApi.deletePlaylist(playlistToDelete.playlistId!!)
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { playlistsSubject.onNext(PlaylistsState.Result(playlists)) }
            .subscribeBy(
                onComplete = { /* do nothing, playlist was already deleted */ },
                onError = { throwable -> playlistsSubject.onNext(PlaylistsState.Error(throwable)) }
            )
    }

    fun fetchSpotifyPlaylists(): Disposable {
        return rockhawkApi.getSpotifyPlaylists()
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { spotifyPlaylistsSubject.onNext(PlaylistsState.Loading) }
            .subscribeBy(
                onSuccess = { fetchedSpotifyPlaylists ->
                    spotifyPlaylistsSubject.onNext(PlaylistsState.Result(fetchedSpotifyPlaylists.associateBy { it.playlistName }))
                },
                onError = { throwable -> spotifyPlaylistsSubject.onNext(PlaylistsState.Error(throwable)) }
            )
    }

    fun importSpotifyPlaylist(playlistId: String): Disposable {
        return rockhawkApi.importSpotifyPlaylist(playlistId)
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { playlistsSubject.onNext(PlaylistsState.Loading) }
            .subscribeBy(
                onSuccess = { importedPlaylist ->
                    if (importedPlaylist.playlistId == null) {
                        throw IllegalStateException("Created playlist $importedPlaylist returned with null playlistId")
                    }
                    playlists[importedPlaylist.playlistName] = importedPlaylist
                    println("Playlist ${importedPlaylist.playlistName} imported")
                    playlistsSubject.onNext(PlaylistsState.Result(playlists))
                },
                onError = { throwable -> playlistsSubject.onNext(PlaylistsState.Error(throwable)) }
            )
    }

    private fun dummyDisposable() = Observable.empty<Unit>().subscribe()

    sealed class PlaylistsState {
        object Loading : PlaylistsState()
        data class Result(val playlists: Map<String, Playlist>) : PlaylistsState()
        data class Error(val throwable: Throwable) : PlaylistsState()
    }
}