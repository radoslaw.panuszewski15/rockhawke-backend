package pl.semidude.rockhawk.presentation.main.playlist

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import androidx.fragment.app.DialogFragment
import kotlinx.android.synthetic.main.dialog_create_playlist.*
import org.koin.android.viewmodel.ext.android.sharedViewModel
import pl.semidude.rockhawk.R
import pl.semidude.rockhawk.presentation.main.MainViewModel
import pl.semidude.rockhawk.presentation.widget.IconButton

class CreatePlaylistDialogFragment : DialogFragment() {

    private val viewModel: MainViewModel by sharedViewModel()

    private val closeButton: ImageView get() = dialog!!.findViewById(R.id.closeButton)
    private val createPlaylistButton: IconButton get() = dialog!!.findViewById(R.id.createPlaylistButton)
    private val playlistNameEditText: EditText get() = dialog!!.findViewById(R.id.playlistNameEditText)

    companion object {
        fun newInstance() = CreatePlaylistDialogFragment()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return AlertDialog.Builder(context!!, R.style.DialogTheme)
            .setView(R.layout.dialog_create_playlist)
            .create()
    }

    override fun onResume() {
        super.onResume()

        closeButton.setOnClickListener { dismiss() }

        createPlaylistButton.setOnClickListener {
            viewModel.createPlaylist(playlistNameEditText.text.toString())
            dismiss()
        }
    }
}