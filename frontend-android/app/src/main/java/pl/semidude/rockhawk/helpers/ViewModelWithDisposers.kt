package pl.semidude.rockhawk.helpers

import androidx.lifecycle.ViewModel
import io.sellmair.disposer.Disposer

open class ViewModelWithDisposers: ViewModel() {

    protected val onCleared = Disposer.create()

    override fun onCleared() {
        super.onCleared()
        onCleared.dispose()
    }
}