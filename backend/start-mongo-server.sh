#!/bin/bash
docker.exe run -d --restart=always --name mongo-server -v ~/docker/volumes/mongo:/data/db -p 27017:27017 --network mongo-net mongo