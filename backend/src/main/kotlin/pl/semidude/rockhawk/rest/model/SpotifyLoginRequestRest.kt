package pl.semidude.rockhawk.rest.model

import com.fasterxml.jackson.annotation.JsonProperty

data class SpotifyLoginRequestRest(
    @JsonProperty("code") val code: String
)