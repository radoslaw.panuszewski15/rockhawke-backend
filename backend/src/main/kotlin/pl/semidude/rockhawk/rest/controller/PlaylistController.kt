package pl.semidude.rockhawk.rest.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException
import pl.semidude.rockhawk.domain.Playlist
import pl.semidude.rockhawk.domain.PlaylistRepository
import pl.semidude.rockhawk.domain.User
import pl.semidude.rockhawk.domain.UserRepository
import pl.semidude.rockhawk.helpers.getAuthenticatedUserId
import pl.semidude.rockhawk.helpers.getById
import pl.semidude.rockhawk.helpers.makeList
import pl.semidude.rockhawk.rest.model.PlaylistRest
import pl.semidude.rockhawk.rest.model.TrackRest
import pl.semidude.rockhawk.service.spotify.api.SpotifyPlaylistsApi
import pl.semidude.rockhawk.service.spotify.api.SpotifyTracksApi

@RestController
@Transactional
open class PlaylistController(
    @Autowired private val userRepository: UserRepository,
    @Autowired private val playlistRepository: PlaylistRepository,
    @Autowired private val spotifyTracksApi: SpotifyTracksApi,
    @Autowired private val spotifyPlaylistsApi: SpotifyPlaylistsApi
) {
    @GetMapping("/users/{userId}/playlists")
    @ResponseStatus(HttpStatus.OK)
    open fun getPlaylists(@PathVariable userId: String): List<PlaylistRest> {
        return doGetPlaylists(userId)
    }

    @GetMapping("/me/playlists")
    @ResponseStatus(HttpStatus.OK)
    open fun getPlaylists(): List<PlaylistRest> {
        return doGetPlaylists(getAuthenticatedUserId())
    }

    private fun doGetPlaylists(userId: String): List<PlaylistRest> {
        val user = userRepository.getById(userId)
            ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "User with id $userId not found")

        return user.playlists.map { it.mapToRest() }
    }

    @PostMapping("/users/{userId}/playlists")
    @ResponseStatus(HttpStatus.CREATED)
    open fun createPlaylist(@PathVariable userId: String, @RequestBody playlistRest: PlaylistRest): PlaylistRest {
        return doCreatePlaylist(userId, playlistRest)
    }

    @PostMapping("/me/playlists")
    @ResponseStatus(HttpStatus.CREATED)
    open fun createPlaylist(@RequestBody playlistRest: PlaylistRest): PlaylistRest {
        return doCreatePlaylist(getAuthenticatedUserId(), playlistRest)
    }

    private fun doCreatePlaylist(userId: String, playlistRest: PlaylistRest): PlaylistRest {
        val user = userRepository.getById(userId)
            ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "User with id $userId not found")

        val playlist = playlistRepository.save(Playlist(
            playlistName = playlistRest.playlistName,
            ownerId = getAuthenticatedUserId(),
            tracks = playlistRest.tracks?.map { it.mapToDomain() }?.toMutableList() ?: mutableListOf()
        ))

        user.playlists.add(playlist)
        userRepository.save(user)

        return playlist.mapToRest()
    }

    @DeleteMapping("/playlists/{playlistId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    open fun deletePlaylist(@PathVariable playlistId: String) {
        doDeletePlaylist(playlistId)
    }

    private fun doDeletePlaylist(playlistId: String) {
        val playlist = playlistRepository.getById(playlistId)
            ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Playlist with id $playlistId not found")

        val user = userRepository.getById(playlist.ownerId)
            ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid owner ${playlist.ownerId} of playlist $playlistId")

        user.playlists.remove(playlist)
        userRepository.save(user)

        playlistRepository.delete(playlist)
    }

    @PatchMapping("/playlists/{playlistId}")
    @ResponseStatus(HttpStatus.OK)
    open fun addTrackToPlaylist(
        @PathVariable playlistId: String,
        @RequestBody trackRest: TrackRest
    ): PlaylistRest {
        return doAddTrackToPlaylist(playlistId, trackRest)
    }

    private fun doAddTrackToPlaylist(playlistId: String, trackRest: TrackRest): PlaylistRest {
        val playlist = (playlistRepository.getById(playlistId)
            ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Playlist with id $playlistId not found"))
        val track = trackRest.mapToDomain()

        if (!playlist.tracks.contains(track)) {
            playlist.tracks.add(track)
        }
        playlistRepository.save(playlist)

        return playlist.mapToRest()
    }

    @DeleteMapping("/playlists/{playlistId}/{trackId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    open fun removeTrackFromPlaylist(
        @PathVariable playlistId: String,
        @PathVariable trackId: String
    ) {
        doRemoveTrackFromPlaylist(playlistId, trackId)
    }

    private fun doRemoveTrackFromPlaylist(playlistId: String, trackId: String) {
        val playlist = (playlistRepository.getById(playlistId)
            ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Playlist with id $playlistId not found"))

        playlist.tracks.removeIf { track -> track.platformId == trackId }
        playlistRepository.save(playlist)
    }

    @GetMapping("/me/playlists/spotify")
    @ResponseStatus(HttpStatus.OK)
    fun getUserSpotifyPlaylists(): List<PlaylistRest> {
        return doGerUserSpotifyPlaylists(getAuthenticatedUserId())
    }

    private fun doGerUserSpotifyPlaylists(userId: String): List<PlaylistRest> {
        val user = userRepository.getById(userId)
            ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "User with id $userId not found")

        if (user.lastSpotifyAccessToken == null || user.spotifyRefreshToken == null) {
            return emptyList()
        }
        return makeList { playlists ->
            playlists += getLikedSongsPlaylist(user)
            playlists += getCreatedPlaylists(user)
        }
    }

    private fun getLikedSongsPlaylist(user: User): PlaylistRest {
        val tracksCount = spotifyTracksApi.getUserTracksCount(user.lastSpotifyAccessToken!!, user.spotifyRefreshToken!!)
        return PlaylistRest(playlistId = "${user.id}:liked_songs", playlistName = "Spotify liked songs", tracksCount = tracksCount)
    }

    private fun getCreatedPlaylists(user: User): List<PlaylistRest> {
        return spotifyPlaylistsApi.getUserPlaylists(user.lastSpotifyAccessToken!!, user.spotifyRefreshToken!!)
            .map {
                PlaylistRest(playlistId = it.id, playlistName = it.name ?: "Unknown", tracksCount = it.tracks?.total ?: 0)
            }
    }

    @PostMapping("/me/playlists/spotify/{playlistId}")
    @ResponseStatus(HttpStatus.OK)
    fun importSpotifyPlaylist(@PathVariable playlistId: String): PlaylistRest {
        return doImportSpotifyPlaylist(getAuthenticatedUserId(), playlistId)
    }

    private fun doImportSpotifyPlaylist(userId: String, playlistId: String): PlaylistRest {
        val user = userRepository.getById(userId)
            ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "User with id $userId not found")

        if (user.lastSpotifyAccessToken == null || user.spotifyRefreshToken == null) {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "User $userId does not have Spotify tokens available")
        }

        var playlist = if (playlistId.contains("liked_songs")) {
            val userTracks = spotifyTracksApi.getUserTracks(user.lastSpotifyAccessToken!!, user.spotifyRefreshToken!!)
            Playlist(playlistName = "Spotify liked songs", tracks = userTracks.toMutableList(), ownerId = userId)
        }
        else {
            val playlistTracks = spotifyTracksApi.getPlaylistTracks(playlistId, user.lastSpotifyAccessToken!!, user.spotifyRefreshToken!!)
            val playlistName = spotifyPlaylistsApi.getPlaylistName(playlistId, user.lastSpotifyAccessToken!!, user.spotifyRefreshToken!!)
            Playlist(playlistName = playlistName, tracks = playlistTracks.toMutableList(), ownerId = userId)
        }

        playlist = playlistRepository.save(playlist)
        user.playlists.add(playlist)
        userRepository.save(user)

        return playlist.mapToRest()
    }
}