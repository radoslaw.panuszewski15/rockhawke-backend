package pl.semidude.rockhawk.rest.model

data class PlaylistRest(
    var playlistId: String? = null,
    var playlistName: String,
    var tracksCount: Int,
    var tracks: List<TrackRest>? = null,
    var coverImage: String? = null
)