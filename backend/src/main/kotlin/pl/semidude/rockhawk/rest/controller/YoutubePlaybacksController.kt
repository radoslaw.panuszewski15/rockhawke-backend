package pl.semidude.rockhawk.rest.controller

import org.apache.catalina.connector.ClientAbortException
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.util.MimeType
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody
import pl.semidude.rockhawk.domain.Playback
import pl.semidude.rockhawk.domain.Range
import pl.semidude.rockhawk.domain.YoutubeTrackRepository
import pl.semidude.rockhawk.helpers.Controller
import pl.semidude.rockhawk.helpers.readBytesBuffered
import pl.semidude.rockhawk.rest.model.PlaybackRest
import pl.semidude.rockhawk.rest.model.TrackRest
import pl.semidude.rockhawk.service.youtube.audio.AudioProviderNotAvailableException
import pl.semidude.rockhawk.service.youtube.audio.MalformedUrlException
import pl.semidude.rockhawk.service.youtube.audio.RangeException
import pl.semidude.rockhawk.service.youtube.audio.YoutubeAudioProvider
import pl.semidude.rockhawk.service.youtube.audio.YoutubeAudioProvider.Companion.END_OF_TRACK
import pl.semidude.rockhawk.service.youtube.playback.PlaybackManager
import java.io.BufferedInputStream
import java.io.InputStream
import java.io.OutputStream
import java.net.URI
import kotlin.concurrent.thread

@RestController
@RequestMapping("/youtubePlaybacks")
class YoutubePlaybacksController(
    @Autowired private val youtubeAudioProvider: YoutubeAudioProvider,
    @Autowired private val playbackManager: PlaybackManager,
    @Autowired private val youtubeTrackRepo: YoutubeTrackRepository
) : Controller {

    private val logger = LogManager.getLogger(javaClass)

    private val playbacks: MutableMap<Long, Playback> = mutableMapOf()
    private val playbacksRequestedToStop: MutableSet<Long> = mutableSetOf()

    /**
     * Endpoint fo preparing playbacks to be played by some front-end audio player.
     * What it does:
     * 1) extracts the YouTube's raw audio URL for further use (it could not be simply returned to the player due to geological restrictions)
     * 2) generates ID for the playback
     * 3) checks if the track is already on s3
     * 4) playbackId and (optionally) s3Url are returned in response
     * @param youtubeTrackId id of the track
     * @return JSON containing generated playbackId as well as the s3Url
     */
    @PostMapping
    fun preparePlayback(
        @RequestBody youtubeTrack: TrackRest
    ): PlaybackRest {

        val playback = playbackManager.createPlayback(excludedIds = playbacksRequestedToStop)
        playback.appendTrack(youtubeTrack.id)
        playbacks[playback.playbackId] = playback

        logger.info("Prepared playback for ${youtubeTrack.id} with id ${playback.playbackId}")

        val s3Url = youtubeTrack.s3Url ?: youtubeTrackRepo.findByPlatformId(youtubeTrack.id)?.s3Url

        return createAppropriateResponse(s3Url, youtubeTrack, playback)
    }

    private fun createAppropriateResponse(s3Url: String?, youtubeTrack: TrackRest, playback: Playback): PlaybackRest {

        return if (s3Url != null) {
            logger.info("RETURN S3 URL FOR ${youtubeTrack.id}")
            PlaybackRest(playback.playbackId, youtubeTrack, s3Url)
        }
        else {
            logger.info("INITIALIZING CACHE FOR ${youtubeTrack.id}")
            youtubeAudioProvider.initTrackCache(youtubeTrack.id)
            thread { youtubeAudioProvider.initRemainingCaches() }

            logger.info("RETURN STREAM FOR ${youtubeTrack.id}")
            PlaybackRest(playback.playbackId, youtubeTrack)
        }
    }

    /**
     * Endpoint for stopping playback which is currently being played.
     * When the playback gets stopped, following happens:
     * - streaming for this playback stops (if any active)
     * - background caching process pauses (if not completed yet). It will be resumed as soon as the track gets played again
     * @param playbackId id of the playback to be stopped
     */
    @DeleteMapping("/{playbackId}")
    fun stopPlayback(@PathVariable playbackId: Long) {
        logger.info("STOP PLAYBACK $playbackId")

        playbacksRequestedToStop.add(playbackId)

        playbacks[playbackId]?.getTrackIds()?.forEach { trackToStop ->
            youtubeAudioProvider.pauseCaching(trackToStop)
        }
    }

    /**
     * Endpoint for seeking to specified part of the track.
     * If the track is not fully cached yet, then the request gets redirected to [startTrackOnPlayback]
     * Otherwise, appropriate byte range is returned
     * @param playbackId id of the playback which has been prepared by [preparePlayback]
     * @param trackPosition track position on the playlist
     * @param range the Range HTTP header, indicating place in the track user want to seek to
     */
    @GetMapping("/{playbackId}/{trackPosition}", headers = ["Range"])
    fun seekTrackOnPlayback(
        @PathVariable playbackId: Long,
        @PathVariable trackPosition: Int,
        @RequestHeader("Range") range: String
    ): ResponseEntity<ByteArray> {

        val (playback, youtubeTrackId) = getPlaybackAndTrack(playbackId, trackPosition)

        logger.info("SEEK PLAYBACK $playbackId WITH TRACK $youtubeTrackId ON RANGE: $range")

//        playbackManager.tryScheduleNextTrackFor(playback, trackPosition)

        val (rangeFrom, rangeTo) = extractRangeValues(range)

        return createAppropriateResponse(youtubeTrackId, playbackId, trackPosition, rangeFrom, rangeTo)
    }

    private fun getPlaybackAndTrack(playbackId: Long, trackPosition: Int): Pair<Playback, String> {

        val playback = playbacks[playbackId] ?: notFound("playback with id $playbackId not found")

        checkTrackPosition(trackPosition, playback, playbackId)
        val youtubeTrackId = playback.getTrackOnPosition(trackPosition)

        return Pair(playback, youtubeTrackId)
    }


    private fun checkTrackPosition(trackPosition: Int, playback: Playback, playbackId: Long) {
        if (trackPosition < 0 || trackPosition > playback.getLastPosition())
            badRequest("trackPosition for playback $playbackId should be within range [0-${playback.getLastPosition()}]")
    }


    private fun extractRangeValues(range: String): MatchResult.Destructured =
        rangeRegex.find(range)?.destructured ?: badRequest("wrong syntax in Range header")


    private fun createAppropriateResponse(youtubeTrackId: String, playbackId: Long, trackPosition: Int, rangeFrom: String, rangeTo: String): ResponseEntity<ByteArray> {

        return if (!youtubeAudioProvider.isTrackFullyCached(youtubeTrackId)) {
            logger.info("REDIRECTING...")
            createRedirectToStreamResponse(playbackId, trackPosition, rangeFrom.toInt())
        }
        else {
            val bytes = getByteRange(youtubeTrackId, rangeFrom, rangeTo)
            createPartialContentResponse(bytes)
        }
    }

    private fun getByteRange(youtubeTrackId: String, rangeFrom: String, rangeTo: String): ByteArray =
        try {
            youtubeAudioProvider.getByteRange(
                youtubeTrackId,
                Range(
                    begin = rangeFrom.toInt(),
                    end = if (rangeTo != "") rangeTo.toInt() + 1 //adding +1 since rangeTo is inclusive, and we want to make it exclusive
                    else END_OF_TRACK
                )
            )
        }
        catch (e: RangeException) {
            rangeNotSatisfiable(e.message)
        }

    private fun createRedirectToStreamResponse(playbackId: Long, trackPosition: Int, startPos: Int): ResponseEntity<ByteArray> =
        ResponseEntity
            .status(HttpStatus.MOVED_PERMANENTLY)
            .location(URI("/youtubePlaybacks/redirected/$playbackId/$trackPosition?startPos=$startPos"))
            .body(ByteArray(0))


    private fun createPartialContentResponse(byteRange: ByteArray): ResponseEntity<ByteArray> =
        ResponseEntity
            .status(HttpStatus.PARTIAL_CONTENT)
            .contentType(MediaType.asMediaType(MimeType.valueOf("audio/webm")))
            .body(byteRange)


    /**
     * This endpoint only gets called from [seekTrackOnPlayback] when the redirection is needed.
     * It must be a separate endpoint, since the only difference between [seekTrackOnPlayback] and [startTrackOnPlayback]
     * is that the former accepts Range header and the latter does not. Due to that header being
     * passed through redirection, it leads to infinite-loop by [seekTrackOnPlayback] calling itself.
     * And that's why we need different endpoint path for redirection.
     */
    @GetMapping("/redirected/{playbackId}/{trackPosition}")
    fun startTrackOnPlaybackFromRedirect(
        @PathVariable playbackId: Long,
        @PathVariable trackPosition: Int,
        @RequestParam startPos: Int?
    ): ResponseEntity<StreamingResponseBody> = startTrackOnPlayback(playbackId, trackPosition, startPos)

    /**
     * Basic entry point for the front-end audio player.
     * If the track has been fully cached already, then it gets returned as it is
     * Otherwise, an asynchronous HTTP stream is established
     * @param playbackId id of the playback which has been prepared by [preparePlayback]
     * @param trackPosition track position on the playlist
     * @param startPos the requested initial position in the stream, zero by default
     */
    @GetMapping("/{playbackId}/{trackPosition}")
    fun startTrackOnPlayback(
        @PathVariable playbackId: Long,
        @PathVariable trackPosition: Int,
        @RequestParam startPos: Int?
    ): ResponseEntity<StreamingResponseBody> {

        val (playback, youtubeTrackId) = getPlaybackAndTrack(playbackId, trackPosition)
        logger.info("START TRACK $youtubeTrackId ON PLAYBACK $playbackId WITH START POS = $startPos")
//        playbackManager.tryScheduleNextTrackFor(playback, trackPosition)

        val streamingResponseBody = createStreamingBody(youtubeTrackId, playbackId, startPos)
        return createAppropriateResponse(startPos, streamingResponseBody)
    }

    private fun createStreamingBody(youtubeTrackId: String, playbackId: Long, startPos: Int?): StreamingResponseBody {

        val inputStream = tryOpenAudioStream(youtubeTrackId, startPos)

        return StreamingResponseBody { outputStream ->
            try {
                if (youtubeAudioProvider.isTrackFullyCached(youtubeTrackId)) {
                    outputStream.writeFullTrack(youtubeTrackId)
                }
                else {
                    outputStream.streamTrack(inputStream, youtubeTrackId, playbackId)
                }
            }
            catch (e: ClientAbortException) {
                /*just end streaming*/
            }
            finally {
                outputStream.close() //flushing commits the response
            }
        }
    }

    private fun tryOpenAudioStream(youtubeTrackId: String, startPos: Int?): BufferedInputStream {
        return try {
            youtubeAudioProvider.openAudioStream(youtubeTrackId, startPos ?: 0)
        }
        catch (e: AudioProviderNotAvailableException) {
            badRequest("track $youtubeTrackId has not been prepared before playing")
        }
        catch (e: MalformedUrlException) {
            internalServerError("malformed url: '${e.url}`")
        }
    }

    private fun createAppropriateResponse(startPos: Int?, streamingResponseBody: StreamingResponseBody): ResponseEntity<StreamingResponseBody> {
        return if (startPos == null)
            createOkStreamResponse(streamingResponseBody)
        else
            createPartialContentStreamResponse(streamingResponseBody)
    }

    private fun createOkStreamResponse(streamingResponseBody: StreamingResponseBody): ResponseEntity<StreamingResponseBody> =
        ResponseEntity
            .ok()
            .contentType(MediaType.asMediaType(MimeType.valueOf("audio/webm")))
            .body(streamingResponseBody)


    private fun createPartialContentStreamResponse(streamingResponseBody: StreamingResponseBody): ResponseEntity<StreamingResponseBody> =
        ResponseEntity
            .status(HttpStatus.PARTIAL_CONTENT)
            .contentType(MediaType.asMediaType(MimeType.valueOf("audio/webm")))
            .body(streamingResponseBody)


    /**
     * Synchronously writes full track at once to the receiver [OutputStream]
     * @param youtubeTrackId id of the track
     */
    private fun OutputStream.writeFullTrack(youtubeTrackId: String) {
        val outputStream = this

        logger.info("WRITE FULL TRACK $youtubeTrackId")

        val trackFullSize = youtubeAudioProvider.getFullTrackSize(youtubeTrackId)
        val byteRange = youtubeAudioProvider.getByteRange(youtubeTrackId, Range.FULL)

        outputStream.write(byteRange, 0, trackFullSize)
    }

    /**
     * Asynchronously streams track to the receiver [OutputStream] from provided [InputStream]
     * @param inputStream the [InputStream] which the audio bytes are read from
     * @param youtubeTrackId id of the track being streamed
     * @param playbackId id of the playback which the streaming is being performed on
     */
    private fun OutputStream.streamTrack(inputStream: BufferedInputStream, youtubeTrackId: String, playbackId: Long) {
        val outputStream = this
        var i = 0

        inputStream.readBytesBuffered(bufferLength = 16 * 1024) { buffer, bytesRead ->

            if (playbacksRequestedToStop.contains(playbackId)) return

            outputStream.write(buffer, 0, bytesRead)

            logger.info("SENT ${buffer.size} BYTES NO ${++i} FOR $youtubeTrackId")
        }

        logger.info("COMPLETED SENDING BYTES FOR $youtubeTrackId")
    }

    companion object {
        private val rangeRegex = """^bytes=(\d+)-(\d*)""".toRegex()

        private val herokuBaseUrl = "https://rockhawke.herokuapp.com"
        private val homeBaseUrl = "http://192.168.1.109:8080"
        private val localhostBaseUrl = "http://localhost:8080"
        private val mobileBaseUrl = "http://192.168.43.85:8080"
        private val pcBaseUrl = "http://192.168.0.108:8080"
        private val laptopBaseUrl = "http://192.168.0.207:8080"
    }
}