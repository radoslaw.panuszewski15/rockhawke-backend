package pl.semidude.rockhawk.rest.controller

import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException
import pl.semidude.rockhawk.domain.TrackRepository
import pl.semidude.rockhawk.domain.User
import pl.semidude.rockhawk.domain.UserRepository
import pl.semidude.rockhawk.helpers.getAuthenticatedUserId
import pl.semidude.rockhawk.helpers.getById
import pl.semidude.rockhawk.rest.model.TrackRest
import pl.semidude.rockhawk.service.common.AsynchronousTrackSearcher
import pl.semidude.rockhawk.service.rest.ResponseEnricher
import pl.semidude.rockhawk.service.youtube.audio.YoutubeAudioProvider
import kotlin.concurrent.thread

@RestController
@RequestMapping("/search")
class SearchController(
    @Autowired private val asynchronousTrackSearcher: AsynchronousTrackSearcher,
    @Autowired private val userRepo: UserRepository,
    @Autowired private val trackRepo: TrackRepository,
    @Autowired private val youtubeAudioProvider: YoutubeAudioProvider,
    @Autowired private val responseEnricher: ResponseEnricher
) {
    private val logger = LogManager.getLogger(javaClass)
    private val userId: String get() = getAuthenticatedUserId()

    @GetMapping
    fun searchTracks(
        @RequestParam query: String,
        @RequestParam youtube: Boolean = false,
        @RequestParam spotify: Boolean = false
    ): List<TrackRest> {

        logger.info("SEARCH TRACKS FOR QUERY '$query'")

        if (!youtube && !spotify)
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "at least one parameter of [youtube, spotify] should be passed")

        val user: User? = userRepo.getById(userId)

        val searchResults: List<TrackRest> = asynchronousTrackSearcher.searchTracks(youtube, query, spotify, user)

        persistNewTracksAsync(searchResults)

        registerTracksForCacheAsync(searchResults)

        return responseEnricher.enrichResponseForSearchResults(searchResults, user)
    }

    private fun persistNewTracksAsync(searchResults: List<TrackRest>) {
        thread {
            trackRepo.saveAll(
                searchResults
                    .filter { !trackRepo.existsByPlatformId(it.id) }
                    .map { it.mapToDomain() }
            )
        }
    }

    private fun registerTracksForCacheAsync(searchResults: List<TrackRest>) {
        searchResults
            .filter { it.type == "youtube" && it.s3Url == null && !youtubeAudioProvider.isTrackInitialized(it.id) }
            .forEach { trackDto ->
                thread { youtubeAudioProvider.registerTrackForCache(trackDto.id) }
            }
    }
}