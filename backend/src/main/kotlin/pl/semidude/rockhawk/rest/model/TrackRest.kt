package pl.semidude.rockhawk.rest.model

import pl.semidude.rockhawk.domain.*
import java.lang.IllegalArgumentException
import java.lang.IllegalStateException

data class TrackRest(
    var id: String,
    var title: String,
    var durationMillis: Long,
    var durationFormatted: String,
    var type: String,
    var thumbnailUrl: String,
    var imageUrl: String,
    var initialized: Boolean = false,
    var inLibrary: Boolean = false,
    var description: String? = null,
    var owner: String? = null,
    var viewCount: String? = null,
    var artists: List<ArtistRest>? = null,
    var album: AlbumRest? = null,
    var s3Url: String? = null,
    var alternativeTitle: String? = null
) {
    companion object {
        const val YOUTUBE = "youtube"
        const val SPOTIFY = "spotify"
    }

    fun mapToDomain(): Track =
        when (type) {
            "spotify" -> SpotifyTrack(
                platformId = id,
                title = title,
                thumbnailUrl = thumbnailUrl,
                durationMillis = durationMillis,
                durationFormatted = durationFormatted,
                artists = artists?.map { it.mapToDomain() }
                    ?: throw IllegalStateException("Spotify TrackDTO should have 'artists' field not null"),
                album = album?.mapToDomain()
                    ?: throw IllegalStateException("Spotify TrackDTO should have 'album' field not null")
            )
            "youtube" -> YoutubeTrack(
                platformId = id,
                title = title,
                thumbnailUrl = thumbnailUrl,
                imageUrl = imageUrl,
                durationMillis = durationMillis,
                durationFormatted = durationFormatted,
                owner = owner ?: throw IllegalStateException("YouTube TrackDTO should have 'owner' field not null"),
                viewCount = viewCount!!,
                s3Url = s3Url,
                alternativeTitle = alternativeTitle
            )
            else -> throw IllegalArgumentException("not supported track type: $type")
        }
}