package pl.semidude.rockhawk.rest.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import pl.semidude.rockhawk.service.accounts.UserService
import pl.semidude.rockhawk.service.authentication.ThirdPartyUserConnector

@RestController
@RequestMapping("/users")
class UserController(
    @Autowired private val userService: UserService,
    @Autowired @Qualifier("spotify") private val spotifyUserConnector: ThirdPartyUserConnector,
    @Autowired @Qualifier("google") private val googleUserConnector: ThirdPartyUserConnector
) {
    @PostMapping
    fun register(
        @RequestParam email: String,
        @RequestParam password: String,
        @RequestParam nickname: String? = null
    ): String {
        return userService.registerUser(
            email = email,
            password = password,
            nickname = nickname
        )
    }

    @PatchMapping("/{userId}")
    fun connectThirdParty(
        @PathVariable userId: String,
        @RequestParam("spotify_code") spotifyCode: String?,
        @RequestParam("google_code") googleCode: String?
    ): ResponseEntity<String> {

        if (spotifyCode != null && googleCode != null)
            return ResponseEntity
                .badRequest()
                .body("only one of 'spotifyCode' and 'googleCode' should be provided")

        val successful = when {
            spotifyCode != null -> spotifyUserConnector.connectUserUsingCode(userId, spotifyCode)
            googleCode != null -> googleUserConnector.connectUserUsingCode(userId, googleCode)
            else -> false
        }

        return if (successful)
            ResponseEntity
                .noContent()
                .build()
        else
            ResponseEntity
                .status(HttpStatus.FORBIDDEN)
                .body("cannot authenticate user with spotifyCode=$spotifyCode and googleCode=$googleCode")
    }
}