package pl.semidude.rockhawk.rest.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException
import pl.semidude.rockhawk.domain.TrackRepository
import pl.semidude.rockhawk.domain.UserRepository
import pl.semidude.rockhawk.helpers.getAuthenticatedUserId
import pl.semidude.rockhawk.helpers.getById
import pl.semidude.rockhawk.rest.model.TrackRest
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock

@RestController
@Transactional
open class LibraryController(
    @Autowired private val userRepository: UserRepository,
    @Autowired private val trackRepository: TrackRepository
) {
    private val libraryLock = ReentrantLock()

    @GetMapping("/users/{userId}/library")
    @ResponseStatus(HttpStatus.OK)
    open fun getLibrary(
        @PathVariable userId: String
    ): List<TrackRest> {
        return doGetLibrary(userId)
    }

    @GetMapping("/me/library")
    @ResponseStatus(HttpStatus.OK)
    open fun getLibrary(): List<TrackRest> {
        return doGetLibrary(getAuthenticatedUserId())
    }

    private fun doGetLibrary(userId: String): List<TrackRest> {
        val user = userRepository.getById(userId)
            ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "User with id $userId not found")

        return user.library
            .map { it.mapToRest() }
            .onEach { it.inLibrary = true }
    }

    @PutMapping("/users/{userId}/library/{trackId}")
    @ResponseStatus(HttpStatus.OK)
    open fun addTrackToLibrary(
        @PathVariable userId: String,
        @PathVariable trackId: String,
        @RequestBody trackRest: TrackRest
    ): List<TrackRest> {
        return doAddTrackToLibrary(userId, trackId, trackRest)
    }

    @PutMapping("/me/library/{trackId}")
    @ResponseStatus(HttpStatus.OK)
    open fun addTrackToLibrary(
        @PathVariable trackId: String,
        @RequestBody trackRest: TrackRest
    ): List<TrackRest> {
        return doAddTrackToLibrary(getAuthenticatedUserId(), trackId, trackRest)
    }

    private fun doAddTrackToLibrary(userId: String, trackId: String, trackRest: TrackRest): List<TrackRest> {
        libraryLock.withLock {
            val user = userRepository.getById(userId)
                ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "User with id $userId not found")

            if (!trackRepository.existsByPlatformId(trackId)) {
                trackRepository.save(trackRest.mapToDomain())
            }
            if (trackRest.id !in user.library.map { it.platformId }) {
                user.library += trackRest.mapToDomain()
                userRepository.save(user)
            }
            return user.library.map { it.mapToRest() }
        }
    }

    @DeleteMapping("/users/{userId}/library/{trackId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    open fun removeTrackFromLibrary(
        @PathVariable userId: String,
        @PathVariable trackId: String
    ) {
        doRemoveTrackFromLibrary(userId, trackId)
    }

    @DeleteMapping("/me/library/{trackId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    open fun removeTrackFromLibrary(
        @PathVariable trackId: String
    ) {
        doRemoveTrackFromLibrary(getAuthenticatedUserId(), trackId)
    }

    private fun doRemoveTrackFromLibrary(userId: String, trackId: String) {
        libraryLock.withLock {
            val user = userRepository.getById(userId)
                ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "User with id $userId not found")

            user.library.removeIf { it.platformId == trackId }
            userRepository.save(user)
        }
    }
}