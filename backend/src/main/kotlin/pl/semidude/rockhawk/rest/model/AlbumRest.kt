package pl.semidude.rockhawk.rest.model

import pl.semidude.rockhawk.domain.Album

data class AlbumRest(
    var id: String,
    var name: String,
    var artists: List<ArtistRest>
) {
    fun mapToDomain(): Album =
        Album(
            id = id,
            name = name,
            artists = artists.map { it.mapToDomain() }
        )
}

