package pl.semidude.rockhawk.rest.model

data class PlaybackRest(
    var playbackId: Long,
    val track: TrackRest,
    var s3Url: String? = null
)