package pl.semidude.rockhawk.rest.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import pl.semidude.rockhawk.domain.YoutubeTrackRepository
import pl.semidude.rockhawk.rest.model.TrackRest
import pl.semidude.rockhawk.service.youtube.api.YoutubeRecommendationApi

@RestController
@RequestMapping("/tracks")
class TrackController(
    @Autowired private val youtubeRecommendationApi: YoutubeRecommendationApi,
    @Autowired private val youtubeTrackRepository: YoutubeTrackRepository
) {

    companion object {
        val patternId = """/watch\?v=(.+)""".toRegex()
        val patternDuration = """- Duration: (.+)\.""".toRegex()
        val patterViewCount = """(\d+) views""".toRegex()
    }

    @GetMapping("/{trackId}/recommendedNext")
    fun getRecommendedNextTrack(@PathVariable trackId: String): TrackRest {
        try {
            val recommendedTrack = youtubeRecommendationApi.getRecommendedTrackFor(trackId)
            youtubeTrackRepository.save(recommendedTrack)
            return recommendedTrack.mapToRest()
        }
        catch (e: Exception) {
            return youtubeTrackRepository.findByPlatformId(trackId)!!.mapToRest()
        }
    }
}