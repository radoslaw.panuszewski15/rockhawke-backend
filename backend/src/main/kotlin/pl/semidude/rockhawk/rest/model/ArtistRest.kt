package pl.semidude.rockhawk.rest.model

import pl.semidude.rockhawk.domain.Artist

data class ArtistRest(
    var id: String,
    var name: String
) {
    fun mapToDomain(): Artist =
        Artist(
            id = id,
            name = name
        )
}

