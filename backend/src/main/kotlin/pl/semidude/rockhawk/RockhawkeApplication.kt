package pl.semidude.rockhawk

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
open class RockhawkeApplication


fun main(args: Array<String>) {
    runApplication<RockhawkeApplication>(*args)
}