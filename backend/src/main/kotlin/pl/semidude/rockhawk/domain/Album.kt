package pl.semidude.rockhawk.domain

import org.springframework.data.mongodb.core.mapping.DBRef
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.repository.MongoRepository
import pl.semidude.rockhawk.config.mongo.cascade.CascadeSave
import pl.semidude.rockhawk.rest.model.AlbumRest

@Document(collection = "albums")
data class Album(
    var id: String,
    var name: String,
    @DBRef @CascadeSave var artists: List<Artist>
) {
    fun mapToRest(): AlbumRest =
        AlbumRest(
            id = id,
            name = name,
            artists = artists.map { it.mapToRest() }
        )
}

interface AlbumRepository : MongoRepository<Artist, String> {

}