package pl.semidude.rockhawk.domain

import org.springframework.data.annotation.TypeAlias
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.repository.MongoRepository
import pl.semidude.rockhawk.config.mongo.inheritance.EnableMongoInheritance
import pl.semidude.rockhawk.rest.model.TrackRest

@Document(collection = "tracks")
@EnableMongoInheritance
@TypeAlias("YoutubeTrack")
data class YoutubeTrack(
    @Indexed override var platformId: String,
    override var title: String,
    override var thumbnailUrl: String,
    override var durationMillis: Long,
    override var durationFormatted: String,
    var owner: String,
    var viewCount: String,
    var imageUrl: String?,
    var s3Url: String? = null,
    var alternativeTitle: String? = null,
    var description: String? = null
) : Track {
    override var mongoId: String = "youtube:$platformId"

    override fun mapToRest(): TrackRest =
        TrackRest(
            id = platformId,
            title = title,
            thumbnailUrl = thumbnailUrl,
            imageUrl = imageUrl ?: thumbnailUrl,
            durationMillis = durationMillis,
            durationFormatted = durationFormatted,
            viewCount = viewCount,
            s3Url = s3Url,
            alternativeTitle = alternativeTitle,
            type = TrackRest.YOUTUBE,
            owner = owner
        )
}

interface YoutubeTrackRepository : MongoRepository<YoutubeTrack, String> {
    /**
     * Find YouTube track by its YouTube identifier
     */
    fun findByPlatformId(platformId: String): YoutubeTrack?

    /**
     * Find YouTube track by its title
     */
    fun findByTitle(title: String?): YoutubeTrack?
}