package pl.semidude.rockhawk.domain

import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.repository.MongoRepository
import pl.semidude.rockhawk.rest.model.ArtistRest

@Document(collection = "artists")
data class Artist(
    var id: String,
    var name: String
) {
    fun mapToRest(): ArtistRest =
        ArtistRest(
            id = id,
            name = name
        )
}

interface ArtistRepository : MongoRepository<Artist, String> {

}