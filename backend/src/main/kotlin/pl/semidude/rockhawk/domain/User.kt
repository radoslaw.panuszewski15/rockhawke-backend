package pl.semidude.rockhawk.domain

import org.springframework.data.mongodb.core.mapping.DBRef
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.repository.MongoRepository
import pl.semidude.rockhawk.config.mongo.cascade.CascadeSave

@Document(collection = "users")
data class User(
    var id: String? = null,
    var email: String? = null,
    var spotifyId: String? = null,
    var googleId: String? = null,
    var facebookId: String? = null,
    var nickname: String? = null,
    var password: String? = null,
    var lastSpotifyAccessToken: String? = null,
    var spotifyRefreshToken: String? = null,
    var lastGoogleAccessToken: String? = null,
    var googleRefreshToken: String? = null,
    var avatarUrl: String? = null,
    @DBRef @CascadeSave val library: MutableList<Track> = mutableListOf(),
    @DBRef val playlists: MutableList<Playlist> = mutableListOf()
)

interface UserRepository : MongoRepository<User, String> {
    fun findBySpotifyId(spotifyId: String): User?
    fun findByGoogleId(googleId: String): User?
    fun findByFacebookId(facebookId: String): User?
    fun findByNickname(username: String): User?
    fun findByEmail(email: String): User?
}