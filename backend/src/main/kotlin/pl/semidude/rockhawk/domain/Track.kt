package pl.semidude.rockhawk.domain

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.repository.MongoRepository
import pl.semidude.rockhawk.rest.model.TrackRest

@Document(collection = "tracks")
interface Track {
    @get:Id
    var mongoId: String
    var platformId: String
    var title: String
    var thumbnailUrl: String
    var durationMillis: Long
    var durationFormatted: String

    /**
     * Map this domain object to REST representation
     */
    fun mapToRest(): TrackRest
}

interface TrackRepository : MongoRepository<Track, String> {
    /**
     * Find track by platform identifier (either YouTube or Spotify)
     */
    fun findByPlatformId(platformId: String): Track?

    /**
     * Check if track with given platform identifier exists
     */
    fun existsByPlatformId(platformId: String): Boolean
}

fun List<Track>.mapToRest(): List<TrackRest> = this.map { it.mapToRest() }