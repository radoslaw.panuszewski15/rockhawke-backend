package pl.semidude.rockhawk.domain

data class Playback(
    val playbackId: Long
) {
    private val tracksIds: MutableList<String> = mutableListOf()

    fun appendTrack(youtubeTrackId: String) {
        tracksIds += youtubeTrackId
    }

    fun getTrackOnPosition(trackPosition: Int): String = tracksIds[trackPosition]

    fun getFirstTrack(): String = tracksIds.first()

    fun getLastTrack(): String = tracksIds.last()

    fun getLastPosition(): Int = tracksIds.lastIndex

    fun getTrackIds(): List<String> = tracksIds
}