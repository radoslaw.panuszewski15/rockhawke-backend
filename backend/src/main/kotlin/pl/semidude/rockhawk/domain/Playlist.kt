package pl.semidude.rockhawk.domain

import org.springframework.data.mongodb.core.mapping.DBRef
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.repository.MongoRepository
import pl.semidude.rockhawk.config.mongo.cascade.CascadeSave
import pl.semidude.rockhawk.rest.model.PlaylistRest

@Document(collection = "playlists")
data class Playlist(
    var id: String? = null,
    var playlistName: String,
    var ownerId: String,
    @DBRef @CascadeSave var tracks: MutableList<Track> = mutableListOf()
) {
    fun mapToRest() = PlaylistRest(
        playlistId = id,
        playlistName = playlistName,
        tracks = tracks.map { it.mapToRest() },
        tracksCount = tracks.size,
        coverImage = tracks.firstOrNull()?.thumbnailUrl
    )
}

interface PlaylistRepository : MongoRepository<Playlist, String> {
    /**
     * Find playlist by its name
     */
    fun findByPlaylistName(playlistName: String): Playlist?

    /**
     * Find playlist by its owner
     */
    fun findByOwnerId(owner: String): Playlist?
}