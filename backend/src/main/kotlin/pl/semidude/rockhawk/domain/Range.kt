@file:Suppress("RemoveRedundantQualifierName")

package pl.semidude.rockhawk.domain

import pl.semidude.rockhawk.service.youtube.audio.YoutubeAudioProvider.Companion.END_OF_TRACK
import kotlin.math.max
import kotlin.math.min

data class Range(val begin: Int, val end: Int): Comparable<Range> {

    operator fun contains(pos: Int) =
        pos in begin until end


    fun collides(other: Range): Boolean =
        !(this.begin >= other.end || other.begin >= this.end)


    fun intersection(other: Range) =
        if (!this.collides(other))
            EMPTY
        else
            Range(
                max(this.begin, other.begin),
                min(this.end, other.end)
            )


    val length: Int get() = end - begin

    fun relativeTo(startPos: Int) =
        Range(
            begin - startPos,
            if (end == END_OF_TRACK) END_OF_TRACK else end - startPos
        )


    override fun compareTo(other: Range): Int =
        begin.compareTo(other.begin)


    override fun toString(): String = "($begin-$end)"

    companion object {
        val EMPTY = Range(0, 0)
        val FULL = Range(0, END_OF_TRACK)
    }
}