package pl.semidude.rockhawk.domain

import org.springframework.data.annotation.TypeAlias
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.DBRef
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.repository.MongoRepository
import pl.semidude.rockhawk.config.mongo.cascade.CascadeSave
import pl.semidude.rockhawk.config.mongo.inheritance.EnableMongoInheritance
import pl.semidude.rockhawk.rest.model.TrackRest
import pl.semidude.rockhawk.service.spotify.api.SpotifyTrackDescriptor

@Document(collection = "tracks")
@EnableMongoInheritance
@TypeAlias("SpotifyTrack")
data class SpotifyTrack(
    @Indexed override var platformId: String,
    override var title: String,
    override var thumbnailUrl: String,
    override var durationMillis: Long,
    override var durationFormatted: String,
    @DBRef @CascadeSave var artists: List<Artist>,
    @DBRef @CascadeSave var album: Album
) : Track {
    override var mongoId: String = "spotify:$platformId"

    override fun mapToRest() = TrackRest(
        id = platformId,
        title = title,
        thumbnailUrl = thumbnailUrl,
        imageUrl = thumbnailUrl,
        durationMillis = durationMillis,
        durationFormatted = durationFormatted,
        artists = artists.map { it.mapToRest() },
        album = album.mapToRest(),
        type = TrackRest.SPOTIFY
    )

    companion object {
        fun fromDescriptor(trackDescriptor: SpotifyTrackDescriptor) =
            SpotifyTrack(
                platformId = trackDescriptor.spotifyId,
                title = trackDescriptor.name,
                durationMillis = trackDescriptor.durationMillis,
                durationFormatted = trackDescriptor.durationFormatted,
                thumbnailUrl = trackDescriptor.thumbnailUrl,
                artists = trackDescriptor.artists.map { Artist(it.spotifyId, it.name) },
                album = trackDescriptor.album.let { album -> Album(album.spotifyId, album.name, album.artists.map { Artist(it.spotifyId, it.name) }) }
            )
    }
}

interface SpotifyTrackRepository : MongoRepository<SpotifyTrack, String> {
    /**
     * Find Spotify track by its Spotify identifier
     */
    fun findByPlatformId(platformId: String): SpotifyTrack?

    /**
     * Find Spotify track by its title
     */
    fun findByTitle(title: String?): SpotifyTrack?
}