package pl.semidude.rockhawk.service.authentication

interface ThirdPartyUserConnector {
    fun connectUserUsingCode(userId: String, code: String): Boolean
}