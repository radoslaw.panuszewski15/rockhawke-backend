package pl.semidude.rockhawk.service.facebook.api

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate

interface FacebookUserApi {
    /**
     * Get user info from Facebook
     */
    fun getUserProfile(accessToken: String): FacebookUserProfile
}

@Service
internal class FacebookUserApiImpl(
    @Qualifier("restPlain") @Autowired private val rest: RestTemplate
) : FacebookUserApi {

    override fun getUserProfile(accessToken: String): FacebookUserProfile {

        val requestUrl = "$ME_ENDPOINT?fields=id,email,name"

        val headers = HttpHeaders().apply {
            accept = listOf(MediaType.APPLICATION_JSON)
            set("Authorization", "Bearer $accessToken")
        }

        val entity = HttpEntity("", headers)

        return rest.exchange(requestUrl, HttpMethod.GET, entity, FacebookUserProfile::class.java).body
    }

    companion object {
        const val ME_ENDPOINT = "https://graph.facebook.com/me"
    }
}