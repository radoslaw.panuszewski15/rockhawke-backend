package pl.semidude.rockhawk.service.authentication

import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import org.springframework.web.client.HttpClientErrorException
import pl.semidude.rockhawk.domain.Playlist
import pl.semidude.rockhawk.domain.PlaylistRepository
import pl.semidude.rockhawk.domain.User
import pl.semidude.rockhawk.domain.UserRepository
import pl.semidude.rockhawk.helpers.getById
import pl.semidude.rockhawk.service.spotify.api.SpotifyRequestTokenResponse
import pl.semidude.rockhawk.service.spotify.api.SpotifyTokenApi
import pl.semidude.rockhawk.service.spotify.api.SpotifyTracksApi
import pl.semidude.rockhawk.service.spotify.api.SpotifyUserApi

/**
 * Tries to authenticate user via Spotify authorization code sent from client app to the /oauth/token endpoint
 */
@Service
@Qualifier("spotify")
class SpotifyAuthenticator(
    @Autowired private val spotifyTokenApi: SpotifyTokenApi,
    @Autowired private val spotifyUserApi: SpotifyUserApi,
    @Autowired private val spotifyTracksApi: SpotifyTracksApi,
    @Autowired private val userRepository: UserRepository,
    @Autowired private val playlistRepository: PlaylistRepository
) : AbstractThirdPartyAuthenticationProvider(fieldName = "spotify_code", authorityName = "SPOTIFY_AUTHORITY"),
    ThirdPartyUserConnector {

    private val logger = LogManager.getLogger()

    /**
     * Performs the authentication.
     * Creates new user on first login. If one already exists, just updates it's Spotify access token.
     * @return userId if successful, null otherwise
     */
    override fun authenticateWithCode(code: String): String? {
        try {
            logger.info("Incoming Spotify authentication with code $code")

            val tokens = spotifyTokenApi.exchangeToken(code)
            val spotifyUserProfile = spotifyUserApi.getUserProfile(tokens.accessToken)
            var user = userRepository.findByEmail(spotifyUserProfile.email!!)

            if (user != null) {
                user.spotifyId = spotifyUserProfile.spotifyId
                user.lastSpotifyAccessToken = tokens.accessToken
                user.spotifyRefreshToken = tokens.refreshToken
                if (user.email == null) user.email = spotifyUserProfile.email
                if (user.nickname == null) user.nickname = spotifyUserProfile.spotifyId
                if (user.avatarUrl == null) user.avatarUrl = spotifyUserProfile.avatarUrl
            }
            else {
                user = User(
                    email = spotifyUserProfile.email,
                    spotifyId = spotifyUserProfile.spotifyId,
                    nickname = spotifyUserProfile.spotifyId,
                    lastSpotifyAccessToken = tokens.accessToken,
                    spotifyRefreshToken = tokens.refreshToken,
                    avatarUrl = spotifyUserProfile.avatarUrl
                )
            }
//            importSpotifyLikedSongs(user, tokens)
            userRepository.save(user)
            return user.id
        }
        catch (e: HttpClientErrorException.BadRequest) {
            return null
        }
    }

    override fun connectUserUsingCode(userId: String, code: String): Boolean {
        try {
            val tokens = spotifyTokenApi.exchangeToken(code)
            val spotifyUserProfile = spotifyUserApi.getUserProfile(tokens.accessToken)

            val user = userRepository.getById(userId)
                ?: throw IllegalArgumentException("user with id $userId not found in database and thus cannot be connected to Spotify")

            user.spotifyId = spotifyUserProfile.spotifyId
            user.lastSpotifyAccessToken = tokens.accessToken
            user.spotifyRefreshToken = tokens.refreshToken
            if (user.email == null) user.email = spotifyUserProfile.email
            if (user.nickname == null) user.nickname = spotifyUserProfile.spotifyId
            if (user.avatarUrl == null) user.avatarUrl = spotifyUserProfile.avatarUrl
//            importSpotifyLikedSongs(user, tokens)
            userRepository.save(user)
            return true
        }
        catch (e: HttpClientErrorException.BadRequest) {
            return false
        }
    }

    private fun importSpotifyLikedSongs(user: User, tokens: SpotifyRequestTokenResponse) {
        if (user.playlists.none { it.playlistName == "Spotify liked songs" }) {
            val spotifyUserTracks = spotifyTracksApi.getUserTracks(tokens.accessToken, tokens.refreshToken)
            val playlist = playlistRepository.save(Playlist(playlistName = "Spotify liked songs", ownerId = user.id!!, tracks = spotifyUserTracks.toMutableList()))
            user.playlists.add(playlist)
        }
    }
}