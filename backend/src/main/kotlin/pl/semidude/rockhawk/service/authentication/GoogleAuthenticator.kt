package pl.semidude.rockhawk.service.authentication

import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import org.springframework.web.client.HttpClientErrorException
import pl.semidude.rockhawk.domain.User
import pl.semidude.rockhawk.domain.UserRepository
import pl.semidude.rockhawk.helpers.getById
import pl.semidude.rockhawk.service.google.api.GoogleTokenApi


/**
 * Tries to authenticate user via Google authorization code sent from client app to the /oauth/token endpoint
 */
@Qualifier("google")
@Service
class GoogleAuthenticator(
    @Autowired private val googleTokenApi: GoogleTokenApi,
    @Autowired private val userRepository: UserRepository
) : AbstractThirdPartyAuthenticationProvider(fieldName = "google_code", authorityName = "GOOGLE_AUTHORITY"),
    ThirdPartyUserConnector {

    private val logger = LogManager.getLogger()

    /**
     * Performs the authentication.
     * Creates new user on first login. If one already exists, just updates it's Google access token.
     * @return userId if successful, null otherwise
     */
    override fun authenticateWithCode(code: String): String? {
        try {
            logger.info("Incoming Google authentication with code $code")

            val googleUserProfile = googleTokenApi.exchangeToken(code)

            var user = userRepository.findByEmail(googleUserProfile.email)

            if (user != null) {
                user.googleId = googleUserProfile.googleId
                user.lastGoogleAccessToken = googleUserProfile.accessToken
                user.googleRefreshToken = googleUserProfile.refreshToken
                if (user.email == null) user.email = googleUserProfile.email
                if (user.nickname == null) user.nickname = googleUserProfile.name
                if (user.avatarUrl == null) user.avatarUrl = googleUserProfile.pictureUrl
            }
            else {
                user = User(
                    email = googleUserProfile.email,
                    googleId = googleUserProfile.googleId,
                    nickname = googleUserProfile.name,
                    avatarUrl = googleUserProfile.pictureUrl,
                    lastGoogleAccessToken = googleUserProfile.accessToken,
                    googleRefreshToken = googleUserProfile.refreshToken
                )
            }

            userRepository.save(user)
            return user.id
        }
        catch (e: HttpClientErrorException.BadRequest) {
            return null
        }
    }

    override fun connectUserUsingCode(userId: String, code: String): Boolean {

        val googleUserProfile = googleTokenApi.exchangeToken(code)

        val user = userRepository.getById(userId)
            ?: throw IllegalArgumentException("User with id $userId not found in database and thus cannot be connected to Spotify")

        user.googleId = googleUserProfile.googleId
        user.lastGoogleAccessToken = googleUserProfile.accessToken
        user.googleRefreshToken = googleUserProfile.refreshToken
        if (user.email == null) user.email = googleUserProfile.email
        if (user.nickname == null) user.nickname = googleUserProfile.name
        if (user.avatarUrl == null) user.avatarUrl = googleUserProfile.pictureUrl

        userRepository.save(user)
        return false
    }
}