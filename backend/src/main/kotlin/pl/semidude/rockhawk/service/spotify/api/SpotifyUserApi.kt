package pl.semidude.rockhawk.service.spotify.api

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate

interface SpotifyUserApi {
    /**
     * Get user info from Spotify
     */
    fun getUserProfile(accessToken: String): SpotifyCurrentUserProfileResponse
}

@Service
class SpotifyUserApiImpl(
    @Qualifier("restPlain") private val restTemplate: RestTemplate
) : SpotifyUserApi {

    override fun getUserProfile(accessToken: String): SpotifyCurrentUserProfileResponse {

        val headers = HttpHeaders().apply {
            accept = listOf(MediaType.APPLICATION_JSON)
            set("Authorization", "Bearer $accessToken")
        }

        val entity = HttpEntity("", headers)

        return restTemplate.exchange(meApiUrl, HttpMethod.GET, entity, SpotifyCurrentUserProfileResponse::class.java).body
    }

    companion object {
        private const val meApiUrl = "https://api.spotify.com/v1/me"
        private const val usersApiUrl = "https://api.spotify.com/v1/users"
    }
}