package pl.semidude.rockhawk.service.youtube.audio

/**
 * Thrown when the range given in HTTP request is not valid
 */
class RangeException(override val message: String?) : Exception()

/**
 * Thrown when cache for given hasn't been initialized
 */
class AudioProviderNotAvailableException : Exception()

/**
 * Thrown when extracted URL from youtube-dl is not valid
 */
class MalformedUrlException(val url: String) : Exception()