package pl.semidude.rockhawk.service.youtube.api

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.http.*
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate
import pl.semidude.rockhawk.domain.YoutubeTrack
import pl.semidude.rockhawk.helpers.makeList
import java.util.*

@Service
@Qualifier("unofficial")
class YoutubeSearchApiUnofficial(
    @Qualifier("restPlain") private val restTemplate: RestTemplate
) : YoutubeSearchApi {

    override fun searchTracks(query: String): List<YoutubeTrack> {

        val root: JsonNode = sendYoutubeSearchRequest(query)

        //in YouTube's terminology, renderer is a structure representing something to be rendered on YT's search result list
        //afaik it could be plain video miniature, playlist or card panel
        val renderers: JsonNode = root.extractRenderers()

        return makeList { tracks ->

            for (renderer in renderers) {
                //skip things like playlists and card panels
                if (!renderer.has("videoRenderer"))
                    continue

                val youtubeTrackId = renderer.extractId()

                val durationFormatted = renderer.extractDuration()
                val colonCount = durationFormatted.count { c -> c == ':' }

                val scanner = Scanner(durationFormatted.replace(":", " "))

                val durationMillis = when (colonCount) {
                    1 -> (scanner.nextInt() * 60 + scanner.nextInt()) * 1000
                    2 -> (scanner.nextInt() * 3600 + scanner.nextInt() * 60 + scanner.nextInt()) * 1000
                    else -> throw IllegalArgumentException("illegal colon count ($colonCount) in duration text '$durationFormatted'")
                }

                tracks += YoutubeTrack(
                        platformId = youtubeTrackId,
                        title = renderer.extractTitle(),
                        durationMillis = durationMillis.toLong(),
                        durationFormatted = durationFormatted,
                        description = renderer.extractDescription(),
                        thumbnailUrl = renderer.extractThumbnailUrl(),
                        imageUrl = "https://i.ytimg.com/vi/$youtubeTrackId/hqdefault.jpg",
                        viewCount = renderer.extractViewCount(),
                        owner = renderer.extractOwner()
                )
            }
        }
    }

    private fun sendYoutubeSearchRequest(query: String): JsonNode {
        val url = "https://www.youtube.com/results?search_query=$query&pbj=1"

        val headers = HttpHeaders().apply {
            accept = listOf(MediaType.APPLICATION_JSON)
//            set("cookie", "CONSENT=YES+PL.pl+20150719-14-0; VISITOR_INFO1_LIVE=bF55ARQZ-Ss; LOGIN_INFO=AFmmF2swRgIhAJSO_3uAoZE0EbwTK34-a8pcdM6CQVAAVHqRJQLGArLAAiEAunGA9OJLmLkxNUwzg8eWsvR9MFl346ryUBLyDBa278Q:QUQ3MjNmellfYlZ2dExyY2NiU01yUlpHVUhoXzRzZFJDaEdCX3RYR3RkenNpU1BWOHh1MnhYNnNrekFmVzFfd1R2OUNKX1ZXX01zaHVuaDRRaE1MQThidlpFLWNOV3JvWFFNYm5yQ3FDNGFjWmxnSmU1Yk9IaEd1elp1bG5seGVFNDctd3NkZTdSOHlrYThoZTN2Wi0yQ2Q4MndGaUtsck1QcnRxeEhUckpYOWNjV3BDc2pUVUd3; PREF=al=pl&f1=50000000; SID=cwc_4yqgfnI-NxYpeCbSEVpupNpgMeN97oP1xZZVIUrHMwwZuAmKAQCugl1VIuKnNGbNqQ.; HSID=ATnmgeBBXrYB1vSGm; SSID=AvYn0OelTttSckPDD; APISID=ilorUIC_vctwxUe7/A8d7GpErixaVWR-ZL; SAPISID=MqhWZ3TmEcG54Q2h/AmlKvRChHksvVpEYW; _ga=GA1.2.992517433.1559851670; YSC=VJqwy0kSOqI; SIDCC=AN0-TYvlhcHqmaoHi1F1msrGxyLNq8HPoKXOHVDHScUEcOLgxGgTqB6BZnSf01DMNsg1sNCvHw; ST-nbxp6=oq=witam&gs_l=youtube.12...0.0.1.248872.0.0.0.0.0.0.0.0..0.0....0...1ac..64.youtube..0.0.0....0.Ny6OWjqJj3Y&feature=web-masthead-search&itct=CBgQ7VAiEwjKi9zc2NfjAhWdZrIKHeOTBys%3D&csn=iZ89XajiGJPEyQW97LTQBg")
            set("cookie", "CONSENT=YES+PL.pl+20150719-14-0; VISITOR_INFO1_LIVE=bF55ARQZ-Ss; PREF=al=pl&f1=50000000; _ga=GA1.2.992517433.1559851670; YSC=tnj6hJoYBKc; SID=mwc_46TQcbJtf-BtOqD_pwx3Zo0MfMr7kmy7fLPSkb_XAAm5kZEep2sLhERQSzKteWrWHQ.; HSID=AdBjvsyIRApYJ8h8X; SSID=AOgYnIISNojWeprix; APISID=s87w3hhuuVtMGMAT/AALVol9-7PebsPO-N; SAPISID=zpFjDcyAYI96Qtso/AGWVD8HS9i5UxM8HE; LOGIN_INFO=AFmmF2swRQIhAPUoAN5jM5YJgEhZIg2SOqJNIVpkQkoQ3OJhcXoFsK3NAiBSA1KErfOn5FrB5GkE3DmF4NHDGKeb5IliydnTZzYG6A:QUQ3MjNmenFoQzhBZzk0VU0wWGRQUkFsU0huc01GQW4tMlRGZDI2UnNLbEp0VzBaSkhIUF9qa0YtR3ZocnJyMWZUdC1ZRTc0VkNLTzdJb3pfVFBTOHprQ3BHdDlYU1dSTDVrRFpiYXp1TmFQOUl1SXdRT0pydjRvMHRBRzhOQmhubk1fT2I4Z0lFY2FFNjhyU05EVlRLR01Kam5jTUE0NEhxaTU1WmZzNG0wZDVwMEI5cXFPUjdV; SIDCC=AN0-TYuHzxIBryI2eTYoBqIgWvBbZ3LFU3UIEwTF6M_ijvfLO0A2T34IIYQ5OwC8SSbzyl58CQ; ST-14ustad=oq=three%20days%20grace&gs_l=youtube.3..35i39k1l2j0i131k1j0j0i131k1j0l3j0i131k1j0.1175.7129.0.8001.17.17.0.0.0.0.127.1225.13j3.17.0....0...1ac.1.64.youtube..0.17.1381.6..0i229k1.158.C6L0Y_nZXO4&feature=web-masthead-search&itct=CBgQ7VAiEwifmtH73-njAhUERxUKHU0ECTw%3D&csn=DBdHXcrJI5GvhAfSoIyIBA")
            set("x-youtube-client-version", "2.20190725")
            set("x-youtube-client-name", "1")
            set("x-youtube-identity-token", "QUFFLUhqbnJuLWczRWZSckpzNTBQNHlwelY4LXREUXRBQXw=")
        }
        val entity = HttpEntity("body", headers)
        val response: ResponseEntity<String> = restTemplate.exchange(url, HttpMethod.GET, entity, String::class.java)
        return objectMapper.readTree(response.body)
    }

    private fun JsonNode.extractRenderers(): JsonNode {
        assertIsRootNode()

        return this[1]
            .path("response")
            .path("contents")
            .path("twoColumnSearchResultsRenderer")
            .path("primaryContents")
            .path("sectionListRenderer")
            .path("contents")[0]
            .path("itemSectionRenderer")
            .path("contents")
    }

    private fun JsonNode.extractId(): String {
        assertIsVideoRenderNode()

        return this
            .path("videoRenderer")
            .path("videoId").asText()
    }

    private fun JsonNode.extractTitle(): String {
        assertIsVideoRenderNode()

        return this
            .path("videoRenderer")
            .path("title")
            .path("runs")[0]
            .path("text").asText()
    }

    private fun JsonNode.extractDuration(): String {
        assertIsVideoRenderNode()

        return this
            .path("videoRenderer")
            .path("lengthText")
            .path("simpleText").asText()
    }

    private fun JsonNode.extractViewCount(): String {
        assertIsVideoRenderNode()

        return this
            .path("videoRenderer")
            .path("shortViewCountText")
            .path("simpleText").asText().replace(" wyświetleń", "")
    }

    private fun JsonNode.extractDescription(): String? {
        assertIsVideoRenderNode()

        return this
            .path("videoRenderer")
            ?.path("descriptionSnippet")
            ?.path("runs")?.get(0)
            ?.path("text")?.asText()
    }

    private fun JsonNode.extractThumbnailUrl(): String {
        assertIsVideoRenderNode()

        return this
            .path("videoRenderer")
            .path("thumbnail")
            .path("thumbnails")[0]
            .path("url").asText()
    }

    private fun JsonNode.extractOwner(): String {
        assertIsVideoRenderNode()

        return this
            .path("videoRenderer")
            .path("ownerText")
            .path("runs")[0]
            .path("text").asText()

    }

    private fun JsonNode.assertIsRootNode() {
        if (!this.isArray || this.size() != 2 || !this[1].has("response"))
            throw IllegalArgumentException("renderers could be extracted only from root node")
    }

    private fun JsonNode.assertIsVideoRenderNode() {
        if (!this.has("videoRenderer"))
            throw IllegalArgumentException("title could be extracted only from videoRenderer node")
    }

    companion object {
        //Creation of [ObjectMapper] is expensive, so it's kept as a static member
        val objectMapper = ObjectMapper()
    }
}