@file:Suppress("UNCHECKED_CAST")

package pl.semidude.rockhawk.service.authentication

import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.GrantedAuthority
import org.springframework.stereotype.Service
import org.springframework.web.client.HttpClientErrorException
import pl.semidude.rockhawk.domain.User
import pl.semidude.rockhawk.domain.UserRepository
import pl.semidude.rockhawk.service.facebook.api.FacebookUserApi

@Service
@Qualifier("facebook")
class FacebookAuthenticator(
    @Autowired private val facebookUserApi: FacebookUserApi,
    @Autowired private val userRepository: UserRepository
) : AuthenticationProvider {

    private val logger = LogManager.getLogger()

    override fun authenticate(authentication: Authentication): Authentication? {

        if (authentication is UsernamePasswordAuthenticationToken) {

            val detailsMap = (authentication.details as Map<String, String>).toMutableMap()
            val shortLivedToken = detailsMap["facebook_token"]

            val userId = shortLivedToken?.let { token -> authenticateWithToken(token) }

            if (userId != null) {
                logger.info("AUTHENTICATED USER WITH ID = $userId")

                detailsMap["user_id"] = userId

                return UsernamePasswordAuthenticationToken(
                    authentication.principal,
                    authentication.credentials,
                    listOf(GrantedAuthority { "FACEBOOK_AUTHORITY" })
                ).apply { details = detailsMap }
            }
        }
        return null
    }

    private fun authenticateWithToken(accessToken: String): String? {
        try {
            logger.info("Incoming Facebook authentication with short-lived token $accessToken")

            val facebookUserProfile = facebookUserApi.getUserProfile(accessToken)
            var user = userRepository.findByEmail(facebookUserProfile.email!!)

            if (user != null) {
                user.facebookId = facebookUserProfile.id
                if (user.email == null) user.email = facebookUserProfile.email
                if (user.nickname == null) user.nickname = facebookUserProfile.name
            }
            else {
                user = User(
                    email = facebookUserProfile.email,
                    facebookId = facebookUserProfile.id,
                    nickname = facebookUserProfile.name
                )
            }
            userRepository.save(user)
            return user.id
        }
        catch (e: HttpClientErrorException.BadRequest) {
            return null
        }
    }

    override fun supports(authentication: Class<*>): Boolean {
        return UsernamePasswordAuthenticationToken::class.java.isAssignableFrom(authentication)
    }
}