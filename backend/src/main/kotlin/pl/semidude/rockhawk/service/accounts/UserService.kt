package pl.semidude.rockhawk.service.accounts

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import pl.semidude.rockhawk.domain.User
import pl.semidude.rockhawk.domain.UserRepository

interface UserService {
    fun registerUser(email: String, password: String, nickname: String? = null): String
}

@Service
class UserServiceImpl(
    @Autowired private val userRepository: UserRepository,
    @Autowired private val passwordEncoder: PasswordEncoder
) : UserService {

    override fun registerUser(email: String, password: String, nickname: String?): String {
        val user = User(
            nickname = nickname,
            email = email,
            password = passwordEncoder.encode(password)
        )
        userRepository.save(user)
        return user.id!!
    }
}