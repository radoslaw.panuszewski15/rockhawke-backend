package pl.semidude.rockhawk.service.facebook.api

import org.springframework.stereotype.Service

interface FacebookTokenApi {

    /**
     * Exchange a short-lived access token, obtained from the client app, to the long-lived one
     */
    fun exchangeToken(shortLivedToken: String): FacebookTokenResponse
}

@Service
internal class FacebookTokenApiImpl : FacebookTokenApi {

    override fun exchangeToken(shortLivedToken: String): FacebookTokenResponse {
        TODO()
    }
}