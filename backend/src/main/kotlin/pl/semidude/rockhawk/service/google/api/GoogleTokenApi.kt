package pl.semidude.rockhawk.service.google.api

import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeTokenRequest
import com.google.api.client.http.javanet.NetHttpTransport
import com.google.api.client.json.jackson2.JacksonFactory
import org.springframework.stereotype.Service

interface GoogleTokenApi {
    fun exchangeToken(code: String): GoogleUserProfile
}

@Service
class GoogleTokenApiImpl : GoogleTokenApi {

    override fun exchangeToken(code: String): GoogleUserProfile {

        val request = GoogleAuthorizationCodeTokenRequest(
            NetHttpTransport(),
            JacksonFactory.getDefaultInstance(),
            "https://oauth2.googleapis.com/token",
            CLIENT_ID,
            CLIENT_SECRET,
            code,
            REDIRECT_URI
        )

        val tokenResponse = request.execute()
        val idPayload = tokenResponse.parseIdToken().payload

        return GoogleUserProfile(
            googleId = idPayload.subject,
            email = idPayload.email,
            name = idPayload["name"] as String,
            pictureUrl = idPayload["picture"] as String,
            accessToken = tokenResponse.accessToken
        )
    }

    companion object {
        private const val CLIENT_ID = "1087689718655-d0i3mk46iiij4t3jbmcvf7hrn6cfalvu.apps.googleusercontent.com"
        private const val CLIENT_SECRET = "3eVtJ3QurwgxG3LMslNWeDQy"
        private const val REDIRECT_URI = ""
    }
}