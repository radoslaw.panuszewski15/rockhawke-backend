package pl.semidude.rockhawk.service.google.api

data class GoogleUserProfile(
    val googleId: String,
    val email: String,
    val name: String,
    val pictureUrl: String,
    val accessToken: String,
    val refreshToken: String? = null
)