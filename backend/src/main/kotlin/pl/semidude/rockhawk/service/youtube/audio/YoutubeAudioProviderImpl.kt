package pl.semidude.rockhawk.service.youtube.audio

import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.model.*
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import pl.semidude.rockhawk.domain.Range
import pl.semidude.rockhawk.domain.YoutubeTrackRepository
import pl.semidude.rockhawk.service.youtube.util.YoutubeUrlExtractor
import java.io.BufferedInputStream
import java.io.ByteArrayInputStream
import java.util.*
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock

/**
 * The responsibility of this class is to create new [TrackAudioProvider] objects and delegate
 * to them all of data-accessibility related methods.
 *
 * Also, this class takes care of uploading track audio bytes to S3 server, as soon as they become
 * fully available.
 */
@Service
class YoutubeAudioProviderImpl(
    @Autowired private val urlExtractor: YoutubeUrlExtractor,
    @Autowired private val s3: AmazonS3,
    @Autowired private val youtubeTrackRepo: YoutubeTrackRepository
) : YoutubeAudioProvider {

    private val logger = LogManager.getLogger(javaClass)

    private val trackAudioProviders = mutableMapOf<String, TrackAudioProvider>()
    private val tracksWaitingForInitialization: Queue<String> = LinkedList()
    private val initLock = ReentrantLock()

    override fun registerTrackForCache(youtubeTrackId: String) {
        if (tracksWaitingForInitialization.contains(youtubeTrackId) || isTrackInitialized(youtubeTrackId))
            return

        initLock.withLock {
            tracksWaitingForInitialization.add(youtubeTrackId)
        }
    }

    override fun initTrackCache(youtubeTrackId: String) {
        val trackAudioProvider = trackAudioProviders[youtubeTrackId]
            ?: createTrackAudioProvider(youtubeTrackId)

        trackAudioProvider.initTrackCache(urlExtractor)
    }

    override fun initRemainingCaches() {
        while (true) {
            val youtubeTrackId = initLock.withLock { tracksWaitingForInitialization.poll() }
                ?: break

            initTrackCache(youtubeTrackId)
        }
    }

    override fun isTrackInitialized(youtubeTrackId: String): Boolean =
        trackAudioProviders[youtubeTrackId]?.isInitialized ?: false


    private fun createTrackAudioProvider(youtubeTrackId: String): TrackAudioProvider {
        val provider = TrackAudioProvider(youtubeTrackId)

        provider.setOnFullBytesListener { bytes ->

            val s3Url = uploadBytesToS3(youtubeTrackId, bytes)
            logger.info("UPLOADED TRACK $youtubeTrackId TO S3")

            val track = youtubeTrackRepo.findByPlatformId(youtubeTrackId)

            if (track != null) {
                track.s3Url = s3Url
                youtubeTrackRepo.save(track)
                logger.info("SAVED S3 URL FOR $youtubeTrackId")
            }
        }

        trackAudioProviders[youtubeTrackId] = provider
        return provider
    }

    private fun uploadBytesToS3(s3Key: String, byteArray: ByteArray): String {

        if (s3.doesObjectExist(bucketName, s3Key)) {
            return s3.getUrl(bucketName, s3Key).toString()
        }
        val metadata = ObjectMetadata().apply {
            contentType = "audio/webm"
            contentLength = byteArray.size.toLong()
        }
        val request = PutObjectRequest(bucketName, s3Key, ByteArrayInputStream(byteArray), metadata)
        request.accessControlList = AccessControlList().apply {
            grantAllPermissions(Grant(GroupGrantee.AllUsers, Permission.FullControl))
        }
        s3.putObject(request)
        return s3.getUrl(bucketName, s3Key).toString()
    }


    override fun openAudioStream(youtubeTrackId: String, startPos: Int): BufferedInputStream =
        trackAudioProviders[youtubeTrackId]?.openAudioStream(startPos)
            ?: noAudioProvider()


    override fun getByteRange(youtubeTrackId: String, range: Range): ByteArray =
        trackAudioProviders[youtubeTrackId]?.getByteRange(range)
            ?: noAudioProvider()


    override fun isTrackFullyCached(youtubeTrackId: String): Boolean = trackAudioProviders[youtubeTrackId]?.isTrackFullyCached()
        ?: false


    override fun getFullTrackSize(youtubeTrackId: String): Int = trackAudioProviders[youtubeTrackId]?.getFullTrackSize()
        ?: noAudioProvider()

    override fun pauseCaching(youtubeTrackId: String) {
        trackAudioProviders[youtubeTrackId]?.pauseCaching()
    }

    override fun resumeCaching(youtubeTrackId: String) {
        trackAudioProviders[youtubeTrackId]?.resumeCaching()
    }

    private fun noAudioProvider(): Nothing =
        throw AudioProviderNotAvailableException()

    companion object {
        private const val bucketName = "rockhawke-track-cache"
    }
}