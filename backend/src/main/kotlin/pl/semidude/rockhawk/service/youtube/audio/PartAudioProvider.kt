package pl.semidude.rockhawk.service.youtube.audio

import org.apache.logging.log4j.LogManager
import pl.semidude.rockhawk.domain.Range
import pl.semidude.rockhawk.helpers.readBytesBuffered
import pl.semidude.rockhawk.helpers.submitCallable
import pl.semidude.rockhawk.service.youtube.audio.YoutubeAudioProvider.Companion.END_OF_TRACK
import java.io.BufferedInputStream
import java.io.InputStream
import java.net.MalformedURLException
import java.net.URL
import java.util.concurrent.Executors
import java.util.concurrent.Future
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.thread
import kotlin.concurrent.withLock
import kotlin.math.max

/**
 * Utility class encapsulating access to continuous part of a given YouTube track.
 * The part's beginning is immutable since creation, but it's end can be changed dynamically
 * (and it will be changed as soon as another [PartAudioProvider] is created and assigned to
 * latter bytes of this part).
 *
 * @param youtubeTrackId id of the track which part is to be cached
 * @param audioUrl extracted URL to raw audio bytes (full bytes, not only this particular part)
 * @param begin beginning of the part, immutable since creation, inclusive
 * @param end end of the part, dynamically changeable, exclusive
 *
 * @see YoutubeAudioProvider
 * @see TrackAudioProvider
 */
class PartAudioProvider(
    private val youtubeTrackId: String,
    private val audioUrl: String,
    range: Range
) {
    val begin: Int = range.begin

    var end: Int = range.end
        set(value) = cacheLock.withLock { field = value }

    val range: Range
        get() = Range(begin, end)

    private val logger = LogManager.getLogger(javaClass)
    private var onFullBytesListener: (() -> Unit)? = null

    private var trackCache = ByteArray(0)
    private var isFullyCached = false
    private var isCachingStarted = AtomicBoolean(false)

    private val cacheLock = ReentrantLock()
    private val enoughBytesCachedCondition = cacheLock.newCondition()

    private val pauseLock = ReentrantLock()
    private val resumedCondition = pauseLock.newCondition()
    private var isCachingPaused = false

    private val executor = Executors.newCachedThreadPool()

    /**
     * Open [InputStream] for reading track bytes from start to end, asynchronously.
     * Automatically resumes track caching if paused.
     * @param startPos the start position, from which the reading should begin; relative to beginning this part
     * @return the input stream
     */
    fun openAudioStream(startPos: Int = 0): BufferedInputStream {

        if (!isCachingStarted.getAndSet(true)) {
            val youtubeBytesStream = openYoutubeBytesStream(audioUrl)
            runCachingThread(youtubeBytesStream)
        }
        else if (!isPartFullyCached()) {
            resumeCaching()
        }
        return BufferedInputStream(DynamicByteArrayInputStream(startPos))
    }

    /**
     * Get relative bytes sub-range (i.e. range taken of this part of the track)
     * @param begin relative beginning of the range, inclusive
     * @param end relative end of the range, exclusive
     * @return [ByteArray] containing the sub-range
     */
    fun getByteRange(range: Range): ByteArray {
        checkIfAccessValid(range)

        val begin = range.begin
        val end = resolveEnd(range)

        checkRange(begin, end)

        return trackCache.copyOfRange(begin, end)
    }

    private fun checkIfAccessValid(range: Range) {

        if (range.end == END_OF_TRACK)
            check(isPartFullyCached()) { notFullyCachedYetMessage(range) }
        else
            check(isPositionAlreadyCached(range.end - 1)) { positionNotCachedYetMessage(range) }
    }

    private fun resolveEnd(range: Range)
        = if (range.end == END_OF_TRACK) trackCache.size
        else range.end


    private fun checkRange(rangeFrom: Int, rangeTo: Int) {
        if (rangeFrom < 0)
            throw RangeException("rangeFrom ($rangeFrom) < 0")

        if (rangeFrom > rangeTo)
            throw RangeException("rangeFrom ($rangeFrom) > rangeTo ($rangeTo)")
    }


    /**
     * Get absolute range of this [PartAudioProvider]
     */
    fun getAbsoluteByteRange(range: Range)
        = getByteRange(range.relativeTo(begin))

    /**
     * Get bytes that are available at current stage of caching
     */
    fun getAvailableBytes(): ByteArray
        = trackCache


    /**
     * Get full bytes of the track or throw exception if they're not available yet
     */
    fun getFullBytes(): ByteArray {
        check(isPartFullyCached()) { "cannot get full bytes of track which has not been fully cached yet ($youtubeTrackId)" }
        return trackCache
    }

    /**
     * Get [Future] object which will provide full bytes of the track as soon as they're available
     */
    fun getFullBytesFuture(): Future<ByteArray>
        = executor.submitCallable {
            cacheLock.withLock {
                while (!isPartFullyCached()) {
                    enoughBytesCachedCondition.await()
                }
            }
            trackCache
        }

    /**
     * Block execution until full bytes of this part are available
     */
    fun waitForFullBytes() {
        getFullBytesFuture().get()
    }

    /**
     * Set listener which will get notified when full bytes of this part have been cached
     */
    fun setOnFullBytesListener(listener: () -> Unit) {
        onFullBytesListener = listener
    }

    /**
     * Determines if this part already has all its bytes available
     */
    fun isPartFullyCached(): Boolean
        = isFullyCached


    /**
     * Determines if given position (absolute within track, not relative to this part) has been already cached
     * and byte on this position is available
     */
    fun isPositionAlreadyCached(pos: Int): Boolean
        = pos < begin + trackCache.size

    /**
     * Get full size of this part if it's already fully cached or throw an exception if it's not
     */
    fun getFullPartSize(): Int =
        if (isFullyCached) trackCache.size
        else throw IllegalStateException("cannot get full size of track that has not been fully cached yet ($youtubeTrackId)")


    /**
     * Start the process of caching
     */
    fun startCaching() {
        if (!isCachingStarted.getAndSet(true)) {
            val youtubeBytesStream = openYoutubeBytesStream(audioUrl)
            runCachingThread(youtubeBytesStream)
        }
    }

    /**
     * Pause the process of caching
     */
    fun pauseCaching() {
        pauseLock.withLock {
            isCachingPaused = true
        }
    }

    /**
     * Resume the process of caching if it has been paused before
     */
    fun resumeCaching() {
        pauseLock.withLock {
            isCachingPaused = false
            resumedCondition.signalAll()
        }
    }

    /**
     * Start the caching main loop in a new thread
     */
    private fun runCachingThread(inputStream: BufferedInputStream) {
        thread {
            inputStream.readBytesBuffered(bufferLength = 16 * 1024) { buffer, bytesRead ->

                val completed = appendBytesToCache(buffer, bytesRead)

                if (completed) return@thread

                pauseIfRequested()
            }

            cacheLock.withLock {
                markAsCompleted(begin, begin + trackCache.size)

                signalNewBytesAppeared()
            }
            onFullBytesListener?.invoke()
        }
    }

    private fun openYoutubeBytesStream(url: String): BufferedInputStream {
        try {
            val connection = URL(url).openConnection()

            connection.setRequestProperty("Range", "bytes=$begin-")

            return BufferedInputStream(connection.getInputStream())
        }
        catch (e: MalformedURLException) {
            throw MalformedUrlException(url)
        }
    }


    private fun pauseIfRequested() {
        pauseLock.withLock {
            if (isCachingPaused) {
                resumedCondition.await()
            }
        }
    }

    /**
     * Append given bytes to the cache
     * @return true if caching has completed
     */
    private fun appendBytesToCache(buffer: ByteArray, bytesRead: Int): Boolean {
        cacheLock.withLock {

            trackCache += buffer.copyOfRange(0, bytesRead)

            if (trackCache.size >= range.length && end != END_OF_TRACK) {

                markAsCompleted(begin, end)

                trimCacheIfNeeded()

                onFullBytesListener?.invoke()
            }
            signalNewBytesAppeared()
        }

        return isFullyCached
    }

    private fun markAsCompleted(begin: Int, end: Int) {
        isFullyCached = true
        logCompletedCaching(begin, end)
    }

    private fun trimCacheIfNeeded() {
        if (trackCache.size > range.length)
            trackCache = trackCache.copyOfRange(0, range.length)
    }

    private fun signalNewBytesAppeared() {
        enoughBytesCachedCondition.signalAll()
    }

    /**
     * Utility class implementing [InputStream] for reading bytes from dynamically growing [ByteArray]
     */
    private inner class DynamicByteArrayInputStream(startPos: Int = 0) : InputStream() {

        private var pos = startPos

        override fun read(): Int {
            if (isPartFullyCached() && pos >= trackCache.size) {
                return -1
            }

            cacheLock.withLock {
                while (!isPartFullyCached() && pos >= trackCache.size) {
                    enoughBytesCachedCondition.await()
                }

                val byte = trackCache[pos++].toInt()
                return byte and 0xFF
            }
        }

        override fun available(): Int
            = max(0, trackCache.size - pos)
    }


    private fun logCompletedCaching(begin: Int, end: Int) {
        logger.info("COMPLETED CACHING BYTES PART ($begin-$end) FOR $youtubeTrackId")
    }

    private fun positionNotCachedYetMessage(range: Range): String =
            "cannot get byte range which has not been cached yet $range from " +
            "part provider ($begin-$end) trackCache.size = ${trackCache.size} " +
            "isPartFullyCached=$isFullyCached"


    private fun notFullyCachedYetMessage(range: Range) =
            "cannot get byte range (${range.begin}-END_OF_TRACK) of " +
            "part that has not been fully cached yet"


    //this class could not be data class (because of custom property setters), so implemented explicitly
    override fun toString(): String =
        "PartAudioProvider(youtubeTrackId='$youtubeTrackId', begin=$begin, end=$end)"


    //this class could not be data class (because of custom property setters), so implemented explicitly
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as PartAudioProvider

        if (youtubeTrackId != other.youtubeTrackId) return false
        if (audioUrl != other.audioUrl) return false
        if (begin != other.begin) return false
        if (end != other.end) return false

        return true
    }

    //this class could not be data class (because of custom property setters), so implemented explicitly
    override fun hashCode(): Int {
        var result = youtubeTrackId.hashCode()
        result = 31 * result + audioUrl.hashCode()
        result = 31 * result + begin
        result = 31 * result + end
        return result
    }

}