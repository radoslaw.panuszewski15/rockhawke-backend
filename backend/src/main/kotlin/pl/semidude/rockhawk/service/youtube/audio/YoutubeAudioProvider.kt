package pl.semidude.rockhawk.service.youtube.audio

import pl.semidude.rockhawk.domain.Range
import java.io.BufferedInputStream
import java.io.InputStream

/**
 * Object implementing this interface will encapsulate the access to YouTube track's audio data - either
 * by returning simple byte ranges or by streaming the underlying bytes in asynchronous manner.
 *
 * It acts as a main entry point for audio-providing functionality for any YouTube track.
 *
 * @see TrackAudioProvider
 * @see PartAudioProvider
 */
interface YoutubeAudioProvider {

    /**
     * Register YouTube track for further cache initialization (which is time-consuming thus should be postponed)
     * @param youtubeTrackId id of the track
     */
    fun registerTrackForCache(youtubeTrackId: String)

    /**
     * Initialize cache for given track by extracting audio URL and saving it for further processing
     * This method should always be called for given track before any other method operating on this track
     * @param youtubeTrackId id of the track
     */
    fun initTrackCache(youtubeTrackId: String)

    /**
     * Call [initTrackCache] on each registered track, which has not been initialized yet
     */
    fun initRemainingCaches()

    /**
     * Determine if given track has been initialized already
     * @param youtubeTrackId id of the track
     */
    fun isTrackInitialized(youtubeTrackId: String): Boolean

    /**
     * Open [InputStream] for reading track bytes from start to end, asynchronously.
     * Automatically resumes track caching if paused
     * @param youtubeTrackId id of the track
     * @return the input stream
     */
    fun openAudioStream(youtubeTrackId: String, startPos: Int): BufferedInputStream

    /**
     * Get [ByteArray] of specified range synchronously
     * @param youtubeTrackId id of the track
     * @param range the requested range
     * @return [ByteArray] containing audio bytes from specified range of the track
     */
    fun getByteRange(youtubeTrackId: String, range: Range): ByteArray

    /**
     * Determines if track has been fully cached already
     * @param youtubeTrackId id of the track
     * @return true if track has been fully cached, false otherwise
     */
    fun isTrackFullyCached(youtubeTrackId: String): Boolean

    /**
     * Gets size of fully cached track or 0 if track is not fully cached
     * @param youtubeTrackId id of the track
     * @return the size or 0 if not fully cached
     */
    fun getFullTrackSize(youtubeTrackId: String): Int

    /**
     * Pause the process of downloading and caching of the track
     * @param youtubeTrackId id of the track
     */
    fun pauseCaching(youtubeTrackId: String)

    /**
     * Resume the process of downloading and caching of the track
     * @param youtubeTrackId id of the track
     */
    fun resumeCaching(youtubeTrackId: String)


    companion object {
        const val END_OF_TRACK = Int.MAX_VALUE
    }
}