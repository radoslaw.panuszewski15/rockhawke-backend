package pl.semidude.rockhawk.service.facebook.api

import org.codehaus.jackson.annotate.JsonProperty

data class FacebookUserProfile(
    @JsonProperty("id") val id: String? = null,
    @JsonProperty("email") val email: String? = null,
    @JsonProperty("name") val name: String? = null
)