package pl.semidude.rockhawk.service.spotify.api

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.stereotype.Service
import org.springframework.web.client.HttpClientErrorException
import org.springframework.web.client.RestTemplate

interface SpotifyPlaylistsApi {
    fun getUserPlaylists(accessToken: String, refreshToken: String): List<SpotifyPlaylist>
    fun getPlaylistDetails(playlistId: String, accessToken: String, refreshToken: String): SpotifyPlaylist
    fun getPlaylistName(playlistId: String, accessToken: String, refreshToken: String): String
}

@Service
class SpotifyPlaylistsApiImpl(
    @Qualifier("restPlain") @Autowired private val rest: RestTemplate,
    @Autowired private val spotifyTokenApi: SpotifyTokenApi
) : SpotifyPlaylistsApi {

    override fun getUserPlaylists(accessToken: String, refreshToken: String): List<SpotifyPlaylist> {
        val response: SpotifyPlaylistsResponse =
            try {
                rest.exchange(MY_PLAYLISTS_ENDPOINT, HttpMethod.GET, prepareRequest(accessToken), SpotifyPlaylistsResponse::class.java).body!!
            }
            catch (e: HttpClientErrorException.Unauthorized) {
                val tokens = spotifyTokenApi.refreshToken(refreshToken)
                rest.exchange(MY_PLAYLISTS_ENDPOINT, HttpMethod.GET, prepareRequest(tokens.accessToken), SpotifyPlaylistsResponse::class.java).body!!
            }
        return response.playlists
    }

    override fun getPlaylistDetails(playlistId: String, accessToken: String, refreshToken: String): SpotifyPlaylist {
        return try {
            rest.exchange("$PLAYLIST_DETAILS_ENDPOINT/$playlistId", HttpMethod.GET, prepareRequest(accessToken), SpotifyPlaylist::class.java).body!!
        }
        catch (e: HttpClientErrorException.Unauthorized) {
            val tokens = spotifyTokenApi.refreshToken(refreshToken)
            rest.exchange("$PLAYLIST_DETAILS_ENDPOINT/$playlistId", HttpMethod.GET, prepareRequest(tokens.accessToken), SpotifyPlaylist::class.java).body!!
        }
    }

    override fun getPlaylistName(playlistId: String, accessToken: String, refreshToken: String): String {
        return try {
            rest.exchange("$PLAYLIST_DETAILS_ENDPOINT/$playlistId?fields=name", HttpMethod.GET, prepareRequest(accessToken), SpotifyPlaylist::class.java).body!!.name!!
        }
        catch (e: HttpClientErrorException.Unauthorized) {
            val tokens = spotifyTokenApi.refreshToken(refreshToken)
            rest.exchange("$PLAYLIST_DETAILS_ENDPOINT/$playlistId?fields=name", HttpMethod.GET, prepareRequest(tokens.accessToken), SpotifyPlaylist::class.java).body!!.name!!
        }
    }

    private fun prepareRequest(accessToken: String): HttpEntity<String> {
        val headers = HttpHeaders().apply {
            accept = listOf(MediaType.APPLICATION_JSON)
            setBearerAuth(accessToken)
        }
        return HttpEntity("", headers)
    }

    companion object {
        const val MY_PLAYLISTS_ENDPOINT = "https://api.spotify.com/v1/me/playlists"
        const val PLAYLIST_DETAILS_ENDPOINT = "https://api.spotify.com/v1/playlists/"
    }
}