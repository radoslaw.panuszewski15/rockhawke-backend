package pl.semidude.rockhawk.service.mongo

import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Service
import pl.semidude.rockhawk.domain.UserRepository
import java.lang.IllegalArgumentException

@Service
class MongoUserDetailsService(
    private val userRepository: UserRepository
) : UserDetailsService {

    override fun loadUserByUsername(username: String): UserDetails {

        val user = userRepository.findByEmail(username)
            ?: throw IllegalArgumentException("user $username not found in Mongo database!")

        return User(user.email, user.password, emptyList())
    }
}