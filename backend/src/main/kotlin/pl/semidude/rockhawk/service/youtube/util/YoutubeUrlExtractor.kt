package pl.semidude.rockhawk.service.youtube.util

import org.apache.commons.io.IOUtils
import org.springframework.stereotype.Service
import pl.semidude.rockhawk.helpers.getYoutubeDlCmd

interface YoutubeUrlExtractor {
    fun extractAudioUrl(youtubeTrackId: String): String
}

@Service
class YoutubeUrlExtractorImpl : YoutubeUrlExtractor {

    override fun extractAudioUrl(youtubeTrackId: String): String {
        val youtubeUrl = "https://www.youtube.com/watch?v=$youtubeTrackId"

        val process = Runtime.getRuntime().exec("${getYoutubeDlCmd()} -q -x -g $youtubeUrl")

        return IOUtils.toString(process.inputStream)
    }
}