package pl.semidude.rockhawk.service.youtube.api

import com.gargoylesoftware.htmlunit.WebClient
import com.gargoylesoftware.htmlunit.html.HtmlElement
import com.gargoylesoftware.htmlunit.html.HtmlPage
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import pl.semidude.rockhawk.domain.YoutubeTrack
import pl.semidude.rockhawk.rest.controller.TrackController
import java.util.*

@Service
@Qualifier("unofficial")
class YoutubeRecommendationApiUnofficial : YoutubeRecommendationApi {

    private val logger = LogManager.getLogger()

    override fun getRecommendedTrackFor(trackId: String): YoutubeTrack {
        val webClient = WebClient()
        webClient.options.isCssEnabled = false
        webClient.options.isJavaScriptEnabled = false
        webClient.options.isThrowExceptionOnScriptError = false

        val page: HtmlPage = webClient.getPage("https://www.youtube.com/watch?v=$trackId")

        val item: HtmlElement = page.getFirstByXPath("//div[contains(@class, 'watch-sidebar-section')]")
        val videoHref: HtmlElement = item.getFirstByXPath(""".//a[contains(@class, ' content-link spf-link  yt-uix-sessionlink      spf-link ')]""")
        val videoProperties: List<HtmlElement> = videoHref.getByXPath("./span") as List<HtmlElement>

        return createTrackDto(videoHref, videoProperties)
            .also { logger.info("RECOMMENDED SONG FOR $trackId IS ${it.title}") }
    }

    private fun createTrackDto(videoHref: HtmlElement, videoProperties: List<HtmlElement>): YoutubeTrack {
        val hrefRaw = videoHref.attributes.getNamedItem("href").nodeValue
        val titleRaw = videoProperties[0].asText()
        val durationRaw = videoProperties[1].asText()
        val ownerRaw = videoProperties[2].asText()
        val viewCountRaw = videoProperties[3].asText()

        val id = TrackController.patternId.matchEntire(hrefRaw)!!.groupValues[1]
        val title = titleRaw
        val durationFormatted = TrackController.patternDuration.matchEntire(durationRaw)!!.groupValues[1]
        val owner = ownerRaw
        val matchResult = TrackController.patterViewCount.matchEntire(viewCountRaw.replace(",", ""))
        val viewCount = formatViewCount(matchResult?.groupValues?.get(1)?.toLong() ?: 0)

        return YoutubeTrack(
            platformId = id,
            title = title,
            durationMillis = getDurationMillis(durationFormatted),
            durationFormatted = durationFormatted,
            thumbnailUrl = "https://i.ytimg.com/vi/$id/hqdefault.jpg",
            imageUrl = "https://i.ytimg.com/vi/$id/hqdefault.jpg",
            owner = owner,
            viewCount = viewCount
        )
    }

    private fun getDurationMillis(durationFormatted: String): Long {
        val colonCount = durationFormatted.count { c -> c == ':' }

        val scanner = Scanner(durationFormatted.replace(":", " "))

        return when (colonCount) {
            1 -> (scanner.nextInt() * 60 + scanner.nextInt()) * 1000
            2 -> (scanner.nextInt() * 3600 + scanner.nextInt() * 60 + scanner.nextInt()) * 1000
            else -> throw IllegalArgumentException("illegal colon count ($colonCount) in duration text '$durationFormatted'")
        }.toLong()
    }
}