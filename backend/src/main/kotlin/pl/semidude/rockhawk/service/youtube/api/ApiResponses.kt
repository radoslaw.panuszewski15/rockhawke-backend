package pl.semidude.rockhawk.service.youtube.api

import com.fasterxml.jackson.annotation.JsonProperty

data class YoutubeSearchResponse(
    @JsonProperty("nextPageToken") val nextPageToken: String,
    @JsonProperty("pageInfo") val pageInfo: YoutubePageInfo,
    @JsonProperty("items") val items: List<YoutubeSearchItem>
)

data class YoutubePageInfo(
    @JsonProperty("totalPageResults") val totalPageResults: Int,
    @JsonProperty("resultsPerPage") val resultsPerPage: Int
)

data class YoutubeSearchItem(
    @JsonProperty("id") val id: YoutubeSearchResultId,
    @JsonProperty("snippet") val snippet: YoutubeSnippet
)

data class YoutubeSearchResultId(
    @JsonProperty("videoId") val videoId: String
)

data class YoutubeVideoListResponse(
    @JsonProperty("pageInfo") val pageInfo: YoutubePageInfo,
    @JsonProperty("items") val items: List<YoutubeVideoDescriptor>
)

data class YoutubeVideoDescriptor(
    @JsonProperty("id") val id: String,
    @JsonProperty("snippet") val snippet: YoutubeSnippet,
    @JsonProperty("contentDetails") val contentDetails: YoutubeContentDetails,
    @JsonProperty("statistics") val statistics: YoutubeStatistics
)

data class YoutubeSnippet(
    @JsonProperty("title") val title: String,
    @JsonProperty("description") val description: String,
    @JsonProperty("thumbnails") val thumbnails: YoutubeThumbnails,
    @JsonProperty("channelTitle") val channelTitle: String
)

data class YoutubeThumbnails(
    @JsonProperty("default") val default: YoutubeThumbnail?,  //120x90
    @JsonProperty("medium") val medium: YoutubeThumbnail?,   //320x180
    @JsonProperty("high") val high: YoutubeThumbnail?,     //480x360
    @JsonProperty("standard") val standard: YoutubeThumbnail?, //640x480
    @JsonProperty("maxres") val maxres: YoutubeThumbnail?    //1280x720
)

data class YoutubeThumbnail(
    @JsonProperty("url") val url: String,
    @JsonProperty("width") val width: String,
    @JsonProperty("height") val height: String
)

data class YoutubeContentDetails(
    @JsonProperty("duration") val duration: String
)

data class YoutubeStatistics(
    @JsonProperty("viewCount") val viewCount: Long
)

fun formatViewCount(viewCount: Long): String {
    return when {
        viewCount >= 1000000 -> "${viewCount / 1000000} mln"
        viewCount >= 1000 -> "${viewCount / 1000} tys."
        else -> viewCount.toString()
    }
}