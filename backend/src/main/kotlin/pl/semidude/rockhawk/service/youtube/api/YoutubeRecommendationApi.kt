package pl.semidude.rockhawk.service.youtube.api

import pl.semidude.rockhawk.domain.YoutubeTrack

interface YoutubeRecommendationApi {

    fun getRecommendedTrackFor(trackId: String): YoutubeTrack
}