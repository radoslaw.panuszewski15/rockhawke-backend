package pl.semidude.rockhawk.service.spotify.api

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.http.*
import org.springframework.stereotype.Service
import org.springframework.util.LinkedMultiValueMap
import org.springframework.web.client.RestTemplate

interface SpotifyTokenApi {
    fun exchangeToken(code: String): SpotifyRequestTokenResponse
    fun refreshToken(refreshToken: String): SpotifyRefreshTokenResponse
}

@Service
class SpotifyTokenApiImpl(
    @Qualifier("restSpotifyBasicAuth") private val restTemplate: RestTemplate
) : SpotifyTokenApi {

    override fun exchangeToken(code: String): SpotifyRequestTokenResponse {

        val headers = HttpHeaders().apply {
            accept = listOf(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_FORM_URLENCODED
        }

        val urlencodedParameters = LinkedMultiValueMap<String, String>().apply {
            add("grant_type", "authorization_code")
            add("code", code)
            add("redirect_uri", "https://rockhawke.herokuapp.com/callback/spotify")
        }

        val requestEntity = HttpEntity(urlencodedParameters, headers)

        return restTemplate.postForObject(tokenApiUrl, requestEntity, SpotifyRequestTokenResponse::class.java)
    }

    override fun refreshToken(refreshToken: String): SpotifyRefreshTokenResponse {

        val headers = HttpHeaders().apply {
            accept = listOf(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_FORM_URLENCODED
        }

        val urlencodedParameters = LinkedMultiValueMap<String, String>().apply {
            add("grant_type", "refresh_token")
            add("refresh_token", refreshToken)
        }

        val entity = HttpEntity(urlencodedParameters, headers)

        return restTemplate.postForObject(tokenApiUrl, entity, SpotifyRefreshTokenResponse::class.java)
    }

    companion object {
        private const val tokenApiUrl = "https://accounts.spotify.com/api/token"
    }
}