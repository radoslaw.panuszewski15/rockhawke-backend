package pl.semidude.rockhawk.service.youtube.api

import pl.semidude.rockhawk.domain.YoutubeTrack

interface YoutubeSearchApi {

    fun searchTracks(query: String): List<YoutubeTrack>
}