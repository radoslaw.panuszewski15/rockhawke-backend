package pl.semidude.rockhawk.service.spotify.api

import com.fasterxml.jackson.annotation.JsonProperty
import java.time.Instant

data class SpotifyRequestTokenResponse(
    @JsonProperty("access_token") val accessToken: String,
    @JsonProperty("expires_in") val accessTokenSecondsToExpire: Long,
    @JsonProperty("refresh_token") val refreshToken: String,
    @JsonProperty("scope") val scope: String
)

data class SpotifyRefreshTokenResponse(
    @JsonProperty("access_token") val accessToken: String,
    @JsonProperty("expires_in") val accessTokenSecondsToExpire: Long,
    @JsonProperty("scope") val scope: String
)

data class SpotifyCurrentUserProfileResponse(
    @JsonProperty("display_name") val displayName: String,
    @JsonProperty("email") val email: String?,
    @JsonProperty("id") val spotifyId: String,
    @JsonProperty("images") private val images: List<SpotifyImageDescriptor>
) {
    val avatarUrl: String get() = images[0].url
}

data class SpotifyImageDescriptor(
    @JsonProperty("url") val url: String,
    @JsonProperty("width") val width: Int?,
    @JsonProperty("height") val height: Int?
)

data class SpotifySearchResponse(
    @JsonProperty("tracks") val tracks: SpotifyTracksPage
)

data class SpotifyTracksPage(
    @JsonProperty("items") val items: List<SpotifyTrackDescriptor>,
    @JsonProperty("limit") val limit: Int,
    @JsonProperty("total") val total: Int
)

data class SpotifyTrackDescriptor(
    @JsonProperty("id") val spotifyId: String,
    @JsonProperty("name") val name: String,
    @JsonProperty("duration_ms") val durationMillis: Long,
    @JsonProperty("album") val album: SpotifyAlbumDescriptor,
    @JsonProperty("artists") val artists: List<SpotifyArtistDescriptor>
) {
    val thumbnailUrl: String get() = album.thumbnailUrl

    val durationFormatted: String by lazy {
        val seconds = durationMillis / 1000
        val minutes = seconds / 60
        val secondsLeft = seconds % 60
        val secondsLeftFormatted = if (secondsLeft < 10) "0$secondsLeft" else "$secondsLeft"
        "$minutes:$secondsLeftFormatted"
    }
}

data class SpotifyAlbumDescriptor(
    @JsonProperty("id") val spotifyId: String,
    @JsonProperty("name") val name: String,
    @JsonProperty("images") val images: List<SpotifyImageDescriptor>,
    @JsonProperty("artists") val artists: List<SpotifyArtistDescriptor>
) {
    val thumbnailUrl: String get() = images[0].url
}

data class SpotifyArtistDescriptor(
    @JsonProperty("id") val spotifyId: String,
    @JsonProperty("name") val name: String
)

data class SpotifyMyTracksResponse(
    @JsonProperty("items") val items: List<SpotifyItem>,
    @JsonProperty("total") val total: Int,
    @JsonProperty("next") val nextPageLink: String?
)

data class SpotifyItem(
    @JsonProperty("added_at") val addedAt: Instant,
    @JsonProperty("track") val track: SpotifyTrackDescriptor
)

data class SpotifyPlaylistsResponse(
    @JsonProperty("items") val playlists: List<SpotifyPlaylist>
)

data class SpotifyPlaylist(
    @JsonProperty("id") val id: String?,
    @JsonProperty("name") val name: String?,
    @JsonProperty("tracks") val tracks: SpotifyTracksLink?,
    @JsonProperty("images") private val images: List<SpotifyImageDescriptor>?
) {
    val coverImage: String get() = images?.get(0)?.url ?: ""
}

data class SpotifyTracksLink(
    @JsonProperty("href") val href: String,
    @JsonProperty("total") val total: Int
)
