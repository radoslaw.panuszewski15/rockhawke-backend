@file:Suppress("UNCHECKED_CAST")

package pl.semidude.rockhawk.service.authentication

import org.apache.logging.log4j.LogManager
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.GrantedAuthority

abstract class AbstractThirdPartyAuthenticationProvider(
    private val fieldName: String,
    private val authorityName: String
) : AuthenticationProvider {

    private val logger = LogManager.getLogger()

    /**
     * Authenticate user with third party provider via authorization code.
     * This method is called from [authenticate] when this [AuthenticationProvider] object is used from
     * AuthorizationServer, but can be used externally too.
     */
    abstract fun authenticateWithCode(code: String): String?

    /**
     * Extract authorization code from the [Authentication] object and then call [authenticateWithCode]
     * for actual authentication
     */
    override fun authenticate(authentication: Authentication): Authentication? {

        if (authentication is UsernamePasswordAuthenticationToken) {

            val detailsMap = (authentication.details as Map<String, String>).toMutableMap()

            val code = detailsMap[fieldName]
            val userId = code?.let { authenticateWithCode(code) }

            if (userId != null) {
                logger.info("Authenticated user with ID = $userId")

                detailsMap["user_id"] = userId

                return UsernamePasswordAuthenticationToken(
                    authentication.principal,
                    authentication.credentials,
                    listOf(GrantedAuthority { authorityName })
                ).apply { details = detailsMap }
            }
        }
        return null
    }

    override fun supports(authentication: Class<*>): Boolean {
        return UsernamePasswordAuthenticationToken::class.java.isAssignableFrom(authentication)
    }
}