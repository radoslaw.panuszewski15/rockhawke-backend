package pl.semidude.rockhawk.service.spotify.api

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.stereotype.Service
import org.springframework.web.client.HttpClientErrorException
import org.springframework.web.client.RestTemplate
import pl.semidude.rockhawk.domain.SpotifyTrack

interface SpotifyTracksApi {

    fun getTrack(spotifyTrackId: String): SpotifyTrack

    fun getUserTracksCount(accessToken: String, refreshToken: String): Int

    fun getUserTracks(accessToken: String, refreshToken: String): List<SpotifyTrack>

    fun getPlaylistTracks(playlistId: String, accessToken: String, refreshToken: String): List<SpotifyTrack>
}

@Service
class SpotifyTrackApiImpl(
    @Qualifier("restPlain") @Autowired private val rest: RestTemplate,
    @Autowired private val spotifyTokenApi: SpotifyTokenApi
) : SpotifyTracksApi {

    override fun getTrack(spotifyTrackId: String): SpotifyTrack {
        return rest.getForObject("$TRACKS_ENDPOINT/$spotifyTrackId", SpotifyTrack::class.java)
            ?: throw IllegalArgumentException("not found track $spotifyTrackId on Spotify")
    }

    override fun getUserTracksCount(accessToken: String, refreshToken: String): Int {
        return getTracks("$MY_TRACKS_ENDPOINT?offset=0&limit=1", accessToken, refreshToken).total
    }

    override fun getUserTracks(accessToken: String, refreshToken: String): List<SpotifyTrack> {
        return getAllTracks("$MY_TRACKS_ENDPOINT?offset=0&limit=50", accessToken, refreshToken)
    }

    override fun getPlaylistTracks(playlistId: String, accessToken: String, refreshToken: String): List<SpotifyTrack> {
        return getAllTracks(PLAYLIST_TRACKS_ENDPOINT.replace("{playlistId}", playlistId), accessToken, refreshToken)
    }

    private fun getAllTracks(initialRequestUrl: String, accessToken: String, refreshToken: String): List<SpotifyTrack> {
        val spotifyTracks = mutableListOf<SpotifyTrack>()
        var nextRequestUrl: String? = initialRequestUrl
        do {
            val response = getTracks(nextRequestUrl!!, accessToken, refreshToken)
            nextRequestUrl = response.nextPageLink
            response.items.map { SpotifyTrack.fromDescriptor(it.track) }.forEach { spotifyTracks.add(it) }

        } while (nextRequestUrl != null)

        return spotifyTracks
    }

    private fun getTracks(requestUrl: String, accessToken: String, refreshToken: String): SpotifyMyTracksResponse {
        return try {
            rest.exchange(requestUrl, HttpMethod.GET, prepareRequest(accessToken), SpotifyMyTracksResponse::class.java).body
        }
        catch (e: HttpClientErrorException.Unauthorized) {
            val tokens = spotifyTokenApi.refreshToken(refreshToken)
            rest.exchange(requestUrl, HttpMethod.GET, prepareRequest(tokens.accessToken), SpotifyMyTracksResponse::class.java).body
        }
    }

    private fun prepareRequest(accessToken: String): HttpEntity<String> {
        val headers = HttpHeaders().apply {
            accept = listOf(MediaType.APPLICATION_JSON)
            setBearerAuth(accessToken)
        }
        return HttpEntity("", headers)
    }

    companion object {
        const val TRACKS_ENDPOINT = "https://api.spotify.com/v1/tracks"
        const val MY_TRACKS_ENDPOINT = "https://api.spotify.com/v1/me/tracks"
        const val PLAYLIST_TRACKS_ENDPOINT = "https://api.spotify.com/v1/playlists/{playlistId}/tracks"
    }
}