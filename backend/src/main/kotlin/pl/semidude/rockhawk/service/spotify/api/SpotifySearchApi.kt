package pl.semidude.rockhawk.service.spotify.api

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.stereotype.Service
import org.springframework.web.client.HttpClientErrorException
import org.springframework.web.client.RestTemplate
import pl.semidude.rockhawk.domain.SpotifyTrack

interface SpotifySearchApi {
    fun searchTracks(query: String, accessToken: String, refreshToken: String): List<SpotifyTrack>
}

@Service
class SpotifySearchApiImpl(
    @Qualifier("restPlain") private val rest: RestTemplate,
    private val spotifyTokenApi: SpotifyTokenApi
) : SpotifySearchApi {

    override fun searchTracks(query: String, accessToken: String, refreshToken: String): List<SpotifyTrack> {

        val url = "$searchApiUrl?q=$query&type=track"

        val response: SpotifySearchResponse =
            try {
                rest.exchange(url, HttpMethod.GET, prepareRequest(accessToken), SpotifySearchResponse::class.java).body!!
            }
            catch (e: HttpClientErrorException.Unauthorized) {
                val tokens = spotifyTokenApi.refreshToken(refreshToken)
                rest.exchange(url, HttpMethod.GET, prepareRequest(tokens.accessToken), SpotifySearchResponse::class.java).body!!
            }
        return response.tracks.items.map { SpotifyTrack.fromDescriptor(it) }
    }

    private fun prepareRequest(bearerToken: String): HttpEntity<String> {
        val headers = HttpHeaders().apply {
            accept = listOf(MediaType.APPLICATION_JSON)
            set("Authorization", "Bearer $bearerToken")
        }
        return HttpEntity("", headers)
    }

    companion object {
        private const val searchApiUrl = "https://api.spotify.com/v1/search"
    }
}