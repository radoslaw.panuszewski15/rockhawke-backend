package pl.semidude.rockhawk.service.common

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import pl.semidude.rockhawk.rest.model.TrackRest
import pl.semidude.rockhawk.domain.*
import pl.semidude.rockhawk.helpers.makeList
import pl.semidude.rockhawk.helpers.submitCallable
import pl.semidude.rockhawk.service.spotify.api.SpotifySearchApi
import pl.semidude.rockhawk.service.youtube.api.YoutubeSearchApi
import java.util.concurrent.ExecutorService
import java.util.concurrent.Future

interface AsynchronousTrackSearcher {
    fun searchTracks(youtube: Boolean, query: String, spotify: Boolean, user: User?): List<TrackRest>
}

@Service
class AsynchronousTrackSearcherImpl(
        @Autowired @Qualifier("official") private val youtubeSearchApi: YoutubeSearchApi,
        @Autowired private val spotifySearchApi: SpotifySearchApi,
        @Autowired private val executor: ExecutorService,
        @Autowired private val youtubeTrackRepo: YoutubeTrackRepository
) : AsynchronousTrackSearcher {

    override fun searchTracks(youtube: Boolean, query: String, spotify: Boolean, user: User?): List<TrackRest> =
        makeList { tracks ->

            var spotifyFoundTracks: Future<List<TrackRest>>? = null
            var youtubeFoundTracks: Future<List<TrackRest>>? = null

            var youtubeCachedTracks: Future<List<YoutubeTrack>>? = null

            if (youtube) {
                youtubeFoundTracks = executor.submitCallable { youtubeSearchApi.searchTracks(query).mapToRest() }
                youtubeCachedTracks = executor.submitCallable { youtubeTrackRepo.findAll() }
            }
//            if (spotify) {
//                spotifyFoundTracks = executor.submitCallable { spotifySearchApi.searchTracks(query, user!!.lastSpotifyAccessToken!!, user.spotifyRefreshToken!!).mapToRest() }
//            }

            youtubeFoundTracks?.let { tracks += it.get() }
            spotifyFoundTracks?.let { tracks += it.get() }

            if (youtubeFoundTracks != null && youtubeCachedTracks != null) {

                youtubeFoundTracks.get().forEach { trackRest ->
                    val track = youtubeCachedTracks.get().find { it.platformId == trackRest.id }

                    if (track != null) {
                        trackRest.s3Url = track.s3Url
                    }
                }
            }
        }
}