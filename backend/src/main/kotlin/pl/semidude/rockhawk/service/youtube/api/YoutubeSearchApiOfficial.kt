package pl.semidude.rockhawk.service.youtube.api

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Service
import org.springframework.web.client.HttpClientErrorException
import org.springframework.web.client.RestTemplate
import pl.semidude.rockhawk.domain.YoutubeTrack
import javax.xml.datatype.DatatypeFactory

@Service
@Qualifier("official")
class YoutubeSearchApiOfficial(
        @Qualifier("restPlain") private val restTemplate: RestTemplate
) : YoutubeSearchApi {

    override fun searchTracks(query: String): List<YoutubeTrack> {

        val videoIds: List<String> = getVideoIds(query)

        val videosDescriptors: List<YoutubeVideoDescriptor> = getVideoDescriptors(videoIds)

        return videosDescriptors.map {

            val (durationMillis, durationFormatted) = decodeDuration(it.contentDetails.duration)

            val formattedViewCount = formatViewCount(it.statistics.viewCount)

            val thumbnailUrl = it.snippet.thumbnails.let { t ->
                t.medium?.url ?: t.default!!.url
            }
            val imageUrl = it.snippet.thumbnails.let { t ->
                t.maxres?.url ?: t.standard?.url ?: t.high?.url ?: t.medium?.url ?: t.default!!.url
            }

            YoutubeTrack(
                    platformId = it.id,
                    title = it.snippet.title,
                    durationMillis = durationMillis,
                    durationFormatted = durationFormatted,
                    thumbnailUrl = thumbnailUrl,
                    imageUrl = imageUrl,
                    description = it.snippet.description,
                    owner = it.snippet.channelTitle,
                    viewCount = formattedViewCount
            )
        }
    }

    private fun getVideoIds(query: String): List<String> {
        val url = "https://www.googleapis.com/youtube/v3/search" +
                "?part=snippet" +
                "&q=$query" +
                "&key=$YOUTUBE_API_KEY" +
                "&maxResults=$PAGE_SIZE" +
                "&type=video"

        return try {
            val response: YoutubeSearchResponse = restTemplate.getForObject(url, YoutubeSearchResponse::class.java)
                    ?: throw IllegalStateException("youtube search response for url $url is null")

            response.items.map { it.id.videoId }
        }
        catch (e: HttpClientErrorException) {
            emptyList()
        }
    }

    private fun getVideoDescriptors(videoIds: List<String>): List<YoutubeVideoDescriptor> {
        val url = "https://www.googleapis.com/youtube/v3/videos" +
                "?part=" + "snippet,contentDetails,statistics" +
                "&id=" + videoIds.joinToString(",") +
                "&key=$YOUTUBE_API_KEY"

        return try {
            val response: YoutubeVideoListResponse = restTemplate.getForObject(url, YoutubeVideoListResponse::class.java)
                    ?: throw IllegalStateException("youtube video list response for url $url is null")

            response.items

        }
        catch (e: HttpClientErrorException) {
            emptyList()
        }
    }

    private fun decodeDuration(durationEncoded: String): Pair<Long, String> {

        val duration = DatatypeFactory.newInstance().newDuration(durationEncoded)

        val hours = duration.hours.toLong()
        val minutes = duration.minutes.toLong()
        val seconds = duration.seconds.toLong()

        val millis = hours*60*60*1000 + minutes*60*1000 + seconds*1000

        return if (hours == 0L) {
            Pair(millis, "$minutes:$seconds")
        } else {
            Pair(millis, "$hours:$minutes:$seconds")
        }
    }

    private fun formatViewCount(viewCount: Long): String {
        return when {
            viewCount >= 1000000 -> "${viewCount / 1000000} mln"
            viewCount >= 1000 -> "${viewCount / 1000} tys."
            else -> viewCount.toString()
        }
    }

    companion object {
        private const val YOUTUBE_API_KEY = "AIzaSyDD22pyIaVDkjkn1mbio7JxzOZ06PfDglY"
        private const val PAGE_SIZE = 20
    }
}