package pl.semidude.rockhawk.service.youtube.audio

import org.apache.logging.log4j.LogManager
import pl.semidude.rockhawk.domain.Range
import pl.semidude.rockhawk.service.youtube.audio.YoutubeAudioProvider.Companion.END_OF_TRACK
import pl.semidude.rockhawk.service.youtube.util.YoutubeUrlExtractor
import java.io.BufferedInputStream
import java.io.InputStream
import java.util.*
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock

/**
 * Utility class encapsulating access to audio data of one particular YouTube track.
 *
 * The responsibility of this class is to manage dynamically changing set of [PartAudioProvider]s
 * and keeping their bounds not overlapping each other.
 *
 * Also it takes care of when to create new [PartAudioProvider] and when to reuse an existing one.
 *
 * @param youtubeTrackId id of the track to be cached
 * @param partAudioProviderFactory optional factory for [PartAudioProvider] objects, used for mocking in tests
 *
 * @see YoutubeAudioProvider
 * @see PartAudioProvider
 */
class TrackAudioProvider(
    private val youtubeTrackId: String,
    private val partAudioProviderFactory: (String, Range) -> PartAudioProvider =
        { audioUrl, range -> PartAudioProvider(youtubeTrackId, audioUrl, range) }
) {
    var isInitialized: Boolean = false
        private set

    private val logger = LogManager.getLogger(javaClass)
    private val urlExtractionLock = ReentrantLock()

    private var audioUrl: String? = null
    private val partAudioProvidersPerRange: NavigableMap<Range, PartAudioProvider> = TreeMap()

    private var onFullBytesListener: ((ByteArray) -> Unit)? = null

    /**
     * Initialize cache for given track by extracting audio URL and saving it for further processing
     * This method should always be called before any other method on this provider
     * @param urlExtractor [YoutubeUrlExtractor] to be used to extract the URL
     */
    fun initTrackCache(urlExtractor: YoutubeUrlExtractor) {
        urlExtractionLock.withLock {
            while (audioUrl.isNullOrEmpty()) {
                audioUrl = urlExtractor.extractAudioUrl(youtubeTrackId)
            }
            isInitialized = true
        }
    }

    fun initTrackCache(audioUrl: String) {
        urlExtractionLock.withLock {
            this.audioUrl = audioUrl
            isInitialized = true
        }
    }

    /**
     * Open [InputStream] for reading track bytes from start to end, asynchronously.
     * Automatically resumes track caching if paused
     * @param startPos the start position, from which the reading should begin; absolute within the track
     * @return the input stream; if existing [PartAudioProvider] was reused to obtain this stream,
     * then it is wrapped in [ReusedAudioStream] class instance
     */
    fun openAudioStream(startPos: Int): BufferedInputStream {

        val range = Range(startPos, END_OF_TRACK)

        val enclosingProvider = findPartAudioProvider(startPos)

        if (enclosingProvider != null && enclosingProvider.isPositionAlreadyCached(startPos))  {
            logReused(enclosingProvider.range)

            return enclosingProvider
                .openAudioStream(startPos - enclosingProvider.begin)
                .markAsReused()
        }
        else {
            logCreated(range)

            enclosingProvider?.updateEnd(startPos)

            val newProvider = createPartAudioProvider(range)
            val successorProvider = newProvider.findSuccessor()

            if (successorProvider != null) {
                newProvider.updateEnd(successorProvider.begin)
            }

            return newProvider.openAudioStream(0)
        }
    }

    private fun findPartAudioProvider(startPos: Int): PartAudioProvider?
        = partAudioProvidersPerRange.filterKeys { range -> startPos in range }.values.firstOrNull()


    private fun createPartAudioProvider(range: Range): PartAudioProvider {
        val newProvider = partAudioProviderFactory(audioUrl!!, range)

        partAudioProvidersPerRange[range] = newProvider

        newProvider.setOnFullBytesListener {
            if (isTrackFullyCached()) {
                onFullBytesListener?.invoke(getByteRange(Range.FULL))
            }
        }
        return newProvider
    }

    private fun PartAudioProvider.findSuccessor(): PartAudioProvider?
        = partAudioProvidersPerRange.higherEntry(this.range)?.value


    private fun PartAudioProvider.updateEnd(newRangeEnd: Int) {
        val partAudioProvider = this

        partAudioProvidersPerRange.remove(partAudioProvider.range)

        partAudioProvider.end = newRangeEnd

        partAudioProvidersPerRange[partAudioProvider.range] = partAudioProvider
    }

    /**
     * Simple wrapper class to indicate that particular [InputStream] was obtained from pre-existing [PartAudioProvider]
     */
    class ReusedAudioStream(inputStream: InputStream) : BufferedInputStream(inputStream)

    private fun BufferedInputStream.markAsReused()
        = ReusedAudioStream(this)


    /**
     * Get [ByteArray] of specified range synchronously
     * @param range the requested range
     * @return [ByteArray] containing audio bytes from specified range of the track
     */
    fun getByteRange(range: Range): ByteArray {
        checkIfAccessValid()

        val begin = range.begin
        val end = resolveEnd(range)

        checkRange(begin, end)

        val byteParts = getByteParts(Range(begin, end))

        return byteParts.reduce { acc, bytes -> acc + bytes }
    }

    private fun checkIfAccessValid() {
        check(isTrackFullyCached()) { "cannot get byte range of track which has not been fully cached yet ($youtubeTrackId)" }
    }

    private fun resolveEnd(range: Range)
        = if (range.end == END_OF_TRACK) getFullTrackSize()
        else range.end


    private fun checkRange(rangeFrom: Int, rangeTo: Int) {
        if (rangeFrom < 0)
            throw RangeException("rangeFrom ($rangeFrom) < 0")

        if (rangeFrom > rangeTo)
            throw RangeException("rangeFrom ($rangeFrom) > rangeTo ($rangeTo)")
    }

    private fun getByteParts(targetRange: Range): List<ByteArray> {
        return partAudioProvidersPerRange
            .filterKeys { range -> range.collides(targetRange) }
            .mapKeys { (range, _) -> range.intersection(targetRange) }
            .map { (rangeIntersected, provider) -> provider.getAbsoluteByteRange(rangeIntersected) }
    }

    /**
     * Determines if track has been fully cached already
     * @return true if track has been fully cached, false otherwise
     */
    fun isTrackFullyCached(): Boolean
        = partAudioProvidersPerRange.isNotEmpty() && partAudioProvidersPerRange.values.all { it.isPartFullyCached() }

    /**
     * Gets size of fully cached track or 0 if track is not fully cached
     * @return the size or 0 if not fully cached
     */
    fun getFullTrackSize(): Int
        = if (isTrackFullyCached()) partAudioProvidersPerRange.values.sumBy { it.getFullPartSize() }
        else throw IllegalStateException("cannot get full size of track that has not been fully cached yet ($youtubeTrackId)")


    /**
     * Start the process of caching starting from the 0 position
     */
    fun startCaching() {
        openAudioStream(0)
    }

    /**
     * Pause the process of caching
     */
    fun pauseCaching() {
        partAudioProvidersPerRange.values.forEach { it.pauseCaching() }
    }

    /**
     * Resume the process of caching if it has been paused before
     */
    fun resumeCaching() {
        partAudioProvidersPerRange.values.forEach { it.resumeCaching() }
    }

    /**
     * Block execution until full bytes of the track are available
     */
    fun waitForFullBytes() {
        check(!partAudioProvidersPerRange.isEmpty()) { "cannot wait for full bytes if caching has not been started" }

        partAudioProvidersPerRange.values.forEach { it.waitForFullBytes() }
    }

    fun setOnFullBytesListener(listener: (ByteArray) -> Unit) {
        onFullBytesListener = listener
    }

    private fun logReused(range: Range) {
        logger.info("REUSE EXISTING AUDIO PROVIDER $range")
    }

    private fun logCreated(range: Range) {
        logger.info("CREATE NEW PART AUDIO PROVIDER (${range.begin}-)")
    }
}