package pl.semidude.rockhawk.service.youtube.playback

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import pl.semidude.rockhawk.domain.Playback
import pl.semidude.rockhawk.service.youtube.api.YoutubeRecommendationApi
import pl.semidude.rockhawk.service.youtube.util.YoutubePlaybackIdGenerator
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock

interface PlaybackManager {

    fun createPlayback(excludedIds: Set<Long>): Playback

    fun tryScheduleNextTrackFor(playback: Playback, currentTrackPosition: Int)
}

@Service
class PlaybackManagerImpl(
    @Autowired private val youtubePlaybackIdGenerator: YoutubePlaybackIdGenerator,
    @Autowired private val youtubeRecommendationApi: YoutubeRecommendationApi
) : PlaybackManager {

    private val scheduleLock = ReentrantLock()

    override fun createPlayback(excludedIds: Set<Long>): Playback {
        val generatedId = youtubePlaybackIdGenerator.generatePlaybackId(excludedIds)
        return Playback(generatedId)
    }

    override fun tryScheduleNextTrackFor(playback: Playback, currentTrackPosition: Int) {

        scheduleLock.withLock {
            if (playback.getLastPosition() == currentTrackPosition) {
                val track = youtubeRecommendationApi.getRecommendedTrackFor(playback.getLastTrack())
                playback.appendTrack(track.platformId)
            }
        }
    }
}