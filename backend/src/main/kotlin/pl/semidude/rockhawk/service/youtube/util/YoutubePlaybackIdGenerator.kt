package pl.semidude.rockhawk.service.youtube.util

import org.springframework.stereotype.Service
import java.util.*

interface YoutubePlaybackIdGenerator {
    fun generatePlaybackId(excludedIds: Set<Long>): Long
    fun releaseId(id: Long)
    fun getLastId(): Long
}

@Service
class YoutubePlaybackIdGeneratorImpl : YoutubePlaybackIdGenerator {

    private val releasedIds: Queue<Long> = LinkedList()
    private var nextId: Long = 0
    private var lastId: Long = -1

    override fun generatePlaybackId(excludedIds: Set<Long>): Long {
        releasedIds.poll()?.let { if (!excludedIds.contains(it)) return it }
        lastId = nextId++
        return lastId
    }

    override fun releaseId(id: Long) {
        releasedIds.add(id)
    }

    override fun getLastId(): Long = lastId
}