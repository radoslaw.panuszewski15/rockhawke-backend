package pl.semidude.rockhawk.service.rest

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import pl.semidude.rockhawk.rest.model.TrackRest
import pl.semidude.rockhawk.domain.TrackRepository
import pl.semidude.rockhawk.domain.User
import pl.semidude.rockhawk.service.youtube.audio.YoutubeAudioProvider
import kotlin.concurrent.thread

interface ResponseEnricher {

    fun enrichResponseForSearchResults(tracks: List<TrackRest>, user: User?): List<TrackRest>
}

@Service
class ResponseEnricherImpl(
        @Autowired private val youtubeAudioProvider: YoutubeAudioProvider
) : ResponseEnricher {

    override fun enrichResponseForSearchResults(tracks: List<TrackRest>, user: User?): List<TrackRest> {

        markInitializedTracks(tracks)

        if (user != null) {
            markTracksInLibrary(user, tracks)
        }

        return tracks
    }

    private fun markInitializedTracks(searchResults: List<TrackRest>) {
        searchResults
            .filter { it.type == "youtube" && (it.s3Url != null || youtubeAudioProvider.isTrackInitialized(it.id)) }
            .forEach { trackDto ->
                trackDto.initialized = true
            }
    }

    private fun markTracksInLibrary(user: User, searchResults: List<TrackRest>) {
        val libraryIds = user.library.map { it.platformId }

        searchResults
            .filter { it.id in libraryIds }
            .forEach { trackDto ->
                trackDto.inLibrary = true
            }
    }
}