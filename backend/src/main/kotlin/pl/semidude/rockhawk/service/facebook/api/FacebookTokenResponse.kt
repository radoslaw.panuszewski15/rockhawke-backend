package pl.semidude.rockhawk.service.facebook.api

import org.codehaus.jackson.annotate.JsonProperty

data class FacebookTokenResponse(
    @JsonProperty("access_token") val accessToken: String,
    @JsonProperty("token_type") val tokenType: String,
    @JsonProperty("expires_in") val expiresIn: Long
)