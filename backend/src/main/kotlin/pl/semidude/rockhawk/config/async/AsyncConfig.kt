package pl.semidude.rockhawk.config.async

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.scheduling.annotation.AsyncConfigurer
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import org.springframework.scheduling.concurrent.ConcurrentTaskExecutor
import org.springframework.web.servlet.config.annotation.AsyncSupportConfigurer
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer



@Configuration
open class AsyncConfig : AsyncConfigurer {

    @Bean
    open fun threadPoolExecutorService(): ExecutorService =
        Executors.newCachedThreadPool()
//
//    override fun getAsyncExecutor(): Executor =
//        ThreadPoolTaskExecutor().apply {
//            corePoolSize = 4
//            maxPoolSize = 4
//            threadNamePrefix = "SuperTaskExecutor-"
//            initialize()
//        }

    @Bean
    open fun webMvcConfigurer(): WebMvcConfigurer {
        return object : WebMvcConfigurerAdapter() {
            override fun configureAsyncSupport(configurer: AsyncSupportConfigurer) {
                configurer.setTaskExecutor(taskExecutor())
            }
        }
    }

    @Bean
    open fun taskExecutor(): ConcurrentTaskExecutor {
        return ConcurrentTaskExecutor(Executors.newCachedThreadPool())
    }
}