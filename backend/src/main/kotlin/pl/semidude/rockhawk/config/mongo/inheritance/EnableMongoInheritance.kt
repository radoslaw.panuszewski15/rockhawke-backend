package pl.semidude.rockhawk.config.mongo.inheritance

/**
 * To use Mongo inheritance support in your classes, you have to annotate your document class with 2 additional
 * annotations: @TypeAlias and @EnableMongoInheritance.
 *
 * Example usage:
 * ```
 * @Document(collection = "people")
 * interface Person {
 *     var id: String
 *     var name: String
 * }
 *
 * @Document(collection = "people")
 * @TypeAlias("Employee")
 * @EnableMongoInheritance
 * data class Employee(
 *     override var id: String
 *     override var name: String,
 *     var salary: Int
 * ) : Person
 *
 * @Document(collection = "people")
 * @TypeAlias("Programmer")
 * @EnableMongoInheritance
 * data class Programmer(
 *     override var id: String
 *     override var name: String,
 *     var reasonForLife: String? = null
 * ) : Person
 *
 * interface PersonRepository : MongoRepository<Person, String>
 * interface EmployeeRepository : MongoRepository<Employee, String>
 * interface ProgrammerRepository : MongoRepository<Programmer, String>
 * ```
 *
 * In the above example, here's how it will work:
 * - all of those classes will be persisted to the same collection named 'people'
 * - PersonRepository.findAll() will return whole collection, i.e. employees and programmers
 * - EmployeeRepository.findAll() will return only those documents, which where persisted from instances of Employee class
 * - ProgrammerRepository.findAll() will return only those documents, which where persisted from instances of Programmer class
 *
 * All of the above applies to:
 * - findAll()
 * - count()
 * - all queries generated from method names, for example: findBySalary()
 *
 * Why is that @TypeAlias required?
 * - Because under the hood, the auto-created _class field is used to determine the original class and, by default,
 *   Spring Data Mongo will fill that field with a full-qualified class name, which is not good when you want to later
 *   refactor your class and especially move it to a different package. That's why you have to provide a type alias of
 *   your choice (at least it should be unique in scope of your application).
 *
 * Based on:
 * https://medium.com/@mladen.maravic/spring-data-mongodb-my-take-on-inheritance-support-102361c08e3d
 * https://github.com/beb4ch/spring-data-mongodb-inheritance-test
 */
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
annotation class EnableMongoInheritance