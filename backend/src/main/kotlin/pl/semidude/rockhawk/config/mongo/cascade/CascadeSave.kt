package pl.semidude.rockhawk.config.mongo.cascade

/**
 * Adding this annotation to a @DBRef field in your document class will result in that @DBRef be persisted
 * in cascaded way before the parent document is persisted
 *
 * Example usage:
 * ```
 * @Document(collection = "people")
 * data class Person(
 *     var id: String,
 *     var name: String,
 *     @DBRef @CascadeSave var address: Address,
 *     @DBRef @CascadeSave var bills: List<Bill>
 * )
 *
 * @Document(collection = "addresses")
 * data class Address(
 *     var id: String,
 *     var city: String,
 *     var street: String,
 *     var houseNumber: Int
 * )
 *
 * @Document(collection = "bills")
 * data class Bill(
 *     var id: String,
 *     var price: BigDecimal,
 *     @DBRef @CascadeSave var creditorAddress: Address
 * )
 * ```
 *
 * As you can see from above example:
 * - it works with lists (and all Iterables in general)
 * - it works with nested @DBRefs
 */
@Target(AnnotationTarget.FIELD)
@Retention(AnnotationRetention.RUNTIME)
annotation class CascadeSave