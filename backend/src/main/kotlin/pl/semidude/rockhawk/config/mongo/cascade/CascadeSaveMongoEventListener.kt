package pl.semidude.rockhawk.config.mongo.cascade

import org.springframework.data.mongodb.core.MongoOperations
import org.springframework.data.mongodb.core.mapping.DBRef
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener
import org.springframework.data.mongodb.core.mapping.event.BeforeSaveEvent
import org.springframework.stereotype.Component
import org.springframework.util.ReflectionUtils
import pl.semidude.rockhawk.helpers.areAnnotationsPresent
import java.lang.reflect.Field

/**
 * Register custom MongoEventListener, and before persisting given entity, persist all of its @DBRef's
 * (if they are annotated with @CascadeSave)
 */
@Component
class CascadeSaveMongoEventListener(
    private val mongoOperations: MongoOperations
) : AbstractMongoEventListener<Any>() {

    override fun onBeforeSave(event: BeforeSaveEvent<Any>) {
        ReflectionUtils.doWithFields(event.source.javaClass, CascadeCallback(event.source, mongoOperations))
    }
}

/**
 * Walk through fields of given document class, and persist its @DBRef's
 */
class CascadeCallback(
    private val source: Any?,
    private val mongoOperations: MongoOperations
) : ReflectionUtils.FieldCallback {

    override fun doWith(field: Field) {
        ReflectionUtils.makeAccessible(field)

        if (field.areAnnotationsPresent(DBRef::class, CascadeSave::class)) {

            field.get(source)?.let { value: Any ->
                if (value is Iterable<*>) {
                    for (elem in value) {
                        mongoOperations.save(elem)
                    }
                }
                else {
                    mongoOperations.save(value)
                }
            }
        }
    }
}