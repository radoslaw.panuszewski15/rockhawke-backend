package pl.semidude.rockhawk.config.rest

import org.springframework.context.annotation.Configuration
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.context.annotation.Bean
import org.springframework.core.env.Environment
import org.springframework.web.client.RestTemplate


@Configuration
open class RestConfig(
    private val env: Environment
) {
    @Bean
    open fun restSpotifyBasicAuth(builder: RestTemplateBuilder): RestTemplate =
        builder.basicAuthentication(
            env.getProperty("spotify.clientId"),
            env.getProperty("spotify.clientSecret")
        ).build()

    @Bean
    open fun restPlain(): RestTemplate = RestTemplate()
}