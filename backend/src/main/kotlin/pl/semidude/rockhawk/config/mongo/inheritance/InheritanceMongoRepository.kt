package pl.semidude.rockhawk.config.mongo.inheritance

import org.springframework.data.annotation.TypeAlias
import org.springframework.data.mongodb.core.MongoOperations
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Criteria.where
import org.springframework.data.mongodb.core.query.Query
import org.springframework.data.mongodb.repository.query.MongoEntityInformation
import org.springframework.data.mongodb.repository.support.SimpleMongoRepository
import pl.semidude.rockhawk.helpers.areAnnotationsPresent
import java.io.Serializable

/**
 * Base class for all mongo repository interfaces.
 * Adds inheritance support for [count] and [findAll] standard queries
 */
class InheritanceMongoRepository<T, ID : Serializable>(
    private val entityMetadata: MongoEntityInformation<T, ID>,
    private val mongoOperations: MongoOperations
) : SimpleMongoRepository<T, ID>(entityMetadata, mongoOperations) {

    /**
     * If the entity class is annotated with @EnableMongoInheritance and @TypeAlias then we add criteria to match the _class field
     */
    private val inheritanceCriteria: Criteria? =
        if (isEntityProperlyAnnotated()) where("_class").`is`(getTypeAliasName())
        else null

    private fun isEntityProperlyAnnotated() =
        entityMetadata.javaType.areAnnotationsPresent(EnableMongoInheritance::class, TypeAlias::class)

    private fun getTypeAliasName() =
        entityMetadata.javaType.getAnnotation(TypeAlias::class.java).value

    /**
     * Apply additional criteria if needed
     */
    override fun count(): Long =
        if (inheritanceCriteria != null)
            mongoOperations.getCollection(entityMetadata.collectionName).countDocuments(inheritanceCriteria.criteriaObject)
        else super.count()

    /**
     * Apply additional criteria if needed
     */
    override fun findAll(): MutableList<T> =
        if (inheritanceCriteria != null)
            mongoOperations.find(Query().addCriteria(inheritanceCriteria), entityMetadata.javaType, entityMetadata.collectionName)
        else super.findAll()
}