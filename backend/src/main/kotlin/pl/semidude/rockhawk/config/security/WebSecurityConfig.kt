package pl.semidude.rockhawk.config.security

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.crypto.factory.PasswordEncoderFactories
import org.springframework.security.crypto.password.PasswordEncoder
import pl.semidude.rockhawk.service.mongo.MongoUserDetailsService


@Configuration
open class WebSecurityConfig(
    @Autowired @Qualifier("spotify") private val spotifyAuthenticationProvider: AuthenticationProvider,
    @Autowired @Qualifier("google") private val googleAuthenticationProvider: AuthenticationProvider,
    @Autowired @Qualifier("facebook") private val facebookAuthenticationProvider: AuthenticationProvider,
    @Autowired private val mongoUserDetailsService: MongoUserDetailsService
) : WebSecurityConfigurerAdapter() {

    override fun configure(builder: AuthenticationManagerBuilder) {
        builder
            .authenticationProvider(spotifyAuthenticationProvider)
            .authenticationProvider(googleAuthenticationProvider)
            .authenticationProvider(facebookAuthenticationProvider)
            .userDetailsService(mongoUserDetailsService)
            .passwordEncoder(passwordEncoder())
    }

    @Bean
    override fun authenticationManagerBean(): AuthenticationManager = super.authenticationManagerBean()

    @Bean
    open fun passwordEncoder(): PasswordEncoder = PasswordEncoderFactories.createDelegatingPasswordEncoder()
}