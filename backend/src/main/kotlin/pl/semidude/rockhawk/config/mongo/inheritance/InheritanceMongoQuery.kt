package pl.semidude.rockhawk.config.mongo.inheritance

import org.springframework.data.annotation.TypeAlias
import org.springframework.data.mongodb.core.MongoOperations
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Criteria.where
import org.springframework.data.mongodb.core.query.Query
import org.springframework.data.mongodb.repository.query.ConvertingParameterAccessor
import org.springframework.data.mongodb.repository.query.MongoQueryMethod
import org.springframework.data.mongodb.repository.query.PartTreeMongoQuery
import org.springframework.data.repository.core.EntityMetadata
import org.springframework.data.repository.query.QueryMethodEvaluationContextProvider
import org.springframework.expression.ExpressionParser
import pl.semidude.rockhawk.helpers.areAnnotationsPresent

/**
 * Used ase replacement for [PartTreeMongoQuery]
 * Adds inheritance support for method name generated  queries
 */
class InheritanceMongoQuery(
    method: MongoQueryMethod,
    mongoOperations: MongoOperations,
    expressionParser: ExpressionParser,
    expressionContextProvider: QueryMethodEvaluationContextProvider
) : PartTreeMongoQuery(method, mongoOperations, expressionParser, expressionContextProvider) {

    private val entityMetadata: EntityMetadata<*> = method.entityInformation

    /**
     * If the entity class is annotated with @EnableMongoInheritance and @TypeAlias then we add criteria to match the _class field
     */
    private val inheritanceCriteria: Criteria? =
        if (isEntityProperlyAnnotated()) where("_class").`is`(getTypeAliasName())
        else null

    private fun isEntityProperlyAnnotated() =
        entityMetadata.javaType.areAnnotationsPresent(EnableMongoInheritance::class, TypeAlias::class)

    private fun getTypeAliasName() =
        entityMetadata.javaType.getAnnotation(TypeAlias::class.java).value

    /**
     * Apply additional criteria if needed
     */
    override fun createQuery(accessor: ConvertingParameterAccessor): Query =
        super.createQuery(accessor)
            .apply {
                if (inheritanceCriteria != null)
                    addCriteria(inheritanceCriteria)
            }

    /**
     * Apply additional criteria if needed
     */
    override fun createCountQuery(accessor: ConvertingParameterAccessor): Query =
        super.createCountQuery(accessor)
            .apply {
                if (inheritanceCriteria != null)
                    addCriteria(inheritanceCriteria)
            }
}