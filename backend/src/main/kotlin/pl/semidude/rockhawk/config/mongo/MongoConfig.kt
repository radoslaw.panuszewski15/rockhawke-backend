package pl.semidude.rockhawk.config.mongo

import ch.qos.logback.classic.Level
import ch.qos.logback.classic.LoggerContext
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.data.mongodb.core.mapping.MongoMappingContext
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories
import pl.semidude.rockhawk.config.mongo.delegation.HiddenDelegationFieldsAwareMongoMappingContext
import pl.semidude.rockhawk.config.mongo.inheritance.InheritanceMongoRepository
import pl.semidude.rockhawk.config.mongo.inheritance.InheritanceMongoRepositoryFactoryBean
import pl.semidude.rockhawk.domain.SpotifyTrack
import javax.annotation.PostConstruct


@Configuration
@EnableMongoRepositories(
    basePackageClasses = [SpotifyTrack::class],
    repositoryBaseClass = InheritanceMongoRepository::class,
    repositoryFactoryBeanClass = InheritanceMongoRepositoryFactoryBean::class
)
open class MongoConfig {

    @Bean
    @Primary
    open fun mappingContext(): MongoMappingContext = HiddenDelegationFieldsAwareMongoMappingContext()

    @PostConstruct
    open fun setMongoLoggingLevel() {
        val loggerContext = LoggerFactory.getILoggerFactory() as LoggerContext
        val rootLogger = loggerContext.getLogger("org.mongodb.driver")
        rootLogger.level = Level.WARN
    }
}