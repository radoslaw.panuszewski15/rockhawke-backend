package pl.semidude.rockhawk.config.security

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken
import org.springframework.security.oauth2.common.OAuth2AccessToken
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer
import org.springframework.security.oauth2.provider.OAuth2Authentication
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter
import org.springframework.security.oauth2.provider.token.TokenEnhancer
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore
import pl.semidude.rockhawk.domain.UserRepository
import org.springframework.security.web.firewall.DefaultHttpFirewall
import org.springframework.security.web.firewall.HttpFirewall



@Configuration
@EnableAuthorizationServer
open class AuthorizationServerConfig(
    @Autowired private val authenticationManager: AuthenticationManager,
    @Autowired private val passwordEncoder: PasswordEncoder,
    @Autowired private val userRepository: UserRepository
) : AuthorizationServerConfigurerAdapter() {

    companion object {
        private const val SIGNING_KEY = "903854375710958751"
    }

    override fun configure(clients: ClientDetailsServiceConfigurer) {
        clients
            .inMemory()
            .withClient("android-client")
            .secret(passwordEncoder.encode("superpassword"))
            .scopes("read", "write")
            .authorizedGrantTypes("password", "refresh_token")
            .accessTokenValiditySeconds(-1)
            .refreshTokenValiditySeconds(-1)
    }

    override fun configure(endpoints: AuthorizationServerEndpointsConfigurer) {

        val tokenEnhancerChain = TokenEnhancerChain()
            .apply { setTokenEnhancers(listOf(includeUserIdEnhancer(), jwtTokenConverter())) }

        endpoints
            .tokenEnhancer(tokenEnhancerChain)
            .tokenStore(jwtTokenStore()) //not necessary since it would be deducted from JwtAccessTokenConverter anyway
            .authenticationManager(authenticationManager)
    }

    @Bean
    open fun includeUserIdEnhancer()
        = IncludeUserIdTokenEnhancer()

    @Bean
    open fun jwtTokenConverter(): JwtAccessTokenConverter {
        return JwtAccessTokenConverter()
            .apply { setSigningKey(SIGNING_KEY) }
            .apply { accessTokenConverter = PreserveAdditionalInformationTokenConverter() }
    }

    @Bean
    open fun jwtTokenStore()
        = JwtTokenStore(jwtTokenConverter())

    @Bean
    open fun defaultHttpFirewall(): HttpFirewall
        = DefaultHttpFirewall()

    /**
     * Reads 'user_id' parameter from Authentication of the request to /oauth/token endpoint, and includes it
     * in the just-generated AccessToken.
     *
     * This operation takes place in AuthorizationServer, before the generated token is returned to client.
     */
    inner class IncludeUserIdTokenEnhancer : TokenEnhancer {

        override fun enhance(accessToken: OAuth2AccessToken, authentication: OAuth2Authentication): OAuth2AccessToken {

            val detailsMap = authentication.userAuthentication.details as Map<String, String>
            val userId = detailsMap["user_id"]
                ?: detailsMap["username"]?.let { userRepository.findByEmail(it) }?.id

            return DefaultOAuth2AccessToken(accessToken)
                .apply { additionalInformation = mapOf("user_id" to userId) }
        }
    }

    /**
     * Preserves all parameters which were extracted from AccessToken (including additionalInformation)
     * into a newly created Authentication object.
     *
     * This operation takes place in ResourceServer, when the user-provided token (which was earlier obtained
     * from AuthorizationServer and enhanced by [IncludeUserIdTokenEnhancer]) is being processed before saving it
     * to the [SecurityContext]. As a result, the 'spotify_code' parameter is present in the Authentication object.
     */
    class PreserveAdditionalInformationTokenConverter : DefaultAccessTokenConverter() {

        override fun extractAuthentication(map: MutableMap<String,*>): OAuth2Authentication {

            return super.extractAuthentication(map).apply { details = map }
        }
    }
}