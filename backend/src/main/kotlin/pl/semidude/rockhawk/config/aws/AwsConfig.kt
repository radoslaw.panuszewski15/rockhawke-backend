package pl.semidude.rockhawk.config.aws

import com.amazonaws.auth.DefaultAWSCredentialsProviderChain
import com.amazonaws.regions.Regions
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
open class AwsConfig {

    @Bean
    open fun s3Client(): AmazonS3 =
        AmazonS3ClientBuilder.standard()
            .withRegion(Regions.EU_CENTRAL_1)
            .withCredentials(DefaultAWSCredentialsProviderChain.getInstance())
            .build()
}
