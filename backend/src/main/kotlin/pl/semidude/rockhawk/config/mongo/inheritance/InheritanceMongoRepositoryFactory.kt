@file:Suppress("KDocUnresolvedReference")

package pl.semidude.rockhawk.config.mongo.inheritance

import org.springframework.data.mapping.context.MappingContext
import org.springframework.data.mongodb.core.MongoOperations
import org.springframework.data.mongodb.core.mapping.MongoPersistentEntity
import org.springframework.data.mongodb.core.mapping.MongoPersistentProperty
import org.springframework.data.mongodb.repository.query.MongoQueryMethod
import org.springframework.data.mongodb.repository.query.StringBasedMongoQuery
import org.springframework.data.mongodb.repository.support.MongoRepositoryFactory
import org.springframework.data.mongodb.repository.support.MongoRepositoryFactoryBean
import org.springframework.data.projection.ProjectionFactory
import org.springframework.data.repository.Repository
import org.springframework.data.repository.core.NamedQueries
import org.springframework.data.repository.core.RepositoryMetadata
import org.springframework.data.repository.core.support.RepositoryFactorySupport
import org.springframework.data.repository.query.QueryLookupStrategy
import org.springframework.data.repository.query.QueryMethodEvaluationContextProvider
import org.springframework.data.repository.query.RepositoryQuery
import org.springframework.expression.spel.standard.SpelExpressionParser
import pl.semidude.rockhawk.config.mongo.inheritance.InheritanceMongoRepositoryFactory.InheritanceQueryLookupStrategy
import java.io.Serializable
import java.lang.reflect.Method
import java.util.*

/**
 * [FactoryBean] which provides our custom repository factory implementation
 */
class InheritanceMongoRepositoryFactoryBean<T : Repository<S, ID>, S, ID : Serializable>(
    repositoryInterface: Class<out T>
) : MongoRepositoryFactoryBean<T, S, ID>(repositoryInterface) {

    override fun getFactoryInstance(operations: MongoOperations): RepositoryFactorySupport =
        InheritanceMongoRepositoryFactory(operations)
}

/**
 * The purpose of this class is to swap the [QueryLookupStrategy] implementation from
 * [MongoQueryLookupStrategy] to [InheritanceQueryLookupStrategy] so it will return
 * [InheritanceMongoQuery] instead of [PartTreeMongoQuery]
 */
class InheritanceMongoRepositoryFactory(
    private val mongoOperations: MongoOperations
) : MongoRepositoryFactory(mongoOperations) {

    companion object {
        val EXPRESSION_PARSER = SpelExpressionParser()
    }

    /**
     * Return our custom implementation of [QueryLookupStrategy]
     * This method will be invoked by [QueryExecutorMethodInterceptor]
     */
    override fun getQueryLookupStrategy(
        key: QueryLookupStrategy.Key?,
        evaluationContextProvider: QueryMethodEvaluationContextProvider
    ): Optional<QueryLookupStrategy> {

        return Optional.of(
            InheritanceQueryLookupStrategy(
                mongoOperations,
                evaluationContextProvider,
                mongoOperations.converter.mappingContext
            )
        )
    }

    /**
     * This class is going to be used by the [QueryExecutorMethodInterceptor] in order to resolve database query
     * for particular method from repository interface.
     *
     * It works as usual for those methods which are either annotated by @Query or have corresponding @NamedQuery on the entity class.
     * If none of above cases apply (which means the query is generated from method's name), the [InheritanceMongoQuery] instance is returned
     *
     * Effectively, this class adds our custom mongo criteria (the _class field matching) to all queries which
     * are generated from method names like findByNameAndSurname etc.
     */
    private class InheritanceQueryLookupStrategy(
        private val operations: MongoOperations,
        private val evaluationContextProvider: QueryMethodEvaluationContextProvider,
        var mappingContext: MappingContext<out MongoPersistentEntity<*>, MongoPersistentProperty>
    ) : QueryLookupStrategy {

        override fun resolveQuery(
            method: Method,
            metadata: RepositoryMetadata,
            factory: ProjectionFactory,
            namedQueries: NamedQueries
        ): RepositoryQuery {

            val queryMethod = MongoQueryMethod(method, metadata, factory, mappingContext)
            val namedQueryName = queryMethod.namedQueryName

            return when {
                //this is the case when there is @NamedQuery annotation on the entity class
                namedQueries.hasQuery(namedQueryName) -> {
                    val namedQuery = namedQueries.getQuery(namedQueryName)
                    StringBasedMongoQuery(namedQuery, queryMethod, operations, EXPRESSION_PARSER,
                        evaluationContextProvider)
                }
                //this is the case when there is @Query annotation in the repository interface
                queryMethod.hasAnnotatedQuery() ->
                    StringBasedMongoQuery(queryMethod, operations, EXPRESSION_PARSER, evaluationContextProvider)

                //this is the case when query is generated from method name
                //we want it to automatically apply our inheritance support
                else -> InheritanceMongoQuery(queryMethod, operations, SpelExpressionParser(), evaluationContextProvider)
            }
        }
    }
}
