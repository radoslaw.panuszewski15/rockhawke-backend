@file:Suppress("KDocUnresolvedReference")

package pl.semidude.rockhawk.config.mongo.delegation

import org.springframework.data.mongodb.core.mapping.BasicMongoPersistentEntity
import org.springframework.data.mongodb.core.mapping.MongoMappingContext
import org.springframework.data.mongodb.core.mapping.MongoPersistentProperty
import org.springframework.data.util.TypeInformation

/**
 * Custom MappingContext for MongoDB which takes care of the hidden Kotlin's delegation fields, i.e.
 * those starting with '$$delegate_' prefix
 *
 * @author Radosław Panuszewski
 */
class HiddenDelegationFieldsAwareMongoMappingContext : MongoMappingContext() {

    override fun <T : Any?> createPersistentEntity(typeInformation: TypeInformation<T>): BasicMongoPersistentEntity<T> =
        HiddenDelegationFieldsAwareMongoPersistentEntity(typeInformation)
}

/**
 * Custom PersistentEntity fo MongoDB which will not contain PersistentProperty if it starts with $$delegate_ prefix
 *
 * It is a fix for a problem with persisting Kotlin classes, which do use the Class Delegation feature, because
 * in that case, an additional hidden fields $$delegate_0, $$delegate_1... are generated. Those fields in turn
 * are not well welcomed by [AbstractBsonWriter] and so it throws an exception with message like:
 * "Invalid BSON field name $$delegate_0"
 *
 * Since those properties won't be held by this PersistentEntity, no exceptions will be thrown when persisting
 * classes which use Kotlin Class Delegation feature.
 *
 * @see AbstractBsonWriter.writeName
 * @author Radosław Panuszewski
 */
class HiddenDelegationFieldsAwareMongoPersistentEntity<T>(
    typeInformation: TypeInformation<T>
) : BasicMongoPersistentEntity<T>(typeInformation) {

    override fun addPersistentProperty(property: MongoPersistentProperty) {
        if (property.name.startsWith("\$\$delegate_")) {
            return
        }
        super.addPersistentProperty(property)
    }
}