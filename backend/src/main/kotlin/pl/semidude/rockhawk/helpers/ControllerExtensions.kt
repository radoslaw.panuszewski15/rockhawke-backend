package pl.semidude.rockhawk.helpers

import org.springframework.http.HttpStatus
import org.springframework.web.server.ResponseStatusException

interface Controller {
    fun notFound(message: String?): Nothing =
        throw ResponseStatusException(HttpStatus.NOT_FOUND, message)

    fun badRequest(message: String?): Nothing =
        throw ResponseStatusException(HttpStatus.BAD_REQUEST, message)

    fun rangeNotSatisfiable(message: String?): Nothing =
        throw ResponseStatusException(HttpStatus.REQUESTED_RANGE_NOT_SATISFIABLE, message)

    fun notAcceptable(message: String?): Nothing =
        throw ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, message)

    fun internalServerError(message: String?): Nothing =
        throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, message)
}