package pl.semidude.rockhawk.helpers

import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails
import java.lang.IllegalStateException

fun getAuthenticatedUserId(): String {

    val authDetails = SecurityContextHolder.getContext().authentication.details as OAuth2AuthenticationDetails

    return (authDetails.decodedDetails as Map<String, String>)["user_id"]
        ?: throw IllegalStateException("no user authenticated!")
}