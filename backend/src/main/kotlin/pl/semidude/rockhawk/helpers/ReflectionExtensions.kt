package pl.semidude.rockhawk.helpers

import java.lang.reflect.Field
import kotlin.reflect.KClass

fun Class<*>.isAnnotationPresent(annotationType: KClass<out Annotation>): Boolean =
    isAnnotationPresent(annotationType.java)

fun Field.isAnnotationPresent(annotationType: KClass<out Annotation>): Boolean =
    isAnnotationPresent(annotationType.java)

fun Field.areAnnotationsPresent(vararg annotationTypes: KClass<out Annotation>): Boolean {
    for (type in annotationTypes) {
        if (!isAnnotationPresent(type))
            return false
    }
    return true
}

fun Class<*>.areAnnotationsPresent(vararg annotationTypes: KClass<out Annotation>): Boolean {
    for (type in annotationTypes) {
        if (!isAnnotationPresent(type))
            return false
    }
    return true
}