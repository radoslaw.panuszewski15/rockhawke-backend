package pl.semidude.rockhawk.helpers

fun <T> emptyMutableList(): MutableList<T> = emptyList<T>().toMutableList()

fun Collection<String>.containsIgnoreCase(elem: String) =
    this.any { it.toLowerCase() == elem.toLowerCase() }

fun Collection<String>.containsAllIgnoreCase(list: List<String>) =
    list.all { this.containsIgnoreCase(it) }

fun <T> buildList(creator: MutableList<T>.() -> Unit): List<T> {
    return mutableListOf<T>().apply { creator() }
}

fun <T> makeList(creator: (MutableList<T>) -> Unit): List<T> {
    return mutableListOf<T>().also { creator(it) }
}

fun ByteArray.printDiff(other: ByteArray, breakOfFirst: Boolean = true) {
    for (i in this.indices){
        if (this[i] != other[i]) {
            println("$i) ${this[i]} != ${other[i]}")
            if (breakOfFirst) break
        }
    }
}