package pl.semidude.rockhawk.helpers

import java.util.concurrent.Callable
import java.util.concurrent.ExecutorService

fun <T> ExecutorService.submitCallable(block: () -> T)
    = submit(Callable(block))