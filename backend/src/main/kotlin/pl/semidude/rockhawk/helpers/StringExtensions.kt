package pl.semidude.rockhawk.helpers

fun String.containsIgnoreCase(substring: String) =
    this.contains(substring, ignoreCase = true)