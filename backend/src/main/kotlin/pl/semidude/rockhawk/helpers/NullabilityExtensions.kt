package pl.semidude.rockhawk.helpers

inline fun <T: Any> T?.alsoNullable(onNotNull: (T) -> Unit = {}, onNull: () -> Unit = {}): T? {
    if (this != null) onNotNull(this)
    else onNull()
    return this
}

inline fun <T: Any> T?.applyNullable(onNotNull: T.() -> Unit = {}, onNull: () -> Unit = {}): T? {
    if (this != null) this.onNotNull()
    else onNull()
    return this
}

inline fun <T: Any, R: Any> T?.letNullable(onNotNull: (T) -> R = { it as R }, onNull: () -> R): R {
    return if (this != null) onNotNull(this)
    else onNull()
}

inline fun <T: Any> T?.doOnNull(onNull: () -> Unit): T? {
    if (this == null)
        onNull()
    return this
}

class DefaultMap<K, V>(
    private val map: Map<K, V>,
    private val defaultProvider: () -> V
) : Map<K, V> by map {

    override fun get(key: K): V? {
        return map.getOrDefault(key, defaultProvider.invoke())
    }
}

class DefaultMutableMap<K, V>(
    private val map: MutableMap<K, V>,
    private val defaultProvider: () -> V
): MutableMap<K, V> by map {

    override operator fun get(key: K): V {
        return map.getOrDefault(key, defaultProvider.invoke())
    }
}

fun <K, V> Map<K, V>.default(defaultProvider: () -> V): DefaultMap<K, V> {
    return DefaultMap(this, defaultProvider)
}

fun <K, V> defaultMapOf(vararg pairs: Pair<K, V> = emptyArray(), defaultProvider: () -> V): DefaultMap<K, V> {
    return mapOf(*pairs).default(defaultProvider)
}

fun <K, V> MutableMap<K, V>.default(defaultProvider: () -> V): DefaultMutableMap<K, V> {
    return DefaultMutableMap(this, defaultProvider)
}

fun <K, V> defaultMutableMapOf(vararg pairs: Pair<K, V> = emptyArray(), defaultProvider: () -> V): DefaultMutableMap<K, V> {
    return mutableMapOf(*pairs).default(defaultProvider)
}