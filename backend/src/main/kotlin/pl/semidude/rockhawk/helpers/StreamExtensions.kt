package pl.semidude.rockhawk.helpers

import java.io.BufferedInputStream

inline fun BufferedInputStream.readBytesBuffered(bufferLength: Int, block: (buffer: ByteArray, bytesRead: Int) -> Unit) {
    val buffer = ByteArray(bufferLength)
    var bytesRead: Int

    while (read(buffer, 0, bufferLength)
            .also { bytesRead = it } != -1) {
        block(buffer, bytesRead)
    }
}