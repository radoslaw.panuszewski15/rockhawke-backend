package pl.semidude.rockhawk.helpers

fun getYoutubeDlCmd(): String {
    return if (System.getProperty("os.name").toLowerCase().contains("win")) {
        "youtube-dl.exe"
    } else {
        "./youtube-dl"
    }
}