@file:Suppress("UsePropertyAccessSyntax")

package pl.semidude.rockhawk

import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.context.annotation.Import
import org.springframework.http.HttpStatus
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.ResultMatcher
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.request
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.result.StatusResultMatchers
import pl.semidude.rockhawk.config.security.TestResourceServerConfig
import pl.semidude.rockhawk.helpers.submitCallable
import pl.semidude.rockhawk.rest.controller.YoutubePlaybacksController
import pl.semidude.rockhawk.rest.model.PlaybackRest
import pl.semidude.rockhawk.rest.model.TrackRest
import java.net.URI
import java.nio.file.Files
import java.nio.file.Paths
import java.util.concurrent.ExecutorService
import kotlin.concurrent.thread
import kotlin.test.assertEquals
import kotlin.test.assertTrue


@SpringBootTest
@RunWith(SpringRunner::class)
@AutoConfigureMockMvc
@Import(TestResourceServerConfig::class)
class YoutubePlaybacksControllerTest {

    companion object {
        private val trackDto = TrackRest(
            id = "kRteE5qwDkE",
            title = "Król Szamanów - Opening PL",
            durationMillis = 231000,
            durationFormatted = "3:51",
            type = "youtube",
            thumbnailUrl = "https://i.ytimg.com/vi/xqds0B_meys/hqdefault.jpg?sqp=-oaymwEiCKgBEF5IWvKriqkDFQgBFQAAAAAYASUAAMhCPQCAokN4AQ==&rs=AOn4CLAPLBWVXc84x8egE77OOHBR1auThw",
            imageUrl = "https://i.ytimg.com/vi/xqds0B_meys/hqdefault.jpg?sqp=-oaymwEiCKgBEF5IWvKriqkDFQgBFQAAAAAYASUAAMhCPQCAokN4AQ==&rs=AOn4CLAPLBWVXc84x8egE77OOHBR1auThw",
            description = "",
            viewCount = "400 tys."
        )

        private val testSongBytes: ByteArray = Files.readAllBytes(Paths.get("shaman-king.webm"))
    }

    @Autowired
    private lateinit var mockMvc: MockMvc
    @Autowired
    private lateinit var executor: ExecutorService
    @MockBean
    private lateinit var http: HttpSecurity
//    @MockBean private var template: OAuth2RestTemplate? = null

    @Autowired
    private lateinit var youtubePlaybacksController: YoutubePlaybacksController

    @Before
    fun setup() {
//        whenever(template!!.oAuth2ClientContext).thenReturn(DefaultOAuth2ClientContext(DefaultAccessTokenRequest()))
//        whenever(template!!.accessToken).thenReturn(DefaultOAuth2AccessToken("my-fake-token"))

        http.authorizeRequests()
//            .anyRequest().permitAll()
    }

    @Test
    fun `when prepare playback and s3 url is not available then it is null`() {
        val playbackDto = youtubePlaybacksController.preparePlayback(trackDto.id)

        assertTrue { playbackDto.s3Url == null }
    }

    @Test
    fun `when start playback which has not been prepared then 404 NOT FOUND`() {
        sendWrongStartPlaybackRequest(-1, statusChecker = { it.isNotFound })
    }

    @Test
    fun `when start playback which has been prepared then audio starts streaming`() {
        val playbackDto = youtubePlaybacksController.preparePlayback(trackDto.id)

        val startResultFuture = executor.submitCallable {
            sendStartPlaybackRequest(playbackDto.playbackId)
        }

        Thread.sleep(500)
//        youtubePlaybacksController.stopPlayback(playbackDto.playbackId)

        val partialAudioBytes = startResultFuture.get().response.contentAsByteArray
        val expectedBytes = testSongBytes.copyOfRange(0, partialAudioBytes.size)

        assertTrue { partialAudioBytes.isNotEmpty() }
        assertTrue { expectedBytes.contentEquals(partialAudioBytes) }
    }

    @Test
    fun `when start playback and seek back then stream is started from sought pos`() {
        val playbackDto = youtubePlaybacksController.preparePlayback(trackDto.id)

        thread { sendStartPlaybackRequest(playbackDto.playbackId) }

        val seekResult = sendSeekRequest(playbackDto, 1024, statusChecker = { it.isMovedPermanently })

        val actualResultFuture = executor
            .submitCallable { sendRequestFromRedirect(seekResult.response.redirectedUrl!!) }

        Thread.sleep(5000)
        youtubePlaybacksController.stopPlayback(0)

        val partialAudioBytes = actualResultFuture.get().response.contentAsByteArray
        val expectedBytes = testSongBytes.copyOfRange(1024, 1024 + partialAudioBytes.size)

        assertTrue { partialAudioBytes.isNotEmpty() }
        assertTrue { expectedBytes.contentEquals(partialAudioBytes) }
    }

    @Test
    fun `when start playback and wait for it to complete and seek back then bytes are returned synchronously`() {
        val playbackDto = youtubePlaybacksController.preparePlayback(trackDto.id)

        sendStartPlaybackRequest(playbackDto.playbackId)

        val seekResult = sendSeekRequest(playbackDto, 1024, statusChecker = { it.isPartialContent })

        val partialAudioBytes = seekResult.response.contentAsByteArray
        val expectedBytes = testSongBytes.copyOfRange(1024, 1024 + partialAudioBytes.size)

        assertTrue { expectedBytes.contentEquals(partialAudioBytes) }
    }

    @Test
    fun `when try to seek to not started track then get redirected`() {
        val playbackDto = youtubePlaybacksController.preparePlayback(trackDto.id)
        val response = youtubePlaybacksController.seekTrackOnPlayback(playbackDto.playbackId, 0, "bytes=664-")

        assertEquals(HttpStatus.MOVED_PERMANENTLY, response.statusCode)
        assertEquals(URI("/youtubePlaybacks/redirected/${playbackDto.playbackId}/0?startPos=664"), response.headers.location)
    }

    private fun sendStartPlaybackRequest(
        playbackId: Long,
        statusChecker: (StatusResultMatchers) -> ResultMatcher = { it.isOk }
    ) =
        mockMvc.perform(
            MockMvcRequestBuilders
                .get("/youtubePlaybacks/$playbackId/0")
        )
            .andExpect(request().asyncStarted())
            .andDo { mvcResult -> mvcResult.getAsyncResult() }
            .andExpect(statusChecker(status()))
            .andReturn()


    private fun sendWrongStartPlaybackRequest(
        playbackId: Long,
        statusChecker: (StatusResultMatchers) -> ResultMatcher = { it.isOk }
    ) =
        mockMvc.perform(
            MockMvcRequestBuilders
                .get("/api/youtubePlaybacks/$playbackId/0")
        )
            .andExpect(statusChecker(status()))
            .andReturn()


    private fun sendSeekRequest(
        playbackRest: PlaybackRest,
        rangeFrom: Int,
        statusChecker: (StatusResultMatchers) -> ResultMatcher
    ) =
        mockMvc.perform(
            MockMvcRequestBuilders
                .get("/api/youtubePlaybacks/${playbackRest.playbackId}/0")
                .header("Range", "bytes=$rangeFrom-")
        )
            .andExpect(statusChecker(status()))
            .andReturn()


    private fun sendRequestFromRedirect(redirectUrl: String) =
        mockMvc.perform(
            MockMvcRequestBuilders
                .get(redirectUrl)
                .header("Range", "bytes=1024-") //including Range as to simulate the real-world behavior
        )
            .andExpect(request().asyncStarted())
            .andDo { mvcResult -> mvcResult.getAsyncResult() }
            .andExpect(status().isPartialContent)
            .andReturn()
}