package pl.semidude.rockhawk

import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import pl.semidude.rockhawk.domain.*

@SpringBootTest
@RunWith(SpringRunner::class)
class MongoRepositoryInheritanceTest {

    @Autowired lateinit var youtubeTrackRepository: YoutubeTrackRepository
    @Autowired lateinit var spotifyTrackRepository: SpotifyTrackRepository

    private val spotifyTrack = SpotifyTrack(
            platformId = "S8BAF1SM",
            title = "Some Awesome Track",
            thumbnailUrl = "http://some/thumbnail.jpg",
            durationMillis = 178000,
            durationFormatted = "2:58",
            album = Album("123", "Some Album", listOf(Artist("321", "Some Artist"))),
            artists = listOf(Artist("321", "Some Artist"))
    )

    private val youtubeTrack = YoutubeTrack(
            platformId = "F1ACE58",
            title = "Some Awesome Track",
            thumbnailUrl = "https://some/thumbnail.jpg",
            imageUrl = "https://some/thumbnail.jpg",
            durationMillis = 178000,
            durationFormatted = "2:58",
            viewCount = "8 mln",
            owner = "koles"
    )

    @Before
    fun setup() {
        youtubeTrackRepository.deleteAll()
        spotifyTrackRepository.deleteAll()
    }

    //findAll()
    @Test
    fun `when save SpotifyTrack then SpotifyTrackRepository#findAll returns it`() {
        spotifyTrackRepository.save(spotifyTrack)
        assertEquals(listOf(spotifyTrack), spotifyTrackRepository.findAll())
    }

    @Test
    fun `when save SpotifyTrack then YoutubeTrackRepository#findAll does not return it`() {
        spotifyTrackRepository.save(spotifyTrack)
        assertEquals(emptyList<YoutubeTrack>(), youtubeTrackRepository.findAll())
    }

    @Test
    fun `when save YoutubeTrack then YoutubeTrackRepository#findAll returns it`() {
        youtubeTrackRepository.save(youtubeTrack)
        assertEquals(listOf(youtubeTrack), youtubeTrackRepository.findAll())
    }

    @Test
    fun `when save YoutubeTrack then SpotifyTrackRepository#findAll does not return it`() {
        youtubeTrackRepository.save(youtubeTrack)
        assertEquals(emptyList<SpotifyTrack>(), spotifyTrackRepository.findAll())
    }

    //count()
    @Test
    fun `when save SpotifyTrack then SpotifyTrackRepository#count counts it`() {
        spotifyTrackRepository.save(spotifyTrack)
        assertEquals(1, spotifyTrackRepository.count())
    }

    @Test
    fun `when save SpotifyTrack then YoutubeTrackRepository#count does not count it`() {
        spotifyTrackRepository.save(spotifyTrack)
        assertEquals(0, youtubeTrackRepository.count())
    }

    @Test
    fun `when save YoutubeTrack then YoutubeTrackRepository#count counts it`() {
        youtubeTrackRepository.save(youtubeTrack)
        assertEquals(1, youtubeTrackRepository.count())
    }

    @Test
    fun `when save YoutubeTrack then SpotifyTrackRepository#count does not count it`() {
        youtubeTrackRepository.save(youtubeTrack)
        assertEquals(0, spotifyTrackRepository.count())
    }

    //queries generated from method names
    @Test
    fun `when save SpotifyTrack then SpotifyTrackRepository#findByTitle returns it`() {
        spotifyTrackRepository.save(spotifyTrack)
        assertEquals(spotifyTrack, spotifyTrackRepository.findByTitle("Some Awesome Track"))
    }

    @Test
    fun `when save YoutubeTrack then SpotifyTrackRepository#findByTitle does not return it`() {
        youtubeTrackRepository.save(youtubeTrack)
        assertEquals(null, spotifyTrackRepository.findByTitle("Some Awesome Track"))
    }

    @Test
    fun `when save YoutubeTrack then YoutubeTrackRepository#findByYoutubeUrl returns it`() {
        youtubeTrackRepository.save(youtubeTrack)
        assertEquals(youtubeTrack, youtubeTrackRepository.findByTitle("Some Awesome Track"))
    }

    @Test
    fun `when save SpotifyTrack then YoutubeTrackRepository#findByYoutubeUrl does not return it`() {
        spotifyTrackRepository.save(spotifyTrack)
        assertEquals(null, youtubeTrackRepository.findByTitle("Some Awesome Track"))
    }
}