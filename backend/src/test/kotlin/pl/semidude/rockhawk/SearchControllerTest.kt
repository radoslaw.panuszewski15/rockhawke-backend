package pl.semidude.rockhawk

import com.nhaarman.mockitokotlin2.whenever
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpStatus
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.web.server.ResponseStatusException
import pl.semidude.rockhawk.config.security.WithUserId
import pl.semidude.rockhawk.domain.TrackRepository
import pl.semidude.rockhawk.rest.controller.SearchController
import pl.semidude.rockhawk.domain.User
import pl.semidude.rockhawk.domain.UserRepository
import pl.semidude.rockhawk.service.common.AsynchronousTrackSearcher
import pl.semidude.rockhawk.service.rest.ResponseEnricher
import pl.semidude.rockhawk.service.youtube.audio.YoutubeAudioProvider
import java.util.*
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

@SpringBootTest
@RunWith(SpringRunner::class)
@WithUserId(userId = "some-user-id")
class SearchControllerTest {

    private lateinit var searchController: SearchController

    @Autowired lateinit var asynchronousTrackSearcher: AsynchronousTrackSearcher
    @Mock lateinit var userRepo: UserRepository
    @Mock lateinit var trackRepo: TrackRepository
    @Mock lateinit var youtubeAudioProvider: YoutubeAudioProvider
    @Autowired lateinit var responseEnricher: ResponseEnricher

    @Before
    fun setup() {
        whenever(userRepo.findById("someDude")).thenReturn(Optional.of(User()))
        searchController = SearchController(
            asynchronousTrackSearcher, userRepo, trackRepo, youtubeAudioProvider, responseEnricher)
    }

    @Test
    fun `when pass neither youtube nor spotify parameters then exception is thrown`() {
        val e = assertFailsWith<ResponseStatusException> {
            searchController.searchTracks("some track")
        }
        assertEquals(HttpStatus.BAD_REQUEST, e.status)
    }

    @Test
    fun `when search for rickroll then we got rickrolled`() {
        val tracks = searchController.searchTracks("rickroll", youtube = true)

        assertTrue(tracks.any {
            it.title == "Rick Astley - Never Gonna Give You Up (Video)" &&
            it.id == "dQw4w9WgXcQ"
        })
    }

    @Test
    fun `when search hunter then nothing breaks up`() {
        val tracks = searchController.searchTracks("hunter", youtube = true)

        assertTrue(tracks.size == 20)
    }
}