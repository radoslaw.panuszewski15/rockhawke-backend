package pl.semidude.rockhawk.config.security

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.whenever
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.springframework.security.authentication.AbstractAuthenticationToken
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails
import org.springframework.security.test.context.support.WithSecurityContext
import org.springframework.security.test.context.support.WithSecurityContextFactory
import javax.servlet.http.HttpServletRequest

@WithSecurityContext(factory = UserIdSecurityContextFactory::class)
annotation class WithUserId(
    val userId: String
)

internal class UserIdSecurityContextFactory : WithSecurityContextFactory<WithUserId> {

    @Mock private lateinit var dummyHttpRequest: HttpServletRequest

    init {
        MockitoAnnotations.initMocks(this)
        whenever(dummyHttpRequest.getAttribute(any())).thenReturn(null)
        whenever(dummyHttpRequest.remoteAddr).thenReturn(null)
        whenever(dummyHttpRequest.session).thenReturn(null)
    }

    override fun createSecurityContext(annotation: WithUserId): SecurityContext {

        val context = SecurityContextHolder.createEmptyContext()

        val authentication = object : AbstractAuthenticationToken(emptyList()) {

            override fun getCredentials(): Any {
                return Unit
            }

            override fun getPrincipal(): Any {
                return Unit
            }

            override fun getDetails(): Any {

                return object : OAuth2AuthenticationDetails(dummyHttpRequest) {

                    override fun getDecodedDetails(): Any {

                        return mapOf(
                            "user_id" to annotation.userId
                        )
                    }
                }
            }
        }

        context.authentication = authentication
        return context
    }
}