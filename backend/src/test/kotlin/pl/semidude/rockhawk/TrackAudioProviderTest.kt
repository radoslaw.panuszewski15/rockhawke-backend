package pl.semidude.rockhawk

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import org.apache.commons.io.IOUtils
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.springframework.test.context.junit4.SpringRunner
import pl.semidude.rockhawk.domain.Range
import pl.semidude.rockhawk.helpers.getYoutubeDlCmd
import pl.semidude.rockhawk.helpers.printDiff
import pl.semidude.rockhawk.helpers.submitCallable
import pl.semidude.rockhawk.service.youtube.audio.PartAudioProvider
import pl.semidude.rockhawk.service.youtube.audio.TrackAudioProvider
import pl.semidude.rockhawk.service.youtube.audio.TrackAudioProvider.ReusedAudioStream
import pl.semidude.rockhawk.service.youtube.audio.YoutubeAudioProvider.Companion.END_OF_TRACK
import pl.semidude.rockhawk.service.youtube.util.YoutubeUrlExtractorImpl
import java.io.BufferedInputStream
import java.io.InputStream
import java.lang.IllegalStateException
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.util.concurrent.Executors
import kotlin.test.assertEquals
import kotlin.test.assertTrue

@Suppress("ConstantConditionIf")
@RunWith(SpringRunner::class)
class TrackAudioProviderTest {

    companion object {
        private const val updateYoutubeDl = true
        private const val refreshTestBytes = true

        private val urlExtractor = YoutubeUrlExtractorImpl()

        private const val youtubeTrackId = "xqds0B_meys"
        private val audioUrl = urlExtractor.extractAudioUrl(youtubeTrackId)

        private val kB = 1024

        private val testSongBytes: ByteArray = run {

            if (updateYoutubeDl) {
                Runtime.getRuntime().exec("${getYoutubeDlCmd()} --update")
            }

            if (refreshTestBytes) {
                if (Files.exists(Path.of("animal.webm"))) {
                    Files.delete(Path.of("animal.webm"))
                }
                val youtubeUrl = "https://www.youtube.com/watch?v=$youtubeTrackId"
                Runtime.getRuntime().exec("${getYoutubeDlCmd()} -x -o animal.webm $youtubeUrl").waitFor()
            }
            Files.readAllBytes(Paths.get("animal.webm"))
        }


        private val executor = Executors.newCachedThreadPool()
    }

    @Mock private lateinit var firstMockPartAudioProvider: PartAudioProvider
    @Mock private lateinit var secondMockPartAudioProvider: PartAudioProvider
    private var providerIndex = 0

    private var lastCreatedPartAudioProvider: PartAudioProvider? = null

    private val mockPartAudioProviderFactory = { audioUrl: String, range: Range ->
        lastCreatedPartAudioProvider = PartAudioProvider(youtubeTrackId, audioUrl, range)

        when (providerIndex++) {
            0 -> firstMockPartAudioProvider
            1 -> secondMockPartAudioProvider
            else -> throw IllegalStateException("cannot get provider for index $providerIndex")
        }.also { mockProvider ->
            whenever(mockProvider.range).thenReturn(range)
            whenever(mockProvider.openAudioStream(any())).thenReturn(BufferedInputStream(IOUtils.toInputStream("")))
        }
    }

    private fun createMockTrackAudioProvider(): TrackAudioProvider =
            TrackAudioProvider(youtubeTrackId, mockPartAudioProviderFactory)
                .also {
                    it.initTrackCache(audioUrl)
                }

    private fun createRealTrackAudioProvider(): TrackAudioProvider =
            TrackAudioProvider(youtubeTrackId)
                .also {
                    it.initTrackCache(audioUrl)
                }

    private fun createPartAudioProvider(range: Range): PartAudioProvider =
            PartAudioProvider(youtubeTrackId, audioUrl, range)

    private fun InputStream.ignoreIfReused(): InputStream =
        if (this is ReusedAudioStream) IOUtils.toInputStream("")
        else this

    @Before
    fun setup() {
        providerIndex = 0
    }

    @Test
    fun `when open first audio stream then new PartAudioProvider is created`() {
        val trackAudioProvider = createMockTrackAudioProvider()
        trackAudioProvider.openAudioStream(0*kB)

        assertEquals(
            createPartAudioProvider(Range(0, END_OF_TRACK)),
            lastCreatedPartAudioProvider
        )
    }

    @Test
    fun `when open second audio stream with not cached position then new PartAudioProvider is created`() {
        whenever(firstMockPartAudioProvider.isPositionAlreadyCached(any())).thenReturn(false)

        val trackAudioProvider = createMockTrackAudioProvider()
        trackAudioProvider.openAudioStream(0*kB)
        trackAudioProvider.openAudioStream(500*kB)

        assertEquals(
            createPartAudioProvider(Range(500 * kB, END_OF_TRACK)),
            lastCreatedPartAudioProvider
        )
    }

    @Test
    fun `when open second audio stream with already cached position then existing PartAudioProvider is reused`() {
        whenever(firstMockPartAudioProvider.isPositionAlreadyCached(any())).thenReturn(true)

        val trackAudioProvider = createMockTrackAudioProvider()
        trackAudioProvider.openAudioStream(0*kB)
        trackAudioProvider.openAudioStream(500*kB)

        assertEquals(
            createPartAudioProvider(Range(0 * kB, END_OF_TRACK)),
            lastCreatedPartAudioProvider
        )
    }

    @Test
    fun `when open second audio stream with not cached position then adjust old PartAudioProvider end pos`() {
        whenever(firstMockPartAudioProvider.isPositionAlreadyCached(any())).thenReturn(false)

        val trackAudioProvider = createMockTrackAudioProvider()
        trackAudioProvider.openAudioStream(0*kB)
        trackAudioProvider.openAudioStream(500*kB)

        verify(firstMockPartAudioProvider).end = 500*kB
    }

    @Test
    fun `test open 2 streams in ascending order`() {
        val trackAudioProvider = createRealTrackAudioProvider()

        val firstInputStream = trackAudioProvider.openAudioStream(0)
        val secondInputStream = trackAudioProvider.openAudioStream(500*kB)

        val firstBytes = executor.submitCallable { firstInputStream.readAllBytes() }
        val secondBytes = executor.submitCallable { secondInputStream.ignoreIfReused().readAllBytes() }

        testSongBytes.printDiff(firstBytes.get() + secondBytes.get(), false)

        assertTrue { testSongBytes.contentEquals(firstBytes.get() + secondBytes.get()) }
    }

    @Test
    fun `test open 3 streams in ascending order`() {
        val trackAudioProvider = createRealTrackAudioProvider()

        val firstInputStream = trackAudioProvider.openAudioStream(0)
        val secondInputStream = trackAudioProvider.openAudioStream(500*kB)
        val thirdInputStream = trackAudioProvider.openAudioStream(1000*kB)

        val firstBytes = executor.submitCallable { firstInputStream.readAllBytes() }
        val secondBytes = executor.submitCallable { secondInputStream.ignoreIfReused().readAllBytes() }
        val thirdBytes = executor.submitCallable { thirdInputStream.ignoreIfReused().readAllBytes() }

        assertTrue { testSongBytes.contentEquals(firstBytes.get() + secondBytes.get() + thirdBytes.get()) }
    }

    @Test
    fun `test open 3rd stream in between 1st and 2nd`() {
        val trackAudioProvider = createRealTrackAudioProvider()

        val firstInputStream = trackAudioProvider.openAudioStream(0)
        val secondInputStream = trackAudioProvider.openAudioStream(1000*kB)
        val thirdInputStream = trackAudioProvider.openAudioStream(500*kB)

        val firstBytes = executor.submitCallable { firstInputStream.readAllBytes() }
        val secondBytes = executor.submitCallable { secondInputStream.ignoreIfReused().readAllBytes() }
        val thirdBytes = executor.submitCallable { thirdInputStream.ignoreIfReused().readAllBytes() }

        assertTrue { testSongBytes.contentEquals(firstBytes.get() + thirdBytes.get() + secondBytes.get()) }
    }

    @Test
    fun `test open 4th stream in between 2nd and 3rd`() {
        val trackAudioProvider = createRealTrackAudioProvider()

        val firstInputStream = trackAudioProvider.openAudioStream(0)
        val secondInputStream = trackAudioProvider.openAudioStream(1000*kB)
        val thirdInputStream = trackAudioProvider.openAudioStream(500*kB)
        val fourthInputStream = trackAudioProvider.openAudioStream(750*kB)

        val firstBytes = executor.submitCallable { firstInputStream.readAllBytes() }
        val secondBytes = executor.submitCallable { secondInputStream.ignoreIfReused().readAllBytes() }
        val thirdBytes =  executor.submitCallable { thirdInputStream.ignoreIfReused().readAllBytes() }
        val fourthBytes = executor.submitCallable { fourthInputStream.ignoreIfReused().readAllBytes() }

        assertTrue { testSongBytes.contentEquals(firstBytes.get() + thirdBytes.get() + fourthBytes.get() + secondBytes.get()) }
    }

    @Test
    fun `test open 4th stream in between 2nd and 3rd and then 5th in between 4th and 2nd`() {
        val trackAudioProvider = createRealTrackAudioProvider()

        val firstInputStream = trackAudioProvider.openAudioStream(0)
        val secondInputStream = trackAudioProvider.openAudioStream(1000*kB)
        val thirdInputStream = trackAudioProvider.openAudioStream(500*kB)
        val fourthInputStream = trackAudioProvider.openAudioStream(750*kB)
        val fifthInputStream = trackAudioProvider.openAudioStream(850*kB)

        val firstBytes = executor.submitCallable { firstInputStream.readAllBytes() }
        val secondBytes = executor.submitCallable { secondInputStream.ignoreIfReused().readAllBytes() }
        val thirdBytes =  executor.submitCallable { thirdInputStream.ignoreIfReused().readAllBytes() }
        val fourthBytes = executor.submitCallable { fourthInputStream.ignoreIfReused().readAllBytes() }
        val fifthBytes = executor.submitCallable { fifthInputStream.ignoreIfReused().readAllBytes() }

        assertTrue { testSongBytes.contentEquals(firstBytes.get() + thirdBytes.get() + fourthBytes.get() + fifthBytes.get() + secondBytes.get()) }
    }

    @Test
    fun `test open streams in reverse order`() {
        val trackAudioProvider = createRealTrackAudioProvider()

        val firstInputStream = trackAudioProvider.openAudioStream(1000*kB)
        val secondInputStream = trackAudioProvider.openAudioStream(750*kB)
        val thirdInputStream = trackAudioProvider.openAudioStream(500*kB)
        val fourthInputStream = trackAudioProvider.openAudioStream(0*kB)

        val firstBytes = executor.submitCallable { firstInputStream.readAllBytes() }
        val secondBytes = executor.submitCallable { secondInputStream.readAllBytes() }
        val thirdBytes = executor.submitCallable { thirdInputStream.readAllBytes() }
        val fourthBytes = executor.submitCallable { fourthInputStream.readAllBytes() }

        assertTrue { testSongBytes.contentEquals(fourthBytes.get() + thirdBytes.get() + secondBytes.get() + firstBytes.get()) }
    }

    @Test
    fun `test open streams in reverse order and 3rd in between 1st and 2nd`() {
        val trackAudioProvider = createRealTrackAudioProvider()

        val firstInputStream = trackAudioProvider.openAudioStream(1000*kB)
        val secondInputStream = trackAudioProvider.openAudioStream(0*kB)
        val thirdInputStream = trackAudioProvider.openAudioStream(500*kB)

        val firstBytes = executor.submitCallable { firstInputStream.readAllBytes() }
        val secondBytes = executor.submitCallable { secondInputStream.readAllBytes() }
        val thirdBytes = executor.submitCallable { thirdInputStream.ignoreIfReused().readAllBytes() }

        assertTrue { testSongBytes.contentEquals(secondBytes.get() + thirdBytes.get() + firstBytes.get()) }
    }

    @Test
    fun `when use existing provider then reused stream returns correct bytes`() {
        val trackAudioProvider = createRealTrackAudioProvider()

        val firstInputStream = trackAudioProvider.openAudioStream(0*kB)
        Thread.sleep(2000)
        val secondInputStream = trackAudioProvider.openAudioStream(500*kB)

        val firstBytes = firstInputStream.readAllBytes()
        val secondBytes = secondInputStream.readAllBytes()

        val partialFirstBytes = firstBytes.copyOfRange(500*kB, firstBytes.size)

        assertTrue { secondInputStream is ReusedAudioStream }
        assertTrue { partialFirstBytes.contentEquals(secondBytes) }
    }

    @Test
    fun `when cache full bytes then getByteRange returns correct bytes`() {
        val trackAudioProvider = createRealTrackAudioProvider()

        trackAudioProvider.startCaching()
        trackAudioProvider.waitForFullBytes()

        val byteRange = trackAudioProvider.getByteRange(Range(500 * kB, 800 * kB))
        val expectedBytes = testSongBytes.copyOfRange(500*kB, 800*kB)

        assertTrue { byteRange.contentEquals(expectedBytes) }
    }

    @Test
    fun `when start caching bytes and seek forward then getByteRange from first part provider returns correct bytes`() {
        val trackAudioProvider = createRealTrackAudioProvider()

        trackAudioProvider.openAudioStream(0*kB)
        trackAudioProvider.openAudioStream(1000*kB)

        trackAudioProvider.waitForFullBytes()

        val byteRange = trackAudioProvider.getByteRange(Range(500 * kB, 800 * kB))
        val expectedBytes = testSongBytes.copyOfRange(500*kB, 800*kB)

        assertTrue { byteRange.contentEquals(expectedBytes) }
    }

    @Test
    fun `when start caching bytes and seek forward then getByteRange from second part provider returns correct bytes`() {
        val trackAudioProvider = createRealTrackAudioProvider()

        trackAudioProvider.openAudioStream(0*kB)
        trackAudioProvider.openAudioStream(1000*kB)

        trackAudioProvider.waitForFullBytes()

        val byteRange = trackAudioProvider.getByteRange(Range(1200 * kB, 1500 * kB))
        val expectedBytes = testSongBytes.copyOfRange(1200*kB, 1500*kB)

        assertTrue { byteRange.contentEquals(expectedBytes) }
    }

    @Test
    fun `when start caching bytes and seek forward then getByteRange crossing both part providers returns correct bytes`() {
        val trackAudioProvider = createRealTrackAudioProvider()

        trackAudioProvider.openAudioStream(0*kB)
        trackAudioProvider.openAudioStream(1000*kB)

        trackAudioProvider.waitForFullBytes()

        val byteRange = trackAudioProvider.getByteRange(Range(800 * kB, 1200 * kB))
        val expectedBytes = testSongBytes.copyOfRange(800*kB, 1200*kB)

        assertTrue { byteRange.contentEquals(expectedBytes) }
    }
}