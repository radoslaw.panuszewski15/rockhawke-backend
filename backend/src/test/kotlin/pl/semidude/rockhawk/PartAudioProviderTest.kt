package pl.semidude.rockhawk

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.test.context.junit4.SpringRunner
import pl.semidude.rockhawk.domain.Range
import pl.semidude.rockhawk.helpers.getYoutubeDlCmd
import pl.semidude.rockhawk.helpers.printDiff
import pl.semidude.rockhawk.service.youtube.audio.PartAudioProvider
import pl.semidude.rockhawk.service.youtube.audio.RangeException
import pl.semidude.rockhawk.service.youtube.util.YoutubeUrlExtractorImpl
import java.lang.IllegalStateException
import java.nio.file.FileSystems
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue

@Suppress("ConstantConditionIf")
@RunWith(SpringRunner::class)
class PartAudioProviderTest {

    companion object {
        private const val updateYoutubeDl = true
        private const val refreshTestBytes = true
        private val urlExtractor = YoutubeUrlExtractorImpl()

        private val youtubeTrackId = "xqds0B_meys"
        private val audioUrl = urlExtractor.extractAudioUrl(youtubeTrackId)

        private val kB = 1024
        private val testSongBytes: ByteArray = run {

            if (updateYoutubeDl) {
                Runtime.getRuntime().exec("${getYoutubeDlCmd()} --update")
            }
            if (refreshTestBytes) {
                if (Files.exists(Path.of("animal.webm"))) {
                    Files.delete(Path.of("animal.webm"))
                }
                val youtubeUrl = "https://www.youtube.com/watch?v=$youtubeTrackId"
                val process = Runtime.getRuntime().exec("${getYoutubeDlCmd()} -x -o animal.webm $youtubeUrl")
                process.waitFor()
            }
            Files.readAllBytes(Paths.get("animal.webm"))
        }
    }

    @Test
    fun `test getFullBytesFuture`() {
        val partAudioProvider = PartAudioProvider(youtubeTrackId, audioUrl, Range(1 * kB, 300 * kB))
        partAudioProvider.startCaching()

        val partialAudioBytes = partAudioProvider.getFullBytesFuture().get()
        val expectedBytes = testSongBytes.copyOfRange(1*kB, 300*kB)

        assertTrue { partialAudioBytes.isNotEmpty() }
        assertTrue { expectedBytes.contentEquals(partialAudioBytes) }
    }

    @Test
    fun `test getFullBytes`() {
        val partAudioProvider = PartAudioProvider(youtubeTrackId, audioUrl, Range(1 * kB, 300 * kB))
        partAudioProvider.startCaching()

        partAudioProvider.waitForFullBytes()

        val partialAudioBytes = partAudioProvider.getFullBytes()
        val expectedBytes = testSongBytes.copyOfRange(kB, 300*kB)

        assertTrue { partialAudioBytes.isNotEmpty() }
        assertTrue { expectedBytes.contentEquals(partialAudioBytes) }
    }

    @Test
    fun `when not fully cached then getFullBytes throws exception`() {
        val partAudioProvider = PartAudioProvider(youtubeTrackId, audioUrl, Range(1 * kB, 300 * kB))
        assertFailsWith<IllegalStateException> { partAudioProvider.getFullBytes() }
    }

    @Test
    fun `test getByteRange`() {
        val partAudioProvider = PartAudioProvider(youtubeTrackId, audioUrl, Range(1 * kB, 300 * kB))
        partAudioProvider.startCaching()

        partAudioProvider.waitForFullBytes()

        val partialAudioBytes = partAudioProvider.getByteRange(Range(0, 512))
        val expectedBytes = testSongBytes.copyOfRange(kB, 1*kB + 512)

        assertTrue { partialAudioBytes.isNotEmpty() }
        assertTrue { expectedBytes.contentEquals(partialAudioBytes) }
    }

    @Test
    fun `when range not cached then getByteRange throws exception`() {
        val partAudioProvider = PartAudioProvider(youtubeTrackId, audioUrl, Range(1 * kB, 300 * kB))
        assertFailsWith<IllegalStateException> { partAudioProvider.getByteRange(Range(0, 100 * kB)) }
    }

    @Test
    fun `when rangeFrom is less than 0 then getByteRange throws exception`() {
        val partAudioProvider = PartAudioProvider(youtubeTrackId, audioUrl, Range(1 * kB, 300 * kB))
        partAudioProvider.startCaching()
        partAudioProvider.waitForFullBytes()

        assertFailsWith<RangeException> { partAudioProvider.getByteRange(Range(-1, 100 * kB)) }
    }

    @Test
    fun `when rangeFrom is greater tah rangeTo then getByteRange throws exception`() {
        val partAudioProvider = PartAudioProvider(youtubeTrackId, audioUrl, Range(1 * kB, 300 * kB))
        partAudioProvider.startCaching()
        partAudioProvider.waitForFullBytes()

        assertFailsWith<RangeException> { partAudioProvider.getByteRange(Range(200 * kB, 100 * kB)) }
    }

    @Test
    fun `test openAudioStream`() {
        val partAudioProvider = PartAudioProvider(youtubeTrackId, audioUrl, Range(1 * kB, 300 * kB))

        val inputStream = partAudioProvider.openAudioStream()

        val partialAudioBytes = inputStream.readAllBytes()
        val expectedBytes = testSongBytes.copyOfRange(1*kB, 300*kB)

        assertTrue { partialAudioBytes.isNotEmpty() }
        assertTrue { expectedBytes.contentEquals(partialAudioBytes) }
    }

    @Test
    fun `when cache 2 overlapping parts then edge values are equal`() {
        val firstProvider = PartAudioProvider(youtubeTrackId, audioUrl, Range(0, 300 * kB))
        val secondProvider = PartAudioProvider(youtubeTrackId, audioUrl, Range(300 * kB - 1, 600 * kB))

        firstProvider.startCaching()
        secondProvider.startCaching()

        firstProvider.waitForFullBytes()
        secondProvider.waitForFullBytes()

        val firstBytes = firstProvider.getFullBytes()
        val secondBytes = secondProvider.getFullBytes()

        assertTrue { firstBytes.isNotEmpty() }
        assertTrue { secondBytes.isNotEmpty() }
        assertEquals(firstBytes.size + 1, secondBytes.size)
        assertEquals(firstBytes.last(), secondBytes.first())
    }

    @Test
    fun `when pause then partial bytes are cached and returned by getAvailableBytes`() {
        val partAudioProvider = PartAudioProvider(youtubeTrackId, audioUrl, Range.FULL)

        partAudioProvider.startCaching()
        Thread.sleep(500)
        partAudioProvider.pauseCaching()

        val availableBytes = partAudioProvider.getAvailableBytes()
        val expectedBytes = testSongBytes.copyOfRange(0, availableBytes.size)

//        assertTrue { availableBytes.size < testSongBytes.size }
        assertTrue { expectedBytes.contentEquals(availableBytes) }
    }

    @Test
    fun `when pause and resume then full bytes are cached`() {
        val partAudioProvider = PartAudioProvider(youtubeTrackId, audioUrl, Range.FULL)

        partAudioProvider.startCaching()
        Thread.sleep(500)
        partAudioProvider.pauseCaching()

        partAudioProvider.resumeCaching()

        val fullBytes = partAudioProvider.getFullBytesFuture().get()
        val expectedBytes = testSongBytes

        fullBytes.printDiff(expectedBytes)

        assertTrue { fullBytes.size == expectedBytes.size }
        assertTrue { expectedBytes.contentEquals(fullBytes)}
    }

    @Test
    fun `when pause caching then openAudioStream resumes it`() {
        val partAudioProvider = PartAudioProvider(youtubeTrackId, audioUrl, Range.FULL)

        partAudioProvider.startCaching()
        Thread.sleep(500)
        partAudioProvider.pauseCaching()

        val inputStream = partAudioProvider.openAudioStream()

        val partialAudioBytes = inputStream.readAllBytes()
        val expectedBytes = testSongBytes

        assertTrue { partialAudioBytes.isNotEmpty() }
        assertTrue { expectedBytes.contentEquals(partialAudioBytes) }
    }

    @Test
    fun `when fully cached then getFullPartSize returns correct size`() {
        val partAudioProvider = PartAudioProvider(youtubeTrackId, audioUrl, Range(1 * kB, 300 * kB))
        partAudioProvider.startCaching()
        partAudioProvider.waitForFullBytes()

        assertEquals(299*kB, partAudioProvider.getFullPartSize())
    }

    @Test
    fun `when not fully cached then getFullPartSize throws exception`() {
        val partAudioProvider = PartAudioProvider(youtubeTrackId, audioUrl, Range(1 * kB, 300 * kB))
        assertFailsWith<IllegalStateException> { partAudioProvider.getFullPartSize() }
    }
}