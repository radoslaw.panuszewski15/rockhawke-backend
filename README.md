# Rockhawk - music from YouTube and Spotify #

## About the project ##

Rockhawk is an application which allows you to listen to all your favorite music in one central place - no matter if it comes from Spotify or from YouTube. It creates a very smooth experience, without forcing you to jump from one platform to another all the time.

You can create your personal music library and mix songs from Spotify and YouTube as you want. You will also be able to make playlists and do all that stuff you would expect from a regular music app.

And... music from YouTube can be played in background, so you can use it as a simple YouTube wrapper if you don't care much about Spotify ;)

## Work in progress ##
Please be aware, that the project is being under active development, so not all features are yet implemented and some bugs can still show up!

## Gallery ##

<img src="https://i.ibb.co/5Yh3kwS/71003599-436567740293848-8892143845454643200-n.jpg" alt="screenshot2" width="280"/>
<img src="https://i.ibb.co/pWNLKWx/Screenshot-20191126-005537-Rockhawk.jpg" alt="scrennshot1" width="280"/>
<img src="https://i.ibb.co/SX0GPf3/Screenshot-20191126-005756-Rockhawk.jpg" alt="scrennshot1" width="280"/>
<img src="https://i.ibb.co/z4VGGKC/Screenshot-20191126-005810-Rockhawk.jpg" alt="scrennshot1" width="280"/>